# rsplt [![build status](https://gitlab.science.ru.nl/dsprenkels/rsplt/badges/master/build.svg)](https://gitlab.science.ru.nl/dsprenkels/rsplt/commits/master)



_rsplt_ (pronounced: "R-split") is a <b>r</b>u<b>s</b>t compiler for the
<b>S</b>imple <b>P</b>rogramming <b>L</b>anguage, as described by the Radboud
University compiler construction course 2016.

## Usage

Make sure you have installed the [Rust](https://www.rust-lang.org) compiler.
You can then use the standard Cargo commands, for running and testing:

```shell
cargo run   # run rsplt
cargo test  # run tests
```