use std::cell::{RefCell, Ref, RefMut};
use std::fmt;
use std::rc::Rc;

use infer::Type as SPLType;
use infer::ToSPLType;
use util::Span;

use codegen::*;

static mut ifstmtcount: i64 = 0;
static mut whilestmtcount: i64 = 0;

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct SPL {
    pub span: Span,
    pub decls: Vec<Decl>
}

impl fmt::Display for SPL {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for (count, v) in self.decls.iter().enumerate() {
            if count != 0 {
                try!(writeln!(f, ""));
            }
            try!(write!(f, "{}", v))
        }
        Ok(())
    }
}

impl ToSSMCode for SPL {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        // we first branch to the main function
        output.push(SSMLine::Instruction(Operation::Control(ControlType::Bra(OpArg::LiteralString(String::from("main"))))));
        for (count, v) in self.decls.iter().enumerate() {
            output.append(&mut v.to_ssm_code());
        }
        // check for main function
        let mut main_found = false;
        for (count, l) in output.iter().enumerate() {
            match *l {
                SSMLine::SSMLabel(ref x) => if x == "main" { main_found = true; break; },
                _                        => {}
            }
        }
        if !main_found { panic!("codegen: no main function found!"); }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Decl {
    Var(VarDecl),
    Fun(FunDecl)
}

impl fmt::Display for Decl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Decl::Var(ref x) => write!(f, "{}", x),
            &Decl::Fun(ref x) => write!(f, "{}", x)
        }
    }
}

impl ToSSMCode for Decl {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match *self {
            Decl::Var(ref x) => output.append(&mut x.to_ssm_code()),
            Decl::Fun(ref x) => output.append(&mut x.to_ssm_code())
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct VarDecl {
    pub span: Span,
    pub var_kw_or_type: VarKwOrType,
    pub name: Ident,
    pub value: Exp,
    pub global: bool,
}

impl fmt::Display for VarDecl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // XXX clean
        write!(f, "{} {} = {};", self.var_kw_or_type, self.name, self.value)
    }
}

impl ToSSMCode for VarDecl {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Comment(String::from("vardecl ")+&self.name.clone().name));
        output.append(&mut self.value.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum VarKwOrType {
    VarKw(VarKeyword),
    Type(Type)
}

impl fmt::Display for VarKwOrType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &VarKwOrType::VarKw(ref x) => write!(f, "{}", x),
            &VarKwOrType::Type(ref x)  => write!(f, "{}", x)
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct VarKeyword {
    pub span: Span
}


impl fmt::Display for VarKeyword {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "var")
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunDecl {
    pub span: Span,
    pub name: Ident,
    pub args: Vec<Ident>,
    pub fun_type: Option<FunType>,
    pub vars: Vec<VarDecl>,
    pub body: Vec<Stmt>
}

impl fmt::Display for FunDecl {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.name.name.starts_with("__builtin__") {
            return write!(f, "");
        }
        try!(write!(f, "{} (", self.name));
        for (count, v) in self.args.iter().enumerate() {
            if count != 0 {
                try!(write!(f, ", "));
            }
            try!(write!(f, "{}", v));
        }
        try!(write!(f, ")"));
        if let Some(ref fun_type) = self.fun_type {
            try!(write!(f, " :: {}", fun_type));
        }
        try!(writeln!(f, " {{"));
        let mut indented = String::new();
        for (count, v) in self.vars.iter().enumerate() {
            indented.push_str(&format!("{}\n", v));
        }

        for (count, v) in self.body.iter().enumerate() {
            indented.push_str(&format!("{}", v));
        }
        for line in indented.lines() {
            try!(writeln!(f, "    {}", line));
        }
        writeln!(f, "}}")
    }
}

impl ToSSMCode for FunDecl {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        let mut localvars: Vec<Ident> = Vec::new();
        output.push(SSMLine::SSMLabel(self.name.clone().name));
        output.push(SSMLine::Instruction(Operation::Control(ControlType::Link(OpArg::LiteralInt((self.args.len() + self.vars.len()) as i64)))));
        for (count, i) in self.args.iter().enumerate() {
            localvars.push(i.clone());
            output.push(SSMLine::Comment(String::from("argvar ")+&self.name.clone().name));
            output.push(SSMLine::Instruction(Operation::Load(LoadType::Mark(OpArg::LiteralInt((count as i64)-(self.args.len() as i64)-1)))));
            output.push(SSMLine::Instruction(Operation::Store(StoreType::Mark(OpArg::LiteralInt(localvars.len() as i64)))));
        }
        for (count, a) in self.vars.iter().enumerate() {
            localvars.push(a.clone().name);
            output.append(&mut a.to_ssm_code());
            output.push(SSMLine::Instruction(Operation::Store(StoreType::Mark(OpArg::LiteralInt(localvars.len() as i64)))));
        }
        for (count, v) in self.body.iter().enumerate() {
            // skip return statement after main, because the machine should halt
            if self.name.clone().name == "main" {
                match *v {
                    Stmt::Return(ref x) => continue,
                    _                   => {},
                }
            }
            output.append(&mut v.to_ssm_code());
        }
        // if a function returns a value, this is stored in the RR register, through the ReturnStmt
        if self.name.clone().name == "main" {
            // halt in case of the main function
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Halt)));
        }
        // replace all Var{Read,Store}'s with the correct local index
        for (count, o) in output.clone().iter().enumerate() {
            match *o {
                SSMLine::Literal(ref x) => {
                    match *x {
                        SSMLiteral::LiteralString(ref s) => {
                            let vs: Vec<&str> = s.split("_").collect();
                            if vs[0] == "VarRead" {
                                let mut lvindex = 0;
                                for (clv, lv) in localvars.iter().enumerate() {
                                    if lv.name.clone() == vs[1] {
                                        lvindex = clv + 1;
                                    }
                                }
                                output[count] = SSMLine::Instruction(Operation::Load(LoadType::Mark(OpArg::LiteralInt(lvindex as i64))));
                            } else if vs[0] == "VarStore" {
                                let mut lvindex = 0;
                                for (clv, lv) in localvars.iter().enumerate() {
                                    if lv.name.clone() == vs[1] {
                                        lvindex = clv + 1;
                                    }
                                }
                                output[count] = SSMLine::Instruction(Operation::Store(StoreType::Mark(OpArg::LiteralInt(lvindex as i64))));
                            } else if vs[0] == "VarStoreField" {
                                let what = match vs[1] {
                                    "hd" => SSMLine::Instruction(Operation::Store(StoreType::Address(OpArg::LiteralInt(0)))),
                                    "tl" => SSMLine::Literal(SSMLiteral::LiteralString(String::from("LDC 1\nSUB\nSTA 0"))), /* hacky */
                                    "fst" => SSMLine::Comment(String::from("storefield.fst not implemented")),
                                    "snd" => SSMLine::Comment(String::from("storefield.snd not implemented")),
                                    _     => SSMLine::Comment(String::from("storefield: unmatched")),
                                };
                                output[count] = what;
                            } else {
                                panic!("You made a boo-boo!");
                            }
                        },
                        _   => {}
                    }
                },
                _   => {},
            }
        }
        output.push(SSMLine::Instruction(Operation::Control(ControlType::Unlink)));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum RetType {
    Type(Type),
    Void(Void)
}

impl fmt::Display for RetType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &RetType::Type(ref x) => write!(f, "{}", x),
            &RetType::Void(ref x) => write!(f, "{}", x)
        }
    }
}

impl ToSPLType for RetType {
    fn to_spl_type(&self) -> SPLType {
        match self {
            &RetType::Type(ref t) => t.to_spl_type(),
            &RetType::Void(ref t) => t.to_spl_type(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Void {
    pub span: Span
}

impl fmt::Display for Void {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Void")
    }
}

impl ToSPLType for Void {
    fn to_spl_type(&self) -> SPLType {
        SPLType::Void
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunType {
    pub span: Span,
    pub args: Vec<Type>,
    pub ret: RetType
}

impl fmt::Display for FunType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.args.len() > 0 {
            for (count, v) in self.args.iter().enumerate() {
                if count != 0 {
                    try!(write!(f, " "));
                }
                try!(write!(f, "{}", v))
            }
            try!(write!(f, " -> "))
        }
        write!(f, "{}", self.ret)
    }
}

impl ToSPLType for FunType {
    fn to_spl_type(&self) -> SPLType {
        let args = self.args.iter().map(|x| Box::new(x.to_spl_type())).collect();
        let ret_type = self.ret.to_spl_type();
        SPLType::Function(args, Box::new(ret_type))
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Type {
    Basic(Box<BasicType>),
    Pair(Box<PairType>),
    List(Box<ListType>),
    Ident(Ident)
}

impl fmt::Display for Type {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Type::Basic(ref x) => write!(f, "{}", x),
            &Type::Pair(ref x)  => write!(f, "{}", x),
            &Type::List(ref x)  => write!(f, "{}", x),
            &Type::Ident(ref x) => write!(f, "{}", x)
        }
    }
}

impl ToSPLType for Type {
    fn to_spl_type(&self) -> SPLType {
        match *self {
           Type::Basic(ref t) => t.to_spl_type(),
           Type::Pair(ref t) => t.to_spl_type(),
           Type::List(ref t) => t.to_spl_type(),
           Type::Ident(ref t) => t.to_spl_type(),
       }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct BasicType {
    pub span: Span,
    pub kind: BasicTypeKind
}

impl fmt::Display for BasicType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl ToSPLType for BasicType {
    fn to_spl_type(&self) -> SPLType {
        match self.kind {
           BasicTypeKind::Int => SPLType::Int,
           BasicTypeKind::Bool => SPLType::Bool,
           BasicTypeKind::Char => SPLType::Char,
       }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum BasicTypeKind {
    Int, Bool, Char
}

impl fmt::Display for BasicTypeKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &BasicTypeKind::Int  => write!(f, "Int"),
            &BasicTypeKind::Bool => write!(f, "Bool"),
            &BasicTypeKind::Char => write!(f, "Char")
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PairType {
    pub span: Span,
    pub fst: Type,
    pub snd: Type
}

impl fmt::Display for PairType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({},{})", self.fst, self.snd)
    }
}

impl ToSPLType for PairType {
    fn to_spl_type(&self) -> SPLType {
        let fst = Box::new(self.fst.to_spl_type());
        let snd = Box::new(self.snd.to_spl_type());
        SPLType::Pair(fst, snd)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ListType {
    pub span: Span,
    pub list_type: Type
}

impl fmt::Display for ListType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[{}]", self.list_type)
    }
}

impl ToSPLType for ListType {
    fn to_spl_type(&self) -> SPLType {
        let inner = Box::new(self.list_type.to_spl_type());
        SPLType::List(inner)
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Stmt {
    If(IfStmt),
    While(WhileStmt),
    Ass(AssStmt),
    FunCall(FunCallStmt),
    Return(ReturnStmt)
}

impl fmt::Display for Stmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Stmt::If(ref x)      => write!(f, "{}", x),
            &Stmt::While(ref x)   => write!(f, "{}", x),
            &Stmt::Ass(ref x)     => write!(f, "{}", x),
            &Stmt::FunCall(ref x) => write!(f, "{}", x),
            &Stmt::Return(ref x)  => write!(f, "{}", x)
        }
    }
}

impl ToSSMCode for Stmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match *self {
            Stmt::If(ref x)      => output.append(&mut x.to_ssm_code()),
            Stmt::While(ref x)   => output.append(&mut x.to_ssm_code()),
            Stmt::Ass(ref x)     => output.append(&mut x.to_ssm_code()),
            Stmt::FunCall(ref x) => output.append(&mut x.to_ssm_code()),
            Stmt::Return(ref x)  => output.append(&mut x.to_ssm_code())
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct IfStmt {
    pub span: Span,
    pub cond: ParenExp,
    pub if_body: Vec<Stmt>,
    pub else_body: Vec<Stmt>
}

impl fmt::Display for IfStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(writeln!(f, "if {} {{", self.cond));
        let mut indented = String::new();
        for (count, v) in self.if_body.iter().enumerate() {
            indented.push_str(&format!("{}", v));
        }
        for line in indented.lines() {
            try!(writeln!(f, "    {}", line));
        }
        if self.else_body.len() > 0 {
            try!(writeln!(f, "}} else {{"));
            let mut indented = String::new();
            for (count, v) in self.else_body.iter().enumerate() {
                indented.push_str(&format!("{}", v));
            }
            for line in indented.lines() {
                try!(writeln!(f, "    {}", line));
            }
        }
        writeln!(f, "}}")
    }
}

impl ToSSMCode for IfStmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        // TODO: if statement counter?
        unsafe {
            output.push(SSMLine::SSMLabel(String::from(format!("ifstmtcond{}", ifstmtcount))));
            output.append(&mut self.cond.to_ssm_code());
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Brt(OpArg::LiteralString(String::from(format!("ifstmt{}ifbody", ifstmtcount)))))));
            // else_body
            output.push(SSMLine::SSMLabel(String::from(format!("ifstmt{}elsebody", ifstmtcount))));
            if self.else_body.len() == 0 {
                output.push(SSMLine::Instruction(Operation::Control(ControlType::Bra(OpArg::LiteralString(String::from(format!("ifstmt{}end", ifstmtcount)))))));
            } else {
                for (count, v) in self.else_body.iter().enumerate() {
                    output.append(&mut v.to_ssm_code());
                }
            }
            output.push(SSMLine::SSMLabel(String::from(format!("ifstmt{}ifbody", ifstmtcount))));
            for (count, v) in self.if_body.iter().enumerate() {
                output.append(&mut v.to_ssm_code());
            }
            // TODO: jump beyond if statement
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Bra(OpArg::LiteralString(String::from(format!("ifstmt{}end", ifstmtcount)))))));

            output.push(SSMLine::SSMLabel(String::from(format!("ifstmt{}end", ifstmtcount))));
            ifstmtcount += 1;
        }

        output
    }
}
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct WhileStmt {
    pub span: Span,
    pub cond: ParenExp,
    pub body: Vec<Stmt>
}

impl fmt::Display for WhileStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(writeln!(f, "while {} {{", self.cond));
        let mut indented = String::new();
        for (count, v) in self.body.iter().enumerate() {
            indented.push_str(&format!("{}", v));
        }
        for line in indented.lines() {
            try!(writeln!(f, "    {}", line));
        }
        writeln!(f, "}}")
    }
}

impl ToSSMCode for WhileStmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        unsafe {
            output.push(SSMLine::SSMLabel(String::from(format!("whilestmt_{}_cond", whilestmtcount))));
            output.append(&mut self.cond.to_ssm_code());
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Brf(OpArg::LiteralString(String::from(format!("whilestmt_{}_end", whilestmtcount)))))));

            output.push(SSMLine::SSMLabel(String::from(format!("whilestmt_{}", whilestmtcount))));

            for (count, v) in self.body.iter().enumerate() {
                output.append(&mut v.to_ssm_code());
            }
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Bra(OpArg::LiteralString(String::from(format!("whilestmt_{}_cond", whilestmtcount)))))));
            output.push(SSMLine::SSMLabel(String::from(format!("whilestmt_{}_end", whilestmtcount))));
            whilestmtcount += 1;
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct AssStmt {
    pub span: Span,
    pub var_name: Ident,
    pub field: Option<Field>,
    pub value: Exp
}

impl fmt::Display for AssStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}", self.var_name));
        if let Some(ref field) = self.field {
            try!(write!(f, " . {}", field));
        };
        writeln!(f, " = {};", self.value)
    }
}

impl ToSSMCode for AssStmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        // TODO: fields
        match self.field {
            Some(ref x) => {
                            // we got a field, determine kind
                            let kind = match (*x).kind {
                                FieldKind::Hd   => "hd_".to_string(),
                                FieldKind::Tl   => "tl_".to_string(),
                                FieldKind::Fst  => "fst_".to_string(),
                                FieldKind::Snd  => "snd_".to_string(),
                            };
                            output.append(&mut self.value.to_ssm_code());
                            output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarRead_")+&self.var_name.clone().name)));
                            output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarStoreField_")+&kind+&self.var_name.clone().name)));
                           },
            None        => {
                            output.append(&mut self.value.to_ssm_code());
                            output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarStore_")+&self.var_name.clone().name)));
                           },
        };
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunCallStmt {
    pub span: Span,
    pub fun_call: FunCall
}


impl fmt::Display for FunCallStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "{};", self.fun_call)
    }
}

impl ToSSMCode for FunCallStmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.fun_call.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ReturnStmt {
    pub span: Span,
    pub value: Option<Exp>
}

impl fmt::Display for ReturnStmt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "return"));
        match self.value {
            Some(ref x) => writeln!(f, " {};", x),
            None        => writeln!(f, ";")
        }
    }
}

impl ToSSMCode for ReturnStmt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match self.value {
            Some(ref x) => output.append(&mut x.to_ssm_code()),
            None        => (),
        };
        output.push(SSMLine::Instruction(Operation::Store(StoreType::Register(OpArg::LiteralString(String::from("RR"))))));
        output.push(SSMLine::Instruction(Operation::Control(ControlType::Unlink)));
        output.push(SSMLine::Instruction(Operation::Control(ControlType::Ret)));
        output
    }
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Exp {
    Op1(Box<Op1Exp>),
    Op2(Box<Op2Exp>),
    Field(Box<FieldExp>),
    FunCall(Box<FunCall>),
    Ident(Ident),
    Int(LiteralInt),
    Char(LiteralChar),
    Bool(LiteralBool),
    Pair(Box<PairExp>),
    Paren(Box<ParenExp>),
    EmptyList(EmptyList)
}

impl fmt::Display for Exp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Exp::Op1(ref x)       => write!(f, "{}", x),
            Exp::Op2(ref x)       => write!(f, "{}", x),
            Exp::Field(ref x)     => write!(f, "{}", x),
            Exp::FunCall(ref x)   => write!(f, "{}", x),
            Exp::Ident(ref x)     => write!(f, "{}", x),
            Exp::Int(ref x)       => write!(f, "{}", x),
            Exp::Char(ref x)      => write!(f, "{}", x),
            Exp::Bool(ref x)      => write!(f, "{}", x),
            Exp::Pair(ref x)      => write!(f, "{}", x),
            Exp::Paren(ref x)     => write!(f, "{}", x),
            Exp::EmptyList(ref x) => write!(f, "{}", x)
        }
    }
}

impl Exp {
    pub fn borrow_mut_spl_type(&mut self) -> RefMut<SPLType> {
        match *self {
            Exp::Op1(ref mut x)       => x.spl_type.borrow_mut(),
            Exp::Op2(ref mut x)       => x.spl_type.borrow_mut(),
            Exp::Field(ref mut x)     => x.spl_type.borrow_mut(),
            Exp::FunCall(ref mut x)   => x.spl_type.borrow_mut(),
            Exp::Ident(ref mut x)     => x.spl_type.borrow_mut(),
            Exp::Int(ref mut x)       => x.spl_type.borrow_mut(),
            Exp::Char(ref mut x)      => x.spl_type.borrow_mut(),
            Exp::Bool(ref mut x)      => match *x {
                LiteralBool::True(ref mut y) => y.spl_type.borrow_mut(),
                LiteralBool::False(ref mut y) => y.spl_type.borrow_mut(),
            },
            Exp::Pair(ref mut x)      => x.spl_type.borrow_mut(),
            Exp::Paren(ref mut x)     => x.spl_type.borrow_mut(),
            Exp::EmptyList(ref mut x) => x.spl_type.borrow_mut(),
        }
    }

    pub fn borrow_spl_type(&self) -> Ref<SPLType> {
        match *self {
            Exp::Op1(ref x)       => x.spl_type.borrow(),
            Exp::Op2(ref x)       => x.spl_type.borrow(),
            Exp::Field(ref x)     => x.spl_type.borrow(),
            Exp::FunCall(ref x)   => x.spl_type.borrow(),
            Exp::Ident(ref x)     => x.spl_type.borrow(),
            Exp::Int(ref x)       => x.spl_type.borrow(),
            Exp::Char(ref x)      => x.spl_type.borrow(),
            Exp::Bool(ref x)      => match *x {
                LiteralBool::True(ref y) => y.spl_type.borrow(),
                LiteralBool::False(ref y) => y.spl_type.borrow(),
            },
            Exp::Pair(ref x)      => x.spl_type.borrow(),
            Exp::Paren(ref x)     => x.spl_type.borrow(),
            Exp::EmptyList(ref x) => x.spl_type.borrow(),
        }
    }
}

impl ToSSMCode for Exp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match *self {
            Exp::Op1(ref x)       => output.append(&mut x.to_ssm_code()),
            Exp::Op2(ref x)       => output.append(&mut x.to_ssm_code()),
            Exp::Field(ref x)     => output.append(&mut x.to_ssm_code()),
            Exp::FunCall(ref x)   => output.append(&mut x.to_ssm_code()),
            Exp::Ident(ref x)     => output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarRead_")+&x.clone().name))),
            Exp::Int(ref x)       => output.append(&mut x.to_ssm_code()),
            Exp::Char(ref x)      => output.append(&mut x.to_ssm_code()),
            Exp::Bool(ref x)      => output.append(&mut x.to_ssm_code()),
            Exp::Pair(ref x)      => output.append(&mut x.to_ssm_code()),
            Exp::Paren(ref x)     => output.append(&mut x.to_ssm_code()),
            Exp::EmptyList(ref x) => output.append(&mut x.to_ssm_code()),
        };
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ParenExp {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub exp: Exp,
}

impl fmt::Display for ParenExp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({})", self.exp)
    }
}

impl ToSSMCode for ParenExp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.exp.to_ssm_code());
        output
    }
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FieldExp {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub var_name: Ident,
    pub field: Field,
}

impl fmt::Display for FieldExp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} . {}", self.var_name, self.field)
    }
}

impl ToSSMCode for FieldExp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        // ident: needs location on the heap
        // field: FieldKind (hd, tl, fst, snd) [. FieldKind [. FieldKind [...]]]
        output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarRead_")+&self.var_name.clone().name)));
        output.append(&mut self.field.to_ssm_code());

        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Op2Exp {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub op2: Op2,
    pub left: Exp,
    pub right: Exp,
}

impl fmt::Display for Op2Exp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {} {}", self.left, self.op2, self.right)
    }
}

impl ToSSMCode for Op2Exp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        /* we might something smart here in case of nested expressions */
        output.append(&mut self.left.to_ssm_code());
        output.append(&mut self.right.to_ssm_code());
        output.append(&mut self.op2.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Op2 {
    pub span: Span,
    pub kind: Op2Kind
}

impl fmt::Display for Op2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl ToSSMCode for Op2 {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.kind.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Op1Exp {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub op1: Op1,
    pub operand: Exp
}

impl fmt::Display for Op1Exp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.op1, self.operand)
    }
}

impl ToSSMCode for Op1Exp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.operand.to_ssm_code());
        output.append(&mut self.op1.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Op1 {
    pub span: Span,
    pub kind: Op1Kind
}

impl fmt::Display for Op1 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.kind)
    }
}

impl ToSSMCode for Op1 {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.kind.to_ssm_code());
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum LiteralBool {
    True(LiteralTrue), False(LiteralFalse)
}

impl fmt::Display for LiteralBool {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &LiteralBool::True(ref x) => write!(f, "{}", x),
            &LiteralBool::False(ref x) => write!(f, "{}", x)
        }
    }
}

impl ToSSMCode for LiteralBool {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match self {
            &LiteralBool::True(ref x) => output.append(&mut x.to_ssm_code()),
            &LiteralBool::False(ref x) => output.append(&mut x.to_ssm_code())
        }
        output
    }
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LiteralTrue {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
}

impl fmt::Display for LiteralTrue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "True")
    }
}

impl ToSSMCode for LiteralTrue {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(-1)))));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LiteralFalse {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
}

impl fmt::Display for LiteralFalse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "False")
    }
}

impl ToSSMCode for LiteralFalse {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(0)))));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct PairExp {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub fst: Exp,
    pub snd: Exp,
}

impl fmt::Display for PairExp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // '(' Exp ',' Exp ')'
        write!(f, "({}, {})", self.fst, self.snd)
    }
}

impl ToSSMCode for PairExp {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.fst.to_ssm_code());
        output.append(&mut self.snd.to_ssm_code());
        output
    }
}


#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Field {
    pub span: Span,
    pub kind: FieldKind,
    pub next: Option<Box<Field>>
}

impl fmt::Display for Field {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(write!(f, "{}", self.kind));
        match self.next {
            Some(ref x) => write!(f, "{}", x),
            None        => Ok(())
        }
    }
}

impl ToSSMCode for Field {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.append(&mut self.kind.to_ssm_code());
        match self.next {
            Some(ref x) => output.append(&mut x.to_ssm_code()),
            None        => {}
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum FieldKind {
    Hd, Tl, Fst, Snd
}

impl fmt::Display for FieldKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &FieldKind::Hd  => write!(f, "hd" ),
            &FieldKind::Tl  => write!(f, "tl" ),
            &FieldKind::Fst => write!(f, "fst"),
            &FieldKind::Snd => write!(f, "snd")
        }
    }
}

impl ToSSMCode for FieldKind {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        match self {
            /* Hd: load the value of the address currently at SP */
            &FieldKind::Hd  => {
                output.push(SSMLine::Instruction(Operation::Load(LoadType::Heap(OpArg::LiteralInt(0)))));
            },
            /* Tl: load the address of the (address currently at SP)-0x01 */
            &FieldKind::Tl  => {
                output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(1)))));
                output.push(SSMLine::Instruction(Operation::Compute(ComputeType::Sub)));
                output.push(SSMLine::Instruction(Operation::Load(LoadType::Heap(OpArg::LiteralInt(0)))));
            },
            &FieldKind::Fst => output.push(SSMLine::Comment(String::from("fst"))),
            &FieldKind::Snd => output.push(SSMLine::Comment(String::from("snd")))
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct FunCall {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub fun_name: Ident,
    pub act_args: Vec<Exp>
}

impl fmt::Display for FunCall {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // id '(' [ Exp (',' Exp)* ] ')'
        try!(write!(f, "{} (", self.fun_name));
        // print the expressions
        for (count, v) in self.act_args.iter().enumerate() {
            if count != 0 {
                try!(write!(f, ", "));
            }
            try!(write!(f, "{}", v));
        }
        write!(f, ")")
    }
}

impl ToSSMCode for FunCall {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        for (count, v) in self.act_args.iter().enumerate() {
            output.append(&mut v.to_ssm_code());
        }
        if self.fun_name.clone().name == "print" {
            // TRAP 0 prints an integer. Execution can continue normally after that
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Trap(OpArg::LiteralInt(0)))));
        } else if self.fun_name.clone().name == "putChar" {
            // TRAP 1 prints a character on the screen
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Trap(OpArg::LiteralInt(1)))));
        } else if self.fun_name.clone().name == "isEmpty" {
            // isEmpty gets the top of the list, compare to -1 and leave that value on the stack
            output.push(SSMLine::Instruction(Operation::Load(LoadType::Heap(OpArg::LiteralInt(0)))));
            output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(-1)))));
            output.push(SSMLine::Instruction(Operation::Compute(ComputeType::Eq)));

        } else {
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Bsr(OpArg::LiteralString(self.fun_name.clone().name)))));
            // after a function returns, the stackpointer needs a decrement of 1
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Ajs(OpArg::LiteralInt(-1)))));
            // assume that when a function returns, it sets the RR register, so load it up afterwards
            output.push(SSMLine::Instruction(Operation::Load(LoadType::Register(OpArg::LiteralString(String::from("RR"))))));
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct EmptyList {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
}

impl fmt::Display for EmptyList {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[]")
    }
}

impl ToSSMCode for EmptyList {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        // heap layout for an empty list (top-down)
        // -1  // next address, or -1 if last element
        // 0   // value
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(-1)))));
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(-1)))));
        output.push(SSMLine::Instruction(Operation::Store(StoreType::HeapMultiple(OpArg::LiteralInt(2)))));

        output
    }
}
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Op2Kind {
    Add, Sub, Mul, Div, Mod, Eq, Lt, Gt, Le, Ge, Ne, And, Or, Cons
}

impl fmt::Display for Op2Kind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Op2Kind::Add  => write!(f, "+" ),
            Op2Kind::Sub  => write!(f, "-" ),
            Op2Kind::Mul  => write!(f, "*" ),
            Op2Kind::Div  => write!(f, "/" ),
            Op2Kind::Mod  => write!(f, "%" ),
            Op2Kind::Eq   => write!(f, "=="),
            Op2Kind::Lt   => write!(f, "<" ),
            Op2Kind::Gt   => write!(f, ">" ),
            Op2Kind::Le   => write!(f, "<="),
            Op2Kind::Ge   => write!(f, ">="),
            Op2Kind::Ne   => write!(f, "!="),
            Op2Kind::And  => write!(f, "&&"),
            Op2Kind::Or   => write!(f, "||"),
            Op2Kind::Cons => write!(f, ":" )
        }
    }
}

impl ToSSMCode for Op2Kind {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        let kind = {
            match *self {
                Op2Kind::Add  => ComputeType::Add,
                Op2Kind::Sub  => ComputeType::Sub,
                Op2Kind::Mul  => ComputeType::Mul,
                Op2Kind::Div  => ComputeType::Div,
                Op2Kind::Mod  => ComputeType::Mod,
                Op2Kind::Eq   => ComputeType::Eq,
                Op2Kind::Lt   => ComputeType::Lt,
                Op2Kind::Gt   => ComputeType::Gt,
                Op2Kind::Le   => ComputeType::Le,
                Op2Kind::Ge   => ComputeType::Ge,
                Op2Kind::Ne   => ComputeType::Ne,
                Op2Kind::And  => ComputeType::And,
                Op2Kind::Or   => ComputeType::Or,
                Op2Kind::Cons => ComputeType::Cons
            }
        };
        if kind == ComputeType::Cons {
            // heap layout for an empty list (top-down)
            // -1  // next address, or -1 if last element
            // 0   // value
            // first, store the last value address of the heap
            output.push(SSMLine::Comment(String::from("cons")));
            output.push(SSMLine::Instruction(Operation::Store(StoreType::Heap)));
            // adjust the stack
            output.push(SSMLine::Instruction(Operation::Control(ControlType::Ajs(OpArg::LiteralInt(-1)))));
            // then store the last value on the stack
            output.push(SSMLine::Instruction(Operation::Store(StoreType::Heap)));
            output.push(SSMLine::Comment(String::from("end_cons")));
        } else {
            output.push(SSMLine::Instruction(Operation::Compute(kind)));
        }
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Op1Kind {
    Not, Neg
}

impl fmt::Display for Op1Kind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Op1Kind::Not => write!(f, "!"),
            &Op1Kind::Neg => write!(f, "-")
        }
    }
}

impl ToSSMCode for Op1Kind {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        let kind = {
            match *self {
                Op1Kind::Not => ComputeType::Not,
                Op1Kind::Neg => ComputeType::Neg
            }
        };
        output.push(SSMLine::Instruction(Operation::Compute(kind)));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Ident {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub name: String
}

impl fmt::Display for Ident {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // write!(f, "{} /* type: {:?} */", self.name, self.spl_type.borrow())
        write!(f, "{}", self.name)
    }
}

impl ToSPLType for Ident {
    fn to_spl_type(&self) -> SPLType {
        self.spl_type.borrow().clone()
    }
}

impl ToSSMCode for Ident {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Comment(String::from("Ident: ")+&self.name.clone()));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LiteralInt {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub value: i64
}

impl fmt::Display for LiteralInt {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl ToSSMCode for LiteralInt {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::SSMNumber(Number::PositiveNumber(NumberType::Decimal(self.value.clone())))))));
        output
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct LiteralChar {
    pub span: Span,
    pub spl_type: Rc<RefCell<SPLType>>,
    pub value: u8
}

impl fmt::Display for LiteralChar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl ToSSMCode for LiteralChar {
    fn to_ssm_code(&self) -> Vec<SSMLine> {
        let mut output: Vec<SSMLine> = Vec::new();
        output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::SSMNumber(Number::PositiveNumber(NumberType::Decimal(self.value.clone() as i64)))))));
        output
    }
}
