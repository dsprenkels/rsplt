use util::{Span, Error, ErrorAndSpan};

pub struct Scanner {
    source: Vec<char>,
    offset: usize,
    span_start: usize,
    eof_reached: bool
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Token {
    // Literal terminator symbols
    Eq, Semicolon, DoubleColon, LeftParen, RightParen, LeftBrace, RightBrace, LeftBracket,
    RightBracket, Comma, Dot, Arrow,

    // Keywords
    Var, Void, Int, Bool, Char, If, Else, While, Return, False, True, Hd, Tl, Fst, Snd,

    // Op{1,2}
    Exclamation, Plus, Minus, Star, Slash, Percent, EqEq, Lt, Gt, Le, Ge, Ne, AndAnd, OrOr, Colon,

    // id, char, int
    Ident(String), LiteralInt(i64), LiteralChar(char),

    // No-op tokens
    Whitespace(String), LineComment(String), MultiLineComment(String),

    // End-of-file
    Eof
}

#[derive(Debug, PartialEq, Eq)]
pub struct TokenAndSpan(pub Token, pub Span);

pub type ScanResult = Result<TokenAndSpan, ErrorAndSpan>;

impl Scanner {
    pub fn new(source: &str) -> Scanner {
        Scanner {
            source: source.chars().collect(),
            offset: 0,
            span_start: 0,
            eof_reached: false,
        }
    }

    /// Scan one token and return an Option, containing a ScanResult
    /// If the Option is None, this means that the scanner found the end of the file
    ///
    /// The scanning Result enum object can be:
    /// - Ok(TokenAndSpan)      A valid token was found at the file span and returned
    /// - Err(Error)    A error was encountered during the scanning process
    fn scan_once(&mut self) -> Option<ScanResult> {
        self.span_start = self.offset;

        match self.get_current_ch() {
            Some(';') => {
                self.offset += 1;
                self.result_token(Token::Semicolon)
            },
            Some(':') if self.next_ch_is(':') => {
                self.offset += 2;
                self.result_token(Token::DoubleColon)
            },
            Some(':') => {
                self.offset += 1;
                self.result_token(Token::Colon)
            },
            Some('(') => {
                self.offset += 1;
                self.result_token(Token::LeftParen)
            },
            Some(')') => {
                self.offset += 1;
                self.result_token(Token::RightParen)
            },
            Some('{') => {
                self.offset += 1;
                self.result_token(Token::LeftBrace)
            },
            Some('}') => {
                self.offset += 1;
                self.result_token(Token::RightBrace)
            },
            Some('[') => {
                self.offset += 1;
                self.result_token(Token::LeftBracket)
            },
            Some(']') => {
                self.offset += 1;
                self.result_token(Token::RightBracket)
            },
            Some(',') => {
                self.offset += 1;
                self.result_token(Token::Comma)
            },
            Some('.') => {
                self.offset += 1;
                self.result_token(Token::Dot)
            },
            Some('-') if self.next_ch_is('>') => {
                self.offset += 2;
                self.result_token(Token::Arrow)
            },
            Some('=') if self.next_ch_is('=') => {
                self.offset += 2;
                self.result_token(Token::EqEq)
            },
            Some('=') => {
                self.offset += 1;
                self.result_token(Token::Eq)
            },
            Some('<') if self.next_ch_is('=') => {
                self.offset += 2;
                self.result_token(Token::Le)
            },
            Some('<') => {
                self.offset += 1;
                self.result_token(Token::Lt)
            },
            Some('>') if self.next_ch_is('=') => {
                self.offset += 2;
                self.result_token(Token::Ge)
            },
            Some('>') => {
                self.offset += 1;
                self.result_token(Token::Gt)
            },
            Some('!') if self.next_ch_is('=') => {
                self.offset += 2;
                self.result_token(Token::Ne)
            },
            Some('!') => {
                self.offset += 1;
                self.result_token(Token::Exclamation)
            },
            Some('&') if self.next_ch_is('&') => {
                self.offset += 2;
                self.result_token(Token::AndAnd)
            },
            Some('&') => {
                self.offset += 1;
                let c = self.get_current_ch();
                let tokenname = c.map_or(String::from("<eof>"),
                                         |x| format!("'{}'", x.to_string()));
                let msg = format!("expected another '&' but found {}", tokenname);
                self.result_error(Error::Plain(msg))
            },
            Some('|') if self.next_ch_is('|') => {
                self.offset += 2;
                self.result_token(Token::OrOr)
            },
            Some('|') => {
                self.offset += 1;
                let c = self.get_current_ch();
                let tokenname = c.map_or(String::from("<eof>"),
                                         |x| format!("'{}'", x.to_string()));
                let msg = format!("expected another '|' but found {}", tokenname);
                self.result_error(Error::Plain(msg))
            },
            Some('+') => {
                self.offset += 1;
                self.result_token(Token::Plus)
            },
            Some('-') => {
                self.offset += 1;
                match self.get_current_ch() {
                    Some('0'...'9') => {
                        let token_and_span = self.scan_literalint().unwrap().unwrap();
                        if let TokenAndSpan(Token::LiteralInt(value), _) = token_and_span {
                            self.result_token(Token::LiteralInt(-value))
                        } else {
                            unreachable!()
                        }
                    },
                    _ => self.result_token(Token::Minus)
                }
            },
            Some('/') if self.next_ch_is('/') => {
                self.scan_linecomment()
            },
            Some('/') if self.next_ch_is('*') => {
                self.scan_multilinecomment()
            },
            Some('/') => {
                self.offset += 1;
                self.result_token(Token::Slash)
            },
            Some('*') => {
                self.offset += 1;
                self.result_token(Token::Star)
            },
            Some('%') => {
                self.offset += 1;
                self.result_token(Token::Percent)
            },
            Some('A'...'Z') | Some('a'...'z') => {
                // this is the beginning of an identifier, but it could also be a keyword
                self.scan_ident_or_keyword()
            },
            Some('0'...'9') => {
                self.scan_literalint()
            },
            Some('\'') => {
                self.offset += 1;
                let c = self.get_current_ch();
                self.offset += 1;
                let e = self.get_current_ch();
                self.offset += 1;
                if e != Some('\'') {
                    self.result_error(Error::Expected(vec!("'")))
                } else {
                    self.result_token(Token::LiteralChar(c.unwrap()))
                }
            },
            Some(c) if c.is_whitespace() => {
                self.scan_whitespace()
            },
            Some(c) => {
                // this should now throw an error of some sort
                let next_char = self.source.get(self.offset + 1).map(|x| x.clone());
                let err = format!("unexpected {}, next to read: {:?}", c, next_char);
                self.result_error(Error::Plain(err))
            },
            None if !self.eof_reached => {
                // eof reached
                self.eof_reached = true;
                self.result_token(Token::Eof)
            },
            None => None,
        }
    }

    /// Build a Option<ScanResult> object containing a given Token object
    fn result_token(&self, token: Token) -> Option<ScanResult> {
        let span = Span {
            start: self.span_start,
            end: self.offset
        };
        Some(Ok(TokenAndSpan(token, span)))
    }

    /// Build a Option<ScanResult> object containing an error string
    fn result_error(&self, error: Error) -> Option<ScanResult> {
        let span = Span {
            start: self.span_start,
            end: self.offset
        };
        Some(Err(ErrorAndSpan(error, span)))
    }

    /// Get the character at the current offset in an option.
    /// If the result is None, this means that the end of the file is reached
    fn get_current_ch(&self) -> Option<char> {
        self.source.get(self.offset).map(|x| x.clone())
    }

    /// Check if the next character is equal to the one provided to this function
    fn next_ch_is(&self, expected: char) -> bool {
        let c = self.source.get(self.offset + 1).map(|x| x.clone());
        c.map_or(false, |x| x == expected)
    }

    /// Scan an identifier or a keyword
    fn scan_ident_or_keyword(&mut self) -> Option<ScanResult> {
        let mut word = String::new();

        // expect one of A-Z an a-z (no numbers or underscores)
        let c = self.get_current_ch().unwrap();
        assert!(match c {
            'A'...'Z' | 'a'...'z' => true,
            _ => false
        });
        word.push(c);
        self.offset += 1;

        // expect one of A-Z, a-z, numbers and underscores
        loop {
            match self.get_current_ch() {
                Some(c @ 'A'...'Z') | Some(c @ 'a'...'z') |
                Some(c @ '0'...'9') | Some(c @ '_') => {
                    // valid character
                    word.push(c);
                    self.offset += 1;
                },
                _ => {
                    break;
                }
            };
        }

        // word is constructed, is it a keyword?
        if let Some(keyword) = Scanner::make_keyword(&word) {
            self.result_token(keyword)
        } else {
            self.result_token(Token::Ident(word))
        }
    }

    /// Scan a literal integer token
    fn scan_literalint(&mut self) -> Option<ScanResult> {
        let mut word = String::new();

        loop {
            let c = self.get_current_ch();
            if c.map_or(false, |x| x.is_numeric()) {
                self.offset += 1;
                word.push(c.unwrap());
            } else {
                break;
            }
        }
        assert!(!word.is_empty());

        let value = word.parse().unwrap();
        self.result_token(Token::LiteralInt(value))
    }

    /// Scan a piece of whitespace (spaces, tabs, returns, newlines)
    fn scan_whitespace(&mut self) -> Option<ScanResult> {
        let mut word = String::new();

        loop {
            let c = self.get_current_ch();
            if c.map_or(false, |x| x.is_whitespace()) {
                self.offset += 1;
                word.push(c.unwrap());
            } else {
                break;
            }
        }

        assert!(!word.is_empty());

        self.result_token(Token::Whitespace(word))
    }

    /// Scan a single line comment (`// ...`)
    fn scan_linecomment(&mut self) -> Option<ScanResult> {
        let mut word = String::new();

        loop {
            let c = self.get_current_ch();
            if c.map_or(false, |x| x != '\n' && x != '\r') {
                self.offset += 1;
                word.push(c.unwrap());
            } else {
                break;
            }
        }

        assert!(!word.is_empty());

        self.result_token(Token::LineComment(word))
    }

    /// Scan a multiline comment (`/* ... */`)
    fn scan_multilinecomment(&mut self) -> Option<ScanResult> {
        let mut word = String::new();

        while !word.ends_with("*/") {
            if let Some(c) = self.get_current_ch() {
                word.push(c);
                self.offset += 1;
            } else {
                // got end of file, but comment did not end, throw an error
                let msg = String::from("multiline comment never ends");
                return self.result_error(Error::Plain(msg));
            }
        }
        assert!(word.starts_with("/*"));
        assert!(word.ends_with("*/"));

        self.result_token(Token::MultiLineComment(word))
    }

    /// Check if the provided string matches a keyword.
    /// This function will return the correct keyword token if it matches
    /// and it will return None if no match was found
    fn make_keyword(keyword_str: &str) -> Option<Token> {
        match keyword_str {
            "Bool" => Some(Token::Bool),
            "Char" => Some(Token::Char),
            "else" => Some(Token::Else),
            "False" => Some(Token::False),
            "fst" => Some(Token::Fst),
            "hd" => Some(Token::Hd),
            "if" => Some(Token::If),
            "Int" => Some(Token::Int),
            "return" => Some(Token::Return),
            "snd" => Some(Token::Snd),
            "tl" => Some(Token::Tl),
            "True" => Some(Token::True),
            "var" => Some(Token::Var),
            "Void" => Some(Token::Void),
            "while" => Some(Token::While),
            _ => None
        }
    }
}

impl Iterator for Scanner {
    type Item = ScanResult;

    fn next(&mut self) -> Option<ScanResult> {
        self.scan_once()
    }
}

/// filter predicate which excludes whitespace and comment tokens
pub fn useful_tokens(x: &ScanResult) -> bool {
    no_whitespace(x) && no_comments(x)
}

/// filter predicat which excludes tokens that are whitespace
pub fn no_whitespace(x: &ScanResult) -> bool {
    match x {
        &Ok(TokenAndSpan(Token::Whitespace(_), _)) => false,
        _ => true
    }
}

/// filter predicate which excludes tokens that are comments
pub fn no_comments(x: &ScanResult) -> bool {
    match x {
        &Ok(TokenAndSpan(Token::LineComment(_), _)) |
        &Ok(TokenAndSpan(Token::MultiLineComment(_), _)) => false,
        _ => true
    }
}

// vim: set et:sw=4:ts=4:ai:
