use ast;

macro_rules! unify_types {
    ($a:ident, $b:ident) => {{
        let opt = try_unification($a.clone(), $b.clone());
        if let Some((x, y)) = opt {
            *$a = x;
            *$b = y;
            return true;
        }
        false
    }};
}

macro_rules! unify_type {
    ($a:ident, $b:expr) => {{
        let opt = try_unification($a.clone(), $b);
        if let Some((x, y)) = opt {
            // only return true if the relevant type has been updated
            if x != *$a {
                *$a = x;
                return true;
            }
        }
        false
    }};
    ($a:expr, $b:ident) => {{
        let opt = try_unification($a, $b.clone());
        if let Some((x, y)) = opt {
            // only return true if the relevant type has been updated
            if y != *$b {
                *$b = y;
                return true;
            }
        }
        false
    }};
}


pub trait ToSPLType {
    fn to_spl_type(&self) -> Type;
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub enum Type {
    Variable(usize),
    Void,
    Int,
    Char,
    List(Box<Type>),
    Pair(Box<Type>, Box<Type>),
    Bool,
    Function(Vec<Box<Type>>, Box<Type>),
    Poly(String), // only used in polymorfic function templates
    // Noop: only used to annotate that this type is not relevant, can be used aa
    //       a placeholder in temporary (short-lived) types
    Noop,
}

// unification of free variables and inference

impl ast::SPL {
    pub fn unify_free_variables(&mut self) {
        let spl = self.clone();
        for decl in self.decls.iter_mut() {
            decl.unify_free_variables(&spl);
        }
    }

    pub fn try_inference(&mut self) {
        let spl = self.clone();
        loop {
            let mut any_inferred = false;
            for decl in self.decls.iter_mut() {
                any_inferred = any_inferred || decl.try_inference();
            }
            if !any_inferred {
                break;
            }
        }
    }
}

impl ast::Decl {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL) {
        match *self {
            ast::Decl::Var(ref mut x) => x.unify_free_variables(spl, None),
            ast::Decl::Fun(ref mut x) => x.unify_free_variables(spl),
        }
    }

    pub fn try_inference(&mut self) -> bool {
        match *self {
            ast::Decl::Var(ref mut x) => x.try_inference(),
            ast::Decl::Fun(ref mut x) => x.try_inference(),
        }
    }
}

impl ast::VarDecl {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.value.unify_free_variables(spl, fun_decl);
    }

    pub fn try_inference(&mut self) -> bool {
        self.value.try_inference() || {
            let mut ident = self.name.spl_type.borrow_mut();
            let mut value = self.value.borrow_mut_spl_type();
            unify_types!(ident, value)
        }
    }
}

impl ast::FunDecl {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL) {
        let fun_decl = self.clone();
        for vars in self.vars.iter_mut() {
            vars.unify_free_variables(spl, None);
        }
        for stmt in self.body.iter_mut() {
            stmt.unify_free_variables(spl, Some(&fun_decl));
        }
    }

    /// This function assumes that the type of the function is always known! Inference on
    /// function signatures or polymorphic function types will *NOT* be implemented!
    pub fn try_inference(&mut self) -> bool {
        for decl in self.vars.iter_mut() {
            if decl.try_inference() {return true};
        }
        let fun_decl = self.clone();
        for stmt in self.body.iter_mut() {
            if stmt.try_inference(&fun_decl) {return true};
        }
        false
    }
}

impl ast::Stmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        match *self {
            ast::Stmt::If(ref mut x)      => x.unify_free_variables(spl, fun_decl),
            ast::Stmt::While(ref mut x)   => x.unify_free_variables(spl, fun_decl),
            ast::Stmt::Ass(ref mut x)     => x.unify_free_variables(spl, fun_decl),
            ast::Stmt::FunCall(ref mut x) => x.unify_free_variables(spl, fun_decl),
            ast::Stmt::Return(ref mut x)  => x.unify_free_variables(spl, fun_decl),
        }
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        match *self {
            ast::Stmt::If(ref mut x)      => x.try_inference(fun_decl),
            ast::Stmt::While(ref mut x)   => x.try_inference(fun_decl),
            ast::Stmt::Ass(ref mut x)     => x.try_inference(fun_decl),
            ast::Stmt::FunCall(ref mut x) => x.try_inference(fun_decl),
            ast::Stmt::Return(ref mut x)  => x.try_inference(fun_decl),
        }
    }
}

impl ast::IfStmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.cond.unify_free_variables(spl, fun_decl);
        let iter = self.if_body.iter_mut().chain(self.else_body.iter_mut());
        for stmt in iter {
            stmt.unify_free_variables(spl, fun_decl);
        }
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        {
            let mut condition = self.cond.spl_type.borrow_mut();
            unify_type!(condition, Type::Bool);
        }
        if self.cond.try_inference() {return true};
        let iter = self.if_body.iter_mut().chain(self.else_body.iter_mut());
        for stmt in iter {
            if stmt.try_inference(fun_decl) {return true};
        }
        false
    }
}

impl ast::WhileStmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.cond.unify_free_variables(spl, fun_decl);
        for stmt in self.body.iter_mut() {
            stmt.unify_free_variables(spl, fun_decl);
        }
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        {
            let mut condition = self.cond.spl_type.borrow_mut();
            unify_type!(condition, Type::Bool);
        }
        self.cond.try_inference() || {
            for stmt in self.body.iter_mut() {
                if stmt.try_inference(fun_decl) {
                    return true
                };
            }
            false
        }
    }
}

impl ast::AssStmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.var_name.unify_free_variables(spl, fun_decl);
        self.value.unify_free_variables(spl, fun_decl);
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        use ast::FieldKind::*;

        if self.value.try_inference() {
            return true;
        }

        // We need to build a SPL type which will untimately match the identifier
        // on the left-hand side.  To do this, we will first generate the list of
        // fields specified on the lhs identifier.  Then, the abstract SPL type will
        // be built from the bottom up (in reverse field order).
        let mut field_kinds = Vec::new();
        let mut next_field = self.field.clone();
        while let Some(field) = next_field {
            next_field = field.next.clone().map(|x| *x);
            field_kinds.push(field.kind.clone());
        }

        let mut spl_type = self.value.borrow_spl_type().clone();
        for kind in field_kinds.into_iter().rev() {
            match kind {
                Hd => {
                    spl_type = Type::List(Box::new(spl_type));
                },
                Tl => {},
                Fst => {
                    spl_type = Type::Pair(Box::new(spl_type), Box::new(Type::Noop));
                },
                Snd => {
                    spl_type = Type::Pair(Box::new(Type::Noop), Box::new(spl_type));
                },
            }
        }

        let mut self_type = self.var_name.spl_type.borrow_mut();
        unify_type!(self_type, spl_type)
    }
}

impl ast::FunCallStmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.fun_call.unify_free_variables(spl, fun_decl)
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        // return value may be anything, so we don't need to unify self with the inner call
        self.fun_call.try_inference()
    }
}

impl ast::ReturnStmt {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        if let Some(ref mut value) = (*self).value {
            value.unify_free_variables(spl, fun_decl);
        }
    }

    pub fn try_inference(&mut self, fun_decl: &ast::FunDecl) -> bool {
        let fun_spl_type = fun_decl.name.spl_type.borrow();
        match self.value {
            Some(ref mut v) => {
                if v.try_inference() {
                    return true;
                }
                let mut ret_spl_type = v.borrow_mut_spl_type();
                if let Type::Function(_, fun_ret_spl_type) = fun_spl_type.clone() {
                    unify_type!(ret_spl_type, *fun_ret_spl_type);
                } else {
                    unreachable!()
                }
            },
            None => {
                if let Type::Function(_, fun_ret_spl_type) = fun_spl_type.clone() {
                    if *fun_ret_spl_type != Type::Void {
                        panic!("function {} should return void", fun_decl.name.clone());
                    }
                } else {
                    unreachable!()
                }

            },
        }
        false
    }
}

impl ast::Exp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        match *self {
            ast::Exp::Op1(ref mut x)       => x.unify_free_variables(spl, fun_decl),
            ast::Exp::Op2(ref mut x)       => x.unify_free_variables(spl, fun_decl),
            ast::Exp::Field(ref mut x)     => x.unify_free_variables(spl, fun_decl),
            ast::Exp::FunCall(ref mut x)   => x.unify_free_variables(spl, fun_decl),
            ast::Exp::Ident(ref mut x)     => x.unify_free_variables(spl, fun_decl),
            ast::Exp::Pair(ref mut x)      => x.unify_free_variables(spl, fun_decl),
            ast::Exp::Paren(ref mut x)     => x.unify_free_variables(spl, fun_decl),
            // the following types are all literal, and can therefore never contain idents
            ast::Exp::Int(ref mut x)       => {},
            ast::Exp::Char(ref mut x)      => {},
            ast::Exp::Bool(ref mut x)      => {},
            ast::Exp::EmptyList(ref mut x) => {},
        }
    }

    pub fn try_inference(&mut self) -> bool {
        match *self {
            ast::Exp::Op1(ref mut x)       => x.try_inference(),
            ast::Exp::Op2(ref mut x)       => x.try_inference(),
            ast::Exp::Field(ref mut x)     => x.try_inference(),
            ast::Exp::FunCall(ref mut x)   => x.try_inference(),
            ast::Exp::Pair(ref mut x)      => x.try_inference(),
            ast::Exp::Paren(ref mut x)     => x.try_inference(),
            // the following types are all primitives, so no inference can be applied
            ast::Exp::Ident(ref mut x)     => false,
            ast::Exp::Int(ref mut x)       => false,
            ast::Exp::Char(ref mut x)      => false,
            ast::Exp::Bool(ref mut x)      => false,
            ast::Exp::EmptyList(ref mut x) => false,
        }
    }
}

impl ast::ParenExp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.exp.unify_free_variables(spl, fun_decl);
    }

    pub fn try_inference(&mut self) -> bool {
        {
            let mut self_type = self.spl_type.borrow_mut();
            let mut exp = self.exp.borrow_mut_spl_type();
            unify_types!(self_type, exp);
        }
        self.exp.try_inference()
    }
}

impl ast::FieldExp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.var_name.unify_free_variables(spl, fun_decl);
    }

    pub fn try_inference(&mut self) -> bool {
        use ast::FieldKind::*;
        let mut kinds = vec!(self.field.kind.clone());
        let mut next_opt = self.field.next.clone();
        while let Some(field) = next_opt {
            kinds.push(field.kind.clone());
            next_opt = field.next.clone();
        }

        // try to infer towards self (up the AST)
        let mut t = self.var_name.spl_type.borrow().clone();
        for kind in kinds.iter() {
            match *kind {
                Hd => {
                    if let Type::List(inner) = t.clone() {
                        t = *inner;
                    } else {
                        panic!("'.hd' used on a non-list type")
                    }
                },
                Tl => {}, // type stays the same
                Fst => {
                    if let Type::Pair(fst, _) = t.clone() {
                        t = *fst;
                    } else {
                        panic!("'.fst' used on a non-pair type")
                    }
                },
                Snd => {
                    if let Type::Pair(_, snd) = t.clone() {
                        t = *snd;
                    } else {
                        panic!("'.snd' used on a non-pair type")
                    }
                }
            }
        };
        let mut self_type = self.spl_type.borrow_mut();
        unify_type!(self_type, t);

        // try to infer towards the identifier (down the AST)
        let mut t = self_type.clone();
        for kind in kinds.iter().rev() {
            match *kind {
                Hd => {
                    t = Type::List(Box::new(t));
                },
                Tl => {}, // type stays the same
                Fst => {
                    t = Type::Pair(Box::new(t), Box::new(Type::Noop));
                },
                Snd => {
                    t = Type::Pair(Box::new(Type::Noop), Box::new(t));
                }
            }
        };
        let mut ident = self.var_name.spl_type.borrow_mut();
        unify_type!(ident, t);

        false
    }
}

impl ast::Op2Exp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.left.unify_free_variables(spl, fun_decl);
        self.right.unify_free_variables(spl, fun_decl);
    }

    pub fn try_inference(&mut self) -> bool {
        use ast::Op2Kind::*;
        self.left.try_inference() || self.right.try_inference() || {
            let mut self_type = self.spl_type.borrow_mut();
            let mut left = self.left.borrow_mut_spl_type();
            let mut right = self.right.borrow_mut_spl_type();
            match self.op2.kind {
                Add | Sub | Mul | Div | Mod => { // arithmetic
                    unify_types!(left, right);
                    unify_types!(self_type, left);
                    unify_types!(self_type, right)
                },
                Eq | Lt | Gt | Le | Ge | Ne => {
                    unify_types!(left, right);
                    unify_type!(self_type, Type::Bool)
                },
                And | Or => {
                    unify_type!(left, Type::Bool);
                    unify_type!(right, Type::Bool);
                    unify_type!(self_type, Type::Bool)
                },
                Cons => {
                    let list_type = Type::List(Box::new(left.clone()));
                    unify_type!(self_type, right.clone());
                    unify_type!(self_type, list_type.clone());
                    unify_type!(right, self_type.clone());
                    unify_type!(right, list_type.clone());
                    match self_type.clone() {
                        Type::List(inner) => {
                            unify_type!(left, *inner);
                        },
                        _ => panic!("cons operator inferring to a non-list type!"),
                    }
                    match right.clone() {
                        Type::List(inner) => {
                            unify_type!(left, *inner);
                        },
                        _ => panic!("cons operator used with a non-list type at right hand side!"),
                    }
                    false
                },
            }
        }
    }
}


impl ast::Op1Exp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.operand.unify_free_variables(spl, fun_decl);
    }

    fn try_inference(&mut self) -> bool {
        self.operand.try_inference() || {
            let mut self_type = self.spl_type.borrow_mut();
            let mut other_type = self.operand.borrow_mut_spl_type();
            unify_types!(self_type, other_type)
        }
    }
}

impl ast::PairExp {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        self.fst.unify_free_variables(spl, fun_decl);
        self.snd.unify_free_variables(spl, fun_decl);
    }

    fn try_inference(&mut self) -> bool {
        self.fst.try_inference() || self.snd.try_inference() || {
            let mut self_type = self.spl_type.borrow_mut();
            let fst_type = self.fst.borrow_spl_type();
            let snd_type = self.snd.borrow_spl_type();
            let pair_type = Type::Pair(Box::new(fst_type.clone()), Box::new(snd_type.clone()));
            unify_type!(self_type, pair_type)
        }
    }
}

impl ast::FunCall {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        for mut act_arg in self.act_args.iter_mut() {
            act_arg.unify_free_variables(spl, fun_decl);
        }

        if self.fun_name.clone().name == "print" ||
           self.fun_name.clone().name == "putChar" ||
           self.fun_name.clone().name == "isEmpty" {
            return;
        }
        let ref mut ident = self.fun_name;
        if let Some(other) = search_environments_fun(spl, fun_decl, &ident.clone()) {
            let other_type = other.spl_type.clone();
            if ident.spl_type != other_type {
                ident.spl_type = other_type.clone();
            }
        } else {
            panic!("referencing undeclared function: `{}`", ident)
        }
    }

    pub fn try_inference(&mut self) -> bool {
        for mut act_arg in self.act_args.iter_mut() {
            if act_arg.try_inference() {return true}
        }

        if self.fun_name.clone().name == "print" ||
           self.fun_name.clone().name == "putChar" ||
           self.fun_name.clone().name == "isEmpty" {
            return false;
        }

        let fun_type = self.fun_name.spl_type.borrow();
        if let Type::Function(fun_args, ret) = fun_type.clone() {
            // unify acting arguments
            if fun_args.len() != self.act_args.len() {
                panic!("function {} is supplied a wrong amount of arguments", self.fun_name);
            }
            for (i, act_arg) in self.act_args.iter_mut().enumerate() {
                if act_arg.try_inference() {
                    return true;
                }
                let mut act_arg_type = act_arg.borrow_mut_spl_type();
                unify_type!(act_arg_type, *fun_args[i].clone());
            }
            // unify return value
            let mut self_type = self.spl_type.borrow_mut();
            unify_type!(self_type, *ret);
        } else {
            unreachable!();
        }
        false
    }
}

impl ast::Ident {
    pub fn unify_free_variables(&mut self, spl: &ast::SPL, fun_decl: Option<&ast::FunDecl>) {
        if let Some(other) = search_environments_var(spl, fun_decl, &self.clone()) {
            let other_type = other.spl_type.clone();
            if self.spl_type != other_type {
                self.spl_type = other.spl_type.clone();
            }
        } else {
            panic!("referencing undeclared variable `{}`", self)
        }
    }
}

// epic unification function

#[inline]
fn try_unification(a: Type, b: Type) -> Option<(Type, Type)> {
    fn error(a: Type, b: Type) -> ! {
        panic!("could not unify types: {:?}, {:?}", a, b);
    }

    match (a, b) {
        (Type::Noop, _) => None,
        (_, Type::Noop) => None,
        (Type::Variable(_), Type::Variable(_)) => None,
        (Type::Void, Type::Void) => None,
        (Type::Char, Type::Char) => None,
        (Type::Int, Type::Int) => None,
        (Type::Bool, Type::Bool) => None,
        (a, Type::Variable(_)) => Some((a.clone(), a)),
        (Type::Variable(_), b) => Some((b.clone(), b)),
        (Type::List(a), Type::List(b)) => {
            try_unification(*a, *b).map(|(x, y)| {
                (Type::List(Box::new(x)), Type::List(Box::new(y)))
            })
        },
        (Type::Pair(a0, a1), Type::Pair(b0, b1)) => {
            if let Some((t, u)) = try_unification(*a0.clone(), *b0.clone()) {
                Some((
                    Type::Pair(Box::new(t), a1),
                    Type::Pair(Box::new(u), b1),
                ))
            } else if let Some((t, u)) = try_unification(*a1, *b1) {
                Some((
                    Type::Pair(a0.clone(), Box::new(t)),
                    Type::Pair(b0.clone(), Box::new(u)),
                ))
            } else {
                None
            }
        },
        (Type::Void, other) => error(Type::Void, other),
        (Type::Char, other) => error(Type::Char, other),
        (Type::Int, other) => error(Type::Int, other),
        (Type::Bool, other) => error(Type::Bool, other),
        (other, Type::Void) => error(other, Type::Void),
        (other, Type::Char) => error(other, Type::Char),
        (other, Type::Int) => error(other, Type::Int),
        (other, Type::Bool) => error(other, Type::Bool),
        (x, y) => panic!("unification of {:?} and {:?} is not implemented", x, y),
    }
}


// functions to search in the AST environments

fn search_environments_var<'a>(spl: &'a ast::SPL,
                               fun_decl: Option<&'a ast::FunDecl>,
                               ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    if let Some(fun_decl) = fun_decl {
        None.or(search_local_environment_var(fun_decl, ident))
            .or(search_fun_environment_var(fun_decl, ident))
            .or(search_global_environment_var(spl, ident))
    } else {
        search_global_environment_var(spl, ident)
    }
}

fn search_environments_fun<'a>(spl: &'a ast::SPL,
                               _: Option<&'a ast::FunDecl>,
                               ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    None.or(search_global_environment_fun(spl, ident))
        .or(search_builtin_fun(spl, ident))
}

pub fn search_local_environment_var<'a>(fun_decl: &'a ast::FunDecl,
                                    ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    // get variable declerations
    for var_decl in fun_decl.vars.iter() {
        if var_decl.name.name == ident.name {
            return Some(&var_decl.name);
        }
    }
    None
}

pub fn search_fun_environment_var<'a>(fun_decl: &'a ast::FunDecl,
                                  ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    // get variable declerations
    for arg_ident in fun_decl.args.iter() {
        if arg_ident.name == ident.name {
            return Some(&arg_ident);
        }
    }
    None
}

pub fn search_global_environment_var<'a>(spl: &'a ast::SPL,
                                     ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    // get variable declerations
    for decl in spl.decls.iter() {
        if let &ast::Decl::Var(ref var_decl) = decl {
            if var_decl.name.name == ident.name {
                return Some(&var_decl.name);
            }
        }
    }
    None
}

fn search_global_environment_fun<'a>(spl: &'a ast::SPL,
                                     ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    // get variable declerations
    for decl in spl.decls.iter() {
        if let &ast::Decl::Fun(ref fun_decl) = decl {
            if fun_decl.name.name == ident.name {
                return Some(&fun_decl.name);
            }
        }
    }
    None
}

fn search_builtin_fun<'a>(spl: &'a ast::SPL,
                          ident: &'a ast::Ident) -> Option<&'a ast::Ident> {
    let alt_name = format!("__builtin__.{}", ident.name);
    for decl in spl.decls.iter() {
        if let &ast::Decl::Fun(ref fun_decl) = decl {
            if fun_decl.name.name == alt_name {
                return Some(&fun_decl.name);
            }
        }
    }
    None
}

fn search_fun_environment_type<'a>(fun_decl: &'a ast::FunDecl,
                                   ident: &'a ast::Ident,
                                   until_argument: Option<usize>) -> Option<&'a ast::Ident> {
    if let Some(ref fun_type) = fun_decl.fun_type {
        for arg_type in fun_type.args.iter().take(until_argument.unwrap_or(fun_type.args.len())) {
            if let &ast::Type::Ident(ref arg) = arg_type {
                if arg.name == ident.name {
                    return Some(&arg);
                }
            }
        }
        if until_argument.unwrap_or(fun_type.args.len()) > fun_type.args.len() {
            if let ast::RetType::Type(ref ret_type) = fun_type.ret {
                if let &ast::Type::Ident(ref ret) = ret_type {
                    if ret.name == ident.name {
                        return Some(&ret);
                    }
                }
            }
        }
    }
    None
}
