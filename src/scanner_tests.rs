#![allow(non_snake_case, unused_imports)]

use scanner::{Scanner,TokenAndSpan,ScanResult,no_whitespace,no_comments,useful_tokens};
use scanner::Token::*;

use util::{Error, Span};

macro_rules! assert_scan_eq {
    ($source:expr, $( $expect:expr ),* ) => {{
        let mut scanner = Scanner::new($source).filter(no_whitespace);
        $(
            assert_eq!(scanner.next(), $expect);
        )*
        assert_eq!(scanner.next().unwrap().unwrap().0, Eof);
        assert_eq!(scanner.next(), None);
    }};
}

macro_rules! assert_scan_expect_token {
    ($source:expr, $( $expect:expr ),* ) => {{
        let mut scanner = Scanner::new($source).filter(no_whitespace);
        $(
            let token = scanner.next().unwrap().unwrap().0;
            assert_eq!(token, $expect);
        )*
        assert_eq!(scanner.next().unwrap().unwrap().0, Eof);
        assert_eq!(scanner.next(), None);
    }}
}

macro_rules! assert_scan_expect_error {
    ($source:expr, $( $expected_error:expr ),* ) => {{
        let mut scanner = Scanner::new($source);
        $(
            let error = scanner.next().unwrap().unwrap_err().0;
            assert_eq!(error, $expected_error);
        )*
    }}
}

#[test]
fn test_scan_basic() {
    assert_scan_expect_token!("",);
    assert_scan_expect_token!("=", Eq);
    assert_scan_expect_token!(";", Semicolon);
    assert_scan_expect_token!("::", DoubleColon);
    assert_scan_expect_token!("(", LeftParen);
    assert_scan_expect_token!(")", RightParen);
    assert_scan_expect_token!("{", LeftBrace);
    assert_scan_expect_token!("}", RightBrace);
    assert_scan_expect_token!("[", LeftBracket);
    assert_scan_expect_token!("]", RightBracket);
    assert_scan_expect_token!(",", Comma);
    assert_scan_expect_token!(".", Dot);
    assert_scan_expect_token!("->", Arrow);

    // Keywords
    assert_scan_expect_token!("Bool", Bool);
    assert_scan_expect_token!("Char", Char);
    assert_scan_expect_token!("else", Else);
    assert_scan_expect_token!("False", False);
    assert_scan_expect_token!("fst", Fst);
    assert_scan_expect_token!("hd", Hd);
    assert_scan_expect_token!("if", If);
    assert_scan_expect_token!("Int", Int);
    assert_scan_expect_token!("return", Return);
    assert_scan_expect_token!("snd", Snd);
    assert_scan_expect_token!("tl", Tl);
    assert_scan_expect_token!("True", True);
    assert_scan_expect_token!("var", Var);
    assert_scan_expect_token!("Void", Void);
    assert_scan_expect_token!("while", While);

    // Op{1,2}
    assert_scan_expect_token!("!", Exclamation);
    assert_scan_expect_token!("+", Plus);
    assert_scan_expect_token!("-", Minus);
    assert_scan_expect_token!("*", Star);
    assert_scan_expect_token!("/", Slash);
    assert_scan_expect_token!("%", Percent);
    assert_scan_expect_token!("==", EqEq);
    assert_scan_expect_token!("<", Lt);
    assert_scan_expect_token!(">", Gt);
    assert_scan_expect_token!("<=", Le);
    assert_scan_expect_token!(">=", Ge);
    assert_scan_expect_token!("!=", Ne);
    assert_scan_expect_token!("&&", AndAnd);
    assert_scan_expect_error!("&", Error::Plain(String::from("expected another '&' but found <eof>")));
    assert_scan_expect_token!("||", OrOr);
    assert_scan_expect_error!("|", Error::Plain(String::from("expected another '|' but found <eof>")));
    assert_scan_expect_token!(":", Colon);

    // id, char, int
    assert_scan_expect_token!("x", Ident(String::from("x")));
    assert_scan_expect_token!("X", Ident(String::from("X")));
    assert_scan_expect_token!("under_score", Ident(String::from("under_score")));
    assert_scan_expect_token!("3", LiteralInt(3));
    assert_scan_expect_token!("-3", LiteralInt(-3));
    assert_scan_expect_token!("- 3", Minus, LiteralInt(3));

    // whitespace (should be skipped)
    assert_scan_expect_token!(" ",);
    assert_scan_expect_token!("\t",);
    assert_scan_expect_token!("\r",);
    assert_scan_expect_token!("\n",);

    // LiteralChar
    assert_scan_expect_token!("'a'", LiteralChar('a'));
    assert_scan_expect_error!("'aa'", Error::Expected(vec!("'")));

    // comments (should be skipped)
    assert_scan_expect_token!("// some comment",
                              LineComment(String::from("// some comment")));
    assert_scan_expect_token!("/* some comment */",
                              MultiLineComment(String::from("/* some comment */")));
}

#[test]
fn test_filter_predicates() {
    let dummyspan = Span { start: 0, end: 0 };
    let output = || {
        vec!(
            Ok(TokenAndSpan(LineComment(String::from("// ...")), dummyspan.clone())),
            Ok(TokenAndSpan(LiteralInt(3), dummyspan.clone())),
            Ok(TokenAndSpan(MultiLineComment(String::from("/* ... */")), dummyspan.clone())),
            Ok(TokenAndSpan(Whitespace(String::from("\n")), dummyspan.clone()))
        )
    };

    // test no_comments filter
    let no_comments_output: Vec<ScanResult> = output().into_iter().filter(no_comments).collect();
    assert_eq!(no_comments_output.get(0), output().get(1));
    assert_eq!(no_comments_output.get(1), output().get(3));
    assert_eq!(no_comments_output.get(2), None);

    // test no_whitespace filter
    let no_whitespace: Vec<ScanResult> = output().into_iter().filter(no_whitespace).collect();
    assert_eq!(no_whitespace.get(0), output().get(0));
    assert_eq!(no_whitespace.get(1), output().get(1));
    assert_eq!(no_whitespace.get(2), output().get(2));
    assert_eq!(no_whitespace.get(3), None);

    // test useful_tokens filter
    let useful_tokens_output: Vec<ScanResult> = output().into_iter().filter(useful_tokens).collect();
    assert_eq!(useful_tokens_output.get(0), output().get(1));
    assert_eq!(useful_tokens_output.get(1), None);
}

#[test]
fn test_scan_facR_function() {
    assert_scan_expect_token!(
        "/*
        Three ways to implement the factorial function in SPL.
        First the recursive version.
        */
        facR ( n ) :: Int -> Int {
            if ( n < 2 ) {
                return 1;
            } else {
                return n * facR ( n - 1 );
            }
        }
        ",
        // function declaration
        MultiLineComment(String::from("/*
        Three ways to implement the factorial function in SPL.
        First the recursive version.
        */")), Ident(String::from("facR")), LeftParen, Ident(String::from("n")),
        RightParen, DoubleColon, Int, Arrow, Int,
        LeftBrace,
            // if statement
            If, LeftParen, Ident(String::from("n")), Lt,
            LiteralInt(2), RightParen,
            LeftBrace,
                Return, LiteralInt(1), Semicolon,
            RightBrace, Else, LeftBrace,
                Return, Ident(String::from("n")), Star, Ident(String::from("facR")), LeftParen,
                Ident(String::from("n")), Minus, LiteralInt(1), RightParen, Semicolon,
            RightBrace,
        RightBrace);
}

#[test]
fn test_scan_facl_function() {
    assert_scan_expect_token!(
        "
        facl (n) :: Int -> Int {
            var r = 1;
            while ( n > 1 ) {
                r = r * n;
                n = n - 1;
            }
            return r;
        }
        ",
        Ident(String::from("facl")), LeftParen, Ident(String::from("n")), RightParen, DoubleColon, Int, Arrow, Int,
        LeftBrace,
            Var, Ident(String::from("r")), Eq, LiteralInt(1), Semicolon,
            While, LeftParen, Ident(String::from("n")), Gt, LiteralInt(1), RightParen,
            LeftBrace,
                Ident(String::from("r")), Eq, Ident(String::from("r")), Star, Ident(String::from("n")), Semicolon,
                Ident(String::from("n")), Eq, Ident(String::from("n")), Minus, LiteralInt(1), Semicolon,
            RightBrace,
            Return, Ident(String::from("r")), Semicolon,
        RightBrace
    )
}

#[test]
fn test_scan_main_function() {
    assert_scan_expect_token!(
        "
        main () :: Void {
            var n = 0;
            var facN = 1;
            var ok = True;
            while ( n < 20 ) {
                facN = facR ( n );
                if ( facN != facl(n) || facN != facL(n) ) {
                    print (n : facN : facl(n) : facL(n) : []);
                    ok  = False;
                }
                n = n + 1;
            }
            print(ok);
        }
        ",
        Ident(String::from("main")), LeftParen, RightParen, DoubleColon, Void,
        LeftBrace,
            Var, Ident(String::from("n")), Eq, LiteralInt(0), Semicolon, Var,
            Ident(String::from("facN")), Eq, LiteralInt(1), Semicolon, Var,
            Ident(String::from("ok")), Eq, True, Semicolon,
            While, LeftParen, Ident(String::from("n")), Lt, LiteralInt(20), RightParen,
            LeftBrace,
                Ident(String::from("facN")), Eq, Ident(String::from("facR")), LeftParen,
                Ident(String::from("n")), RightParen, Semicolon, If, LeftParen,
                Ident(String::from("facN")), Ne, Ident(String::from("facl")), LeftParen,
                Ident(String::from("n")), RightParen, OrOr, Ident(String::from("facN")), Ne,
                Ident(String::from("facL")), LeftParen, Ident(String::from("n")), RightParen,
                RightParen,
                LeftBrace,
                    Ident(String::from("print")), LeftParen, Ident(String::from("n")), Colon,
                    Ident(String::from("facN")), Colon, Ident(String::from("facl")), LeftParen,
                    Ident(String::from("n")), RightParen, Colon, Ident(String::from("facL")),
                    LeftParen, Ident(String::from("n")), RightParen, Colon, LeftBracket,
                    RightBracket, RightParen, Semicolon, Ident(String::from("ok")), Eq, False,
                    Semicolon,
                RightBrace,
                Ident(String::from("n")), Eq, Ident(String::from("n")), Plus, LiteralInt(1),
                Semicolon,
            RightBrace,
            Ident(String::from("print")), LeftParen, Ident(String::from("ok")), RightParen,
            Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_facL_function() {
    assert_scan_expect_token!(
        "
        facL (n) :: Int -> Int {
            return product(fromTo(1, n));
        }
        ",
        Ident(String::from("facL")), LeftParen, Ident(String::from("n")), RightParen, DoubleColon, Int, Arrow, Int,
        LeftBrace,
            Return, Ident(String::from("product")), LeftParen, Ident(String::from("fromTo")), LeftParen, LiteralInt(1),
            Comma, Ident(String::from("n")), RightParen, RightParen, Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_product_function() {
    assert_scan_expect_token!(
        "
        product (list) :: [Int] -> Int {
            if ( isEmpty(list) ) {
                return 1;
            } else {
                return list.hd * product(list.tl);
            }
        }
        ",
        Ident(String::from("product")), LeftParen, Ident(String::from("list")), RightParen,
        DoubleColon, LeftBracket, Int, RightBracket, Arrow, Int,
        LeftBrace,
            If, LeftParen, Ident(String::from("isEmpty")), LeftParen, Ident(String::from("list")),
            RightParen, RightParen,
            LeftBrace,
                Return, LiteralInt(1), Semicolon,
            RightBrace, Else, LeftBrace,
                Return, Ident(String::from("list")), Dot, Hd, Star,
                Ident(String::from("product")), LeftParen, Ident(String::from("list")),
                Dot, Tl, RightParen, Semicolon,
            RightBrace,
        RightBrace
    );
}

#[test]
fn test_scan_fromTo_function() {
    assert_scan_expect_token!(
        "
        fromTo (from, to) :: Int Int -> [Int] {
            if ( from <= to ) {
                return from : fromTo(from + 1, to);
            } else {
                return [];
            }
        }
        ",
        Ident(String::from("fromTo")), LeftParen, Ident(String::from("from")), Comma,
        Ident(String::from("to")), RightParen, DoubleColon, Int, Int, Arrow, LeftBracket, Int,
        RightBracket,
        LeftBrace,
            If, LeftParen, Ident(String::from("from")), Le, Ident(String::from("to")), RightParen,
            LeftBrace,
                Return, Ident(String::from("from")), Colon, Ident(String::from("fromTo")),
                LeftParen, Ident(String::from("from")), Plus, LiteralInt(1), Comma,
                Ident(String::from("to")), RightParen, Semicolon,
            RightBrace, Else, LeftBrace,
                Return, LeftBracket, RightBracket, Semicolon,
            RightBrace,
        RightBrace
    );
}

#[test]
fn test_scan_reverse_function() {
    assert_scan_expect_token!(
        "
        reverse (list) :: [t] -> [t] {
            var accu = [];
            while ( !isEmpty(list) ) {
                accu = list.hd : accu;
                list = list.tl;
            }
            return accu;
        }
        ",
        Ident(String::from("reverse")), LeftParen, Ident(String::from("list")), RightParen,
        DoubleColon, LeftBracket, Ident(String::from("t")), RightBracket, Arrow, LeftBracket,
        Ident(String::from("t")), RightBracket,
        LeftBrace,
            Var, Ident(String::from("accu")), Eq, LeftBracket, RightBracket, Semicolon,
            While, LeftParen, Exclamation, Ident(String::from("isEmpty")), LeftParen,
            Ident(String::from("list")), RightParen, RightParen,
            LeftBrace,
                Ident(String::from("accu")), Eq, Ident(String::from("list")), Dot, Hd, Colon,
                Ident(String::from("accu")), Semicolon,
                Ident(String::from("list")), Eq, Ident(String::from("list")), Dot, Tl, Semicolon,
            RightBrace,
            Return, Ident(String::from("accu")), Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_abs_function() {
    assert_scan_expect_token!(
        "
        abs (n) :: Int -> Int {
            if (n < 0) return -n;
            else return n;
        }
        ",
        Ident(String::from("abs")), LeftParen, Ident(String::from("n")), RightParen, DoubleColon,
        Int, Arrow, Int,
        LeftBrace,
            If, LeftParen, Ident(String::from("n")), Lt, LiteralInt(0), RightParen,
                Return, Minus, Ident(String::from("n")), Semicolon,
            Else, Return, Ident(String::from("n")), Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_swapCopy_function() {
    assert_scan_expect_token!(
        "
        swapCopy (pair) :: (a, b) -> (b, a) {
            return (pair.snd, pair.fst);
        }
        ",
        Ident(String::from("swapCopy")), LeftParen, Ident(String::from("pair")), RightParen,
        DoubleColon, LeftParen, Ident(String::from("a")), Comma, Ident(String::from("b")),
        RightParen, Arrow, LeftParen, Ident(String::from("b")), Comma, Ident(String::from("a")),
        RightParen,
        LeftBrace,
            Return, LeftParen, Ident(String::from("pair")), Dot, Snd, Comma,
            Ident(String::from("pair")), Dot, Fst, RightParen, Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_swap_function() {
    assert_scan_expect_token!(
        "
        swap (tuple) :: (a, a) -> (a, a) {
            var tmp = tuple.fst;
            tuple.fst = tuple.snd;
            tuple.snd = tmp;
            return tuple;
        }
        ",
        Ident(String::from("swap")), LeftParen, Ident(String::from("tuple")), RightParen,
        DoubleColon, LeftParen, Ident(String::from("a")), Comma, Ident(String::from("a")),
        RightParen, Arrow, LeftParen, Ident(String::from("a")), Comma, Ident(String::from("a")),
        RightParen,
        LeftBrace,
            Var, Ident(String::from("tmp")), Eq, Ident(String::from("tuple")), Dot, Fst,
            Semicolon,
            Ident(String::from("tuple")), Dot, Fst, Eq, Ident(String::from("tuple")), Dot, Snd,
            Semicolon,
            Ident(String::from("tuple")), Dot, Snd, Eq, Ident(String::from("tmp")), Semicolon,
            Return, Ident(String::from("tuple")), Semicolon,
        RightBrace
    );
}

#[test]
fn test_scan_append_function() {
    assert_scan_expect_token!(
        "
        append (l1, l2) :: [t] [t] -> [t] {
            if ( isEmpty(l1) ) {
                return l2;
            } else {
                l1.tl = append(l1.tl, l2);
                return l1;
            }
        }
        ",
        Ident(String::from("append")), LeftParen, Ident(String::from("l1")), Comma,
        Ident(String::from("l2")), RightParen, DoubleColon, LeftBracket, Ident(String::from("t")),
        RightBracket, LeftBracket, Ident(String::from("t")), RightBracket, Arrow, LeftBracket,
        Ident(String::from("t")), RightBracket,
        LeftBrace,
            If, LeftParen, Ident(String::from("isEmpty")), LeftParen, Ident(String::from("l1")),
            RightParen, RightParen,
            LeftBrace,
                Return, Ident(String::from("l2")), Semicolon,
            RightBrace, Else, LeftBrace,
                Ident(String::from("l1")), Dot, Tl, Eq, Ident(String::from("append")),
                LeftParen, Ident(String::from("l1")), Dot, Tl, Comma, Ident(String::from("l2")),
                RightParen, Semicolon,
                Return, Ident(String::from("l1")), Semicolon,
            RightBrace,
        RightBrace
    )
}
#[test]
fn test_scan_squareOddNumbers_function() {
    assert_scan_expect_token!(
        "
        squareOddNumbers (list) :: [Int] -> [Int] {
            while ( !isEmpty(list) && list.hd % 2 == 0 ) {
                list = list.tl;
            }
            if ( !isEmpty(list) ) {
                list.hd = list.hd * list.hd;
                list.tl = squareOddNumbers(list.tl);
            }
            return list;
        }
        ",
        Ident(String::from("squareOddNumbers")), LeftParen, Ident(String::from("list")),
        RightParen, DoubleColon, LeftBracket, Int, RightBracket, Arrow, LeftBracket, Int,
        RightBracket,
        LeftBrace,
            While, LeftParen, Exclamation, Ident(String::from("isEmpty")), LeftParen,
            Ident(String::from("list")), RightParen, AndAnd, Ident(String::from("list")),
            Dot, Hd, Percent, LiteralInt(2), EqEq, LiteralInt(0), RightParen,
            LeftBrace,
                Ident(String::from("list")), Eq, Ident(String::from("list")), Dot, Tl, Semicolon,
            RightBrace,
            If, LeftParen, Exclamation, Ident(String::from("isEmpty")), LeftParen,
            Ident(String::from("list")), RightParen, RightParen,
            LeftBrace,
                Ident(String::from("list")), Dot, Hd, Eq, Ident(String::from("list")), Dot,
                Hd, Star, Ident(String::from("list")), Dot, Hd, Semicolon,
                Ident(String::from("list")), Dot, Tl, Eq, Ident(String::from("squareOddNumbers")),
                LeftParen, Ident(String::from("list")), Dot, Tl, RightParen, Semicolon,
            RightBrace,
            Return, Ident(String::from("list")), Semicolon,
        RightBrace
    );
}
