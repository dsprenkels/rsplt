use std::fmt;
use std::fmt::Write;
use std::ops::BitOr;
use std::process;

use ansi_term::{Colour, Style};

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct Span {
    pub start: usize,
    pub end: usize
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Error {
    Plain(String),
    Expected(Vec<&'static str>)
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &Error::Plain(ref x) => write!(f, "{}", x),
            &Error::Expected(ref xs) => {
                assert!(!xs.is_empty());
                try!(write!(f, "expected "));
                if xs.len() == 1 {
                    write!(f, "{}", xs.first().unwrap())
                } else {
                    assert!(xs.len() >= 2);
                    let (lst, xs) = xs.split_last().unwrap();
                    let (fst, xs) = xs.split_first().unwrap();
                    let mut disp_string = String::new();
                    try!(write!(disp_string, "expected {}", fst));
                    for x in xs {
                        try!(write!(disp_string, ", {}", x));
                    }
                    try!(write!(disp_string, " or {}", lst));
                    write!(f, "{}", disp_string)
                }
            }
        }
    }
}

#[derive(Debug, PartialEq, Eq, Clone)]
pub struct ErrorAndSpan(pub Error, pub Span);

impl BitOr for ErrorAndSpan {
    type Output = ErrorAndSpan;

    fn bitor(self, _rhs: ErrorAndSpan) -> ErrorAndSpan {
        match () {
            _ if self.1.start < _rhs.1.start => _rhs,
            _ if self.1.start > _rhs.1.start => self,
            _ if self.1.end < _rhs.1.end => _rhs,
            _ if self.1.end > _rhs.1.end => self,
            _ => {
                match (&self.0, &_rhs.0) {
                    (&Error::Plain(_), &Error::Expected(_)) => self,
                    (&Error::Expected(_), &Error::Plain(_)) => _rhs,
                    (&Error::Plain(_), &Error::Plain(_)) => self,
                    (&Error::Expected(_), &Error::Expected(_)) => {
                        if let (Error::Expected(ref expected1), Error::Expected(ref expected2)) =
                                    (self.0, _rhs.0) {
                            let mut new_expected = Vec::new();
                            for x in expected1.iter().chain(expected2.iter()) {
                                if !new_expected.contains(x) {
                                    new_expected.push(x.clone());
                                }
                            }
                            let error = Error::Expected(new_expected);
                            let span = self.1.clone();
                            ErrorAndSpan(error, span)
                        } else {
                            unreachable!()
                        }
                    }
                }
            }
        }
    }
}

#[inline]
// Inline, because a lot of times, clone() operations happen that can
// be short-circuited by the optimizer
pub fn combine_spans(a: &Span, b: &Span) -> Span {
    Span {
        start: a.start,
        end: b.end
    }
}

pub fn error(msg: &str) -> ! {
    println!("error: {}", msg);
    process::exit(1);
}

/// This function will print a nice error message at the given span
pub fn span_error(source: &str, eas: &ErrorAndSpan) -> ! {
    let &ErrorAndSpan(ref error, Span {start, end}) = eas;
    let (start_line, start_col) = get_line_col(source, start);
    let (end_line, end_col) = get_line_col(source, end);
    let linecol = format!("{}:{}", start_line + 1, start_col);
    let error_msg = format!("{}", error);
    println!("{}: {}", Colour::Red.paint("error"), Style::new().bold().paint(error_msg));
    println!("{} {}", linecol, source.lines().nth(start_line).unwrap_or("(end-of-file)"));

    assert!(start_line <= end_line);
    if start_line + 1 < end_line {
        unimplemented!();
    }

    let span_line = if start_line + 1 == end_line {
        String::from("^")
    } else {
        format!("^{}", repeat_string("~", end_col - start_col - 1))
    };
    println!("{} {}{}", repeat_string(" ", linecol.len()),
                        repeat_string(" ", start_col - 1),
                        Colour::Red.paint(span_line));
    process::exit(1);
}

fn get_line_col(source: &str, offset: usize) -> (usize, usize) {
    let mut line = 0;
    let mut col = 0;
    for (i, c) in source.char_indices() {
        if i > offset {
            break;
        }
        if c == '\n' {
            line += 1;
            col = 0;
        } else {
            col += 1;
        }
    }
    (line, col)
}

fn repeat_string(s: &str, x: usize) -> String {
    (0..x).map(|_| s).collect::<String>()
}
