#![allow(dead_code, unused_variables, unused_mut)]

extern crate ansi_term;
extern crate docopt;
extern crate rustc_serialize;

mod ast;
mod util;
#[macro_use] mod parser;
mod scanner;
mod ast_tests;
mod parser_tests;
mod scanner_tests;
mod infer;
mod infer_tests;
mod codegen;
mod llvm;

use std::fs;
use std::io;
use std::io::Read;
use std::process;
use std::io::Write;

use codegen::*;

use ansi_term::Colour;
use docopt::Docopt;

const USAGE: &'static str = "
Usage:
  rsplt [options] <input>
  rsplt [options]

Options:
  -o <filename>       Write the output to <filename>
  --exec              JIT compile and execute the input file
  --pretty-print      Pretty print the output, instead of compiling
  --emit-llvm         Compile the input file to LLVM IR (version 3.6)
  --emit-ssm          Compile the input file to SSM
  -h --help           Display this message
  --version           Print version info and exit
  -v --verbose        Use verbose output
";

struct SimpleLogger;

#[derive(Debug, RustcDecodable)]
struct Args {
    arg_input: String,
    flag_help: bool,
    flag_exec: bool,
    flag_emit_llvm: bool,
    flag_emit_ssm: bool,
    flag_pretty_print: bool,
    flag_verbose: bool,
    flag_version: bool,
}

fn main() {
    let args: Args = Docopt::new(USAGE)
                            .and_then(|d| d.decode())
                            .unwrap_or_else(|e| e.exit());

    if args.flag_version {
        show_version_and_exit();
    }

    if args.arg_input == "" {
        util::error("No input file given");
    }

    let source = match read_file_contents(args.arg_input) {
        Ok(x) => x,
        Err(error) => util::error(&format!("{}", error))
    };

    // get scan tokens
    let mut tokens = Vec::new();
    let scanner = scanner::Scanner::new(&source[..]).filter(scanner::useful_tokens);
    for token_result in scanner {
        match token_result {
            Ok(token) => tokens.push(token),
            Err(msg) => util::span_error(&source, &msg)
        }
    }
    let tokens_str = format!("{:?}", tokens);

    let mut parser = parser::Parser::new(tokens);
    let mut ast = match parser.parse_spl() {
        Ok(ast) => ast,
        Err(eas) => {
            util::span_error(&source, &eas)
        }
    };

    if args.flag_exec {
        ast.unify_free_variables();
        ast.try_inference();
        let llvm = llvm::Context::from(ast);
        exec_and_exit(llvm);
    }

    if args.flag_emit_llvm {
        ast.unify_free_variables();
        ast.try_inference();
        let llvm = llvm::Context::from(ast);
        emit_llvm_and_exit(llvm);
    }

    if args.flag_pretty_print {
        pretty_print_and_exit(ast);
    }

    if args.flag_emit_ssm {
        ast.unify_free_variables();
        let ssm = ast.to_ssm_code();
        emit_ssm_and_exit(ssm);
    }

}

fn show_version_and_exit() -> ! {
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");
    println!("rsplt {}", VERSION);
    process::exit(0);
}

fn exec_and_exit(llvm: llvm::Context) -> ! {
    let status = {
        let piped = process::Stdio::piped();
        let inherit = process::Stdio::inherit();
        let mut lli = process::Command::new("/usr/bin/lli-3.6");
        let mut child = lli.stdin(piped)
                       .stdout(inherit)
                       .spawn()
                       .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
        let llvm_str = llvm.to_string();
        let lli_input = llvm_str.as_bytes();
        child.stdin.as_mut().unwrap().write(lli_input).unwrap();
        child.wait().unwrap()
    };
    process::exit(status.code().unwrap_or(0));
}

fn emit_llvm_and_exit(llvm: llvm::Context) -> ! {
    print!("{}", llvm);
    process::exit(0);
}

fn emit_ssm_and_exit(ssm: Vec<SSMLine>) -> ! {
    for (count, v) in ssm.iter().enumerate() {
        match *v {
            SSMLine::SSMLabel(ref x) => {},
            _                        => { print!("\t"); },
        }
        println!("{}", v);
    }
    process::exit(0);
}

fn pretty_print_and_exit(ast: ast::SPL) -> ! {
    print!("{}", ast);
    process::exit(0);
}

fn read_file_contents(filename: String) -> io::Result<String> {
    let mut f = try!(fs::File::open(filename));
    let mut ret = String::new();
    try!(f.read_to_string(&mut ret));
    Ok(ret)
}
