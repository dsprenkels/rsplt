use std::cell::RefCell;
use std::char::from_u32;
use std::rc::Rc;

use ast::*;
use infer;
use infer::ToSPLType;
use scanner::{Token, TokenAndSpan};
use util::{combine_spans, Error, ErrorAndSpan, Span};


/// Wrap this around an attempt at parsing some tree token, this will
/// immediatly return out of the current scope if the wrapped function call
/// returns an error.
/// Refer to the Rust documentation of the `try!` macro for more
/// information on this type of pattern
macro_rules! try_parse {
    ($ret:expr) => {{
        match $ret {
            Ok((parse, offset)) => (parse, offset),
            Err(err) => return Err(err)
        }
    }}
}

/// Wrap this around an attempt at parsing some tree token, this will
/// immediatly return out of the current scope if the wrapped function call
/// returns an a succesful parse. This returned value will be wrapped in the
/// enum path provided as the first argument to this macro.
/// If no enum path is provided, the macro will not wrap the result.
///
/// Refer to the Rust documentation of the `try!` macro for more
/// information on this type of pattern.
///
/// This macro can be used as a parser combinator, when at the current state
/// multiple parses are possible. For example:
///
/// ```rust
/// yield_parse!(LiteralBool::True, self.parse_literal_bool_true(offset));
/// yield_parse!(LiteralBool::False, self.parse_literal_bool_false(offset));
/// ```
///
/// will make this function return Ok((LiteralBool::...(_), _)) if the
/// following token is a True or False keyword.
/// If no parser fits, the rest of the current function (below the macro
/// invocations) will be executed normally. This would be a nice spot to
/// return an Err(_).
macro_rules! yield_parse {
    ($ret:expr) => {{
        match $ret {
            Ok((parse, offset)) => return Ok((parse, offset)),
            Err(eas) => eas
        }
    }};
    ($enumtype:path, $ret:expr) => {{
        match $ret {
            Ok((parse, offset)) => return Ok(($enumtype(parse), offset)),
            Err(eas) => eas
        }
    }};
    ($enumtype:path, $wrapper:expr, $ret:expr) => {{
        match $ret {
            Ok((parse, offset)) => return Ok(($enumtype($wrapper(parse)), offset)),
            Err(eas) => eas
        }
    }};
}

/// Expect and consume one token, or return immediatly out of the current
/// scope with an error
macro_rules! expect_token {
    ($expect_readable:expr, $input:expr, $offset:ident, $expect:pat) => {
        expect_token_any!(vec!($expect_readable), $input, $offset, $expect)
    }
}

/// Expect one or more tokens or immediatly return out of the current scope
/// with an error message.
/// N.b. This macro will bump `offset` (by one), so you do *not* have
/// to do this yourself!
macro_rules! expect_token_any {
    ($expect_readables:expr, $input:expr, $offset:ident, $($expect:pat),+) =>
    {{
        let tas = eat_token!($input, $offset);
        let &TokenAndSpan(ref token, ref span) = tas;
        match token {
            $(
                &$expect => {
                    tas
                },
            )+
            _ => {
                return Err(ErrorAndSpan(Error::Expected($expect_readables), span.clone()));
            }

        }
    }}

}

/// Return the next token and bump the offset. This should be the preferred
/// method of consuming tokens from the input buffer.
macro_rules! eat_token {
    ($input:expr, $offset:ident) => {{
        match $input.get($offset) {
            Some(token) => {
                $offset += 1;
                token
            },
            None => panic!("I am peeking into the next token, but I am already past the end-of-file!")
        }
    }}
}

/// Return the next token, but do not bump the offset. Only use this if you really
/// do not want to consume the token. Not bumping (when you actually should) could
/// result in unwanted recursion and stack overflows.
macro_rules! peek_token {
    ($input:expr, $offset:ident) => {{
        match $input.get($offset) {
            Some(token) => token,
            None => panic!("I am consuming a token, but I am already past the end-of-file!")
        }
    }}
}

pub type TopParseResult<T> = Result<T, ErrorAndSpan>;
pub type ParseResult<T> = Result<(T, usize), ErrorAndSpan>;

pub struct Parser {
    input: Vec<TokenAndSpan>,
    type_variable_counter: usize,
}

impl Parser {
    pub fn new(input: Vec<TokenAndSpan>) -> Parser {
        Parser {
            input: input,
            type_variable_counter: 0
        }
    }

    fn new_type_variable(&mut self) -> infer::Type {
        let x = self.type_variable_counter;
        self.type_variable_counter += 1;
        infer::Type::Variable(x)
    }

    fn new_type_variable_ref(&mut self) -> Rc<RefCell<infer::Type>> {
        Rc::new(RefCell::new(self.new_type_variable()))
    }

    pub fn parse_spl(&mut self) -> TopParseResult<SPL> {
        let offset = 0;
        let mut decls = Vec::new(); //make_builtin_functions();
        let (new_decl, mut offset) = try_parse!(self.parse_decl(offset));
        decls.push(new_decl);
        loop {
            if let &TokenAndSpan(Token::Eof, _) = peek_token!(self.input, offset) {
                break;
            }
            let (new_decl, new_offset) = try_parse!(self.parse_decl(offset));
            decls.push(new_decl);
            offset = new_offset;
        }
        // we can safely unwrap (because try_parse! would have returned otherwise)
        let span = combine_spans(
                get_decl_span(decls.first().unwrap()), get_decl_span(decls.last().unwrap())
        );
        let spl = SPL {
            span: span,
            decls: decls
        };
        Ok(spl)
    }

    pub fn parse_decl(&mut self, mut offset: usize) -> ParseResult<Decl> {
        let mut eas = yield_parse!(Decl::Var, self.parse_var_decl(offset, true));
        eas = eas | yield_parse!(Decl::Fun, self.parse_fun_decl(offset));
        Err(eas)
    }

    pub fn parse_var_decl(&mut self, mut offset: usize, global: bool) -> ParseResult<VarDecl> {
        let (var_kw_or_type, offset) = try_parse!(self.parse_var_kw_or_type(offset));
        let (mut name, mut offset) = try_parse!(self.parse_ident(offset));
        match &var_kw_or_type {
            &VarKwOrType::VarKw(_) => {},
            &VarKwOrType::Type(ref t) => {
                name.spl_type = Rc::new(RefCell::new(t.to_spl_type()));
            },
        };
        expect_token!("'='", self.input, offset, Token::Eq);
        let (value, mut offset) = try_parse!(self.parse_exp(offset));
        let &TokenAndSpan(_, ref semicolon_span) =
                expect_token!("';'", self.input, offset, Token::Semicolon);
        let var_decl = VarDecl {
            span: combine_spans(get_var_kw_or_type_span(&var_kw_or_type), semicolon_span),
            var_kw_or_type: var_kw_or_type,
            name: name,
            value: value,
            global: global,
        };
        Ok((var_decl, offset))
    }

    pub fn parse_var_kw_or_type(&mut self, mut offset: usize) -> ParseResult<VarKwOrType> {
        let mut eas = yield_parse!(VarKwOrType::VarKw, self.parse_var_kw(offset));
        eas = eas | yield_parse!(VarKwOrType::Type, self.parse_type(offset));
        Err(eas)
    }

    pub fn parse_var_kw(&mut self, mut offset: usize) -> ParseResult<VarKeyword> {
        let &TokenAndSpan(_, ref span) =
                expect_token!("'var'", self.input, offset, Token::Var);
        let var_kw = VarKeyword { span: span.clone() };
        Ok((var_kw, offset))
    }

    #[allow(unused_assignments)]
    pub fn parse_fun_decl(&mut self, mut offset: usize) -> ParseResult<FunDecl> {
        // parse function name
        let (fun_name, mut offset) = try_parse!(self.parse_ident(offset));

        // parse arguments
        expect_token!("'('", self.input, offset, Token::LeftParen);
        let mut args = Vec::new();
        loop {
            if let &TokenAndSpan(Token::RightParen, _) = peek_token!(self.input, offset) {
                break;
            }
            // expect identifier
            let (ident, new_offset) = try_parse!(self.parse_ident(offset));
            args.push(ident);
            offset = new_offset;
            // expect comma
            let some_tas = self.input.get(offset);
            match some_tas {
                Some(&TokenAndSpan(Token::Comma, _)) => {
                    offset += 1
                },
                _ => {
                    break;
                }
            };
        }
        expect_token_any!(vec!("','", "')'"), self.input, offset, Token::RightParen);

        // parse function type
        let (fun_type, mut offset) = match peek_token!(self.input, offset) {
            &TokenAndSpan(Token::DoubleColon, _) => {
                eat_token!(self.input, offset);
                let (fun_type, offset) = try_parse!(self.parse_fun_type(offset));
                (Some(fun_type), offset)
            },
            _ => {
                (None, offset)
            }
        };

        // parse '{'
        if fun_type == None {
            // the compiler currently expects explicit function types
            let &TokenAndSpan(_, ref brace_span) =
                    expect_token!("'{'", self.input, offset, Token::LeftBrace);
            let msg = concat!("This compiler does not support function type inference. ",
                              "Please provide the function type yourself.");
            let error = Error::Plain(msg.to_string());
            return Err(ErrorAndSpan(error, brace_span.clone()));
        } else {
            expect_token!("'{'", self.input, offset, Token::LeftBrace);
        }

        // parse variable declerations
        let mut vars = Vec::new();
        loop {
            if let Ok((var_decl, new_offset)) = self.parse_var_decl(offset, false) {
                vars.push(var_decl);
                offset = new_offset;
            } else {
                break;
            }
        };

        // parse statements
        let (first_stmt, mut offset) = try_parse!(self.parse_stmt(offset));
        let mut body = vec!(first_stmt);
        let (body, mut offset) = match self.parse_stmts(offset, body) {
            Ok((body, offset)) => (body, offset),
            Err(error) => return Err(error)
        };

        // parse '}'
        let &TokenAndSpan(_, ref last_span) =
                expect_token_any!(vec!("a statement", "'}'"), self.input, offset, Token::RightBrace);

        // initialize the initial function type
        {
            let mut spl_type = fun_name.spl_type.borrow_mut();
            *spl_type = if let Some(ref fun_type) = fun_type {
                fun_type.to_spl_type()
            } else {
                let range = ('a' as u32)..('z' as u32);
                let mut polyvar_iter = range.map(|x| from_u32(x).unwrap().to_string());
                let arg_spl_types = args.iter()
                    .map(|_| Box::new(infer::Type::Poly(polyvar_iter.next().unwrap())))
                    .collect();
                let return_type = Box::new(infer::Type::Poly(polyvar_iter.next().unwrap()));
                infer::Type::Function(arg_spl_types, return_type)
            };
        }

        // assign initital argument types
        for (i, mut arg) in args.iter().enumerate() {
            if let infer::Type::Function(arg_types, _) = fun_name.spl_type.borrow().clone() {
                let mut t = arg.spl_type.borrow_mut();
                *t = *arg_types[i].clone();
            }
        }

        let fun_decl = FunDecl {
            span: combine_spans(&fun_name.span, &last_span),
            name: fun_name,
            args: args,
            fun_type: fun_type,
            vars: vars,
            body: body
        };

        Ok((fun_decl, offset))
    }

    pub fn parse_ret_type(&mut self, mut offset: usize) -> ParseResult<RetType> {
        let mut eas = yield_parse!(RetType::Type, self.parse_type(offset));
        eas = eas | yield_parse!(RetType::Void, self.parse_void(offset));
        Err(eas)
    }

    pub fn parse_void(&mut self, mut offset: usize) -> ParseResult<Void> {
        let &TokenAndSpan(_, ref span) = expect_token!("'Void'", self.input, offset, Token::Void);
        let void = Void { span: span.clone() };
        Ok((void, offset))
    }

    pub fn parse_fun_type(&mut self, mut offset: usize) -> ParseResult<FunType> {
        let mut eas = yield_parse!(self.parse_fun_type_with_arguments(offset));
        eas = eas | yield_parse!(self.parse_fun_type_without_arguments(offset));;
        Err(eas)
    }

    fn parse_fun_type_with_arguments(&mut self, mut offset: usize) -> ParseResult<FunType> {
        // Type+ '->' RetType
        let (first_arg, mut offset) = try_parse!(self.parse_type(offset));
        let start = get_type_span(&first_arg).start;
        let mut args: Vec<Type> = vec!(first_arg);
        loop {
            let pas = self.parse_type(offset);
            match pas {
                Ok((arg, new_offset)) => {
                    args.push(arg);
                    offset = new_offset;
                },
                Err(err) => break
            };
        }
        expect_token_any!(vec!("a type", "'->'"), self.input, offset, Token::Arrow);
        let (ret, mut offset) = try_parse!(self.parse_ret_type(offset));
        let fun_type = FunType {
            span: Span { start: start, end: get_ret_type_span(&ret).end },
            args: args,
            ret: ret
        };
        Ok((fun_type, offset))
    }

    fn parse_fun_type_without_arguments(&mut self, mut offset: usize) -> ParseResult<FunType> {
        // RetType
        let (ret, mut offset) = try_parse!(self.parse_ret_type(offset));
        let fun_type = FunType {
            span: get_ret_type_span(&ret).clone(),
            args: Vec::new(),
            ret: ret
        };
        Ok((fun_type, offset))
    }

    pub fn parse_type(&mut self, mut offset: usize) -> ParseResult<Type> {
        // BasicType | PairType | ListType | id
        let mut eas = yield_parse!(Type::Basic, Box::new, self.parse_basic_type(offset));
        eas = eas | yield_parse!(Type::Pair, Box::new, self.parse_pair_type(offset));
        eas = eas | yield_parse!(Type::List, Box::new, self.parse_list_type(offset));
        eas = eas | yield_parse!(Type::Ident, self.parse_ident_as_type(offset));
        Err(eas)
    }

    pub fn parse_basic_type(&mut self, mut offset: usize) -> ParseResult<BasicType> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::Int  => BasicTypeKind::Int,
            &Token::Bool => BasicTypeKind::Bool,
            &Token::Char => BasicTypeKind::Char,
            _ => {
                let error = Error::Expected(vec!("'Int'", "'Bool'", "'Char'"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let basic_type = BasicType {
            kind: kind,
            span: span.clone()
        };
        Ok((basic_type, offset))
    }

    pub fn parse_pair_type(&mut self, mut offset: usize) -> ParseResult<PairType> {
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("'('", self.input, offset, Token::LeftParen);
        let (fst, mut offset) = try_parse!(self.parse_type(offset));
        expect_token!("','", self.input, offset, Token::Comma);
        let (snd, mut offset) = try_parse!(self.parse_type(offset));
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("')' (unclosed parenthesis)", self.input, offset, Token::RightParen);

        let pair_type = PairType {
            fst: fst,
            snd: snd,
            span: Span { start: start, end: end }
        };
        Ok((pair_type, offset))
    }

    pub fn parse_list_type(&mut self, mut offset: usize) -> ParseResult<ListType> {
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("'['", self.input, offset, Token::LeftBracket);
        let (found_type, mut offset) = try_parse!(self.parse_type(offset));
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("']'", self.input, offset, Token::RightBracket);

        let list_type = ListType {
            list_type: found_type,
            span: Span { start: start, end: end }
        };
        Ok((list_type, offset))
    }

    pub fn parse_stmt(&mut self, mut offset: usize) -> ParseResult<Stmt> {
        let mut eas = yield_parse!(Stmt::If, self.parse_if_stmt(offset));
        eas = eas | yield_parse!(Stmt::While, self.parse_while_stmt(offset));
        eas = eas | yield_parse!(Stmt::Return, self.parse_return_stmt(offset));
        eas = eas | yield_parse!(Stmt::Ass, self.parse_ass_stmt(offset));
        eas = eas | yield_parse!(Stmt::FunCall, self.parse_fun_call_stmt(offset));
        Err(eas)
    }

    fn parse_stmts(&mut self, mut offset: usize, mut body: Vec<Stmt>) -> ParseResult<Vec<Stmt>> {
        loop {
            let pr = self.parse_stmt(offset);
            match pr {
                Ok((stmt, new_offset)) => {
                    body.push(stmt);
                    offset = new_offset;
                },
                Err(error) => {
                    let &TokenAndSpan(ref token, _) = peek_token!(self.input, offset);
                    if token == &Token::RightBrace {
                        break;
                    } else {
                        return Err(error);
                    }
                }
            }
        };
        Ok((body, offset))
    }

    pub fn parse_if_stmt(&mut self, mut offset: usize) -> ParseResult<IfStmt> {
        let &TokenAndSpan(_, Span { start, end: _ }) =
                expect_token!("'if'", self.input, offset, Token::If);
        let (cond, mut offset) = try_parse!(self.parse_paren_exp(offset));
        expect_token!("'{'", self.input, offset, Token::LeftBrace);
        let (if_body, mut offset) = try_parse!(self.parse_stmts(offset, Vec::new()));
        let &TokenAndSpan(_, Span { start: _, mut end }) =
                expect_token!("'}'", self.input, offset, Token::RightBrace);

        // if the next token is 'else', we expect an else block
        let (else_body, mut offset) = match peek_token!(self.input, offset) {
            &TokenAndSpan(Token::Else, _) => {
                let ((body, span), mut offset) = try_parse!(self.parse_else_stmt(offset));
                end = span.end;
                (body, offset)
            },
            _ => (Vec::new(), offset)
        };

        let if_stmt = IfStmt {
            cond: cond,
            if_body: if_body,
            else_body: else_body,
            span: Span { start: start, end: end }
        };
        Ok((if_stmt, offset))
    }

    fn parse_else_stmt(&mut self, mut offset: usize) -> ParseResult<(Vec<Stmt>, Span)> {
        let &TokenAndSpan(_, Span { start, end: _ }) =
                expect_token!("'else'", self.input, offset, Token::Else);
        expect_token!("'{'", self.input, offset, Token::LeftBrace);
        let (else_body, mut offset) = try_parse!(self.parse_stmts(offset, Vec::new()));
        let &TokenAndSpan(_, Span { start: _, end }) =
                expect_token!("'}'", self.input, offset, Token::RightBrace);
        Ok(((else_body, Span { start: start, end: end }), offset))
    }

    pub fn parse_while_stmt(&mut self, mut offset: usize) -> ParseResult<WhileStmt> {
        let &TokenAndSpan(_, Span { start, end: _ }) =
                expect_token!("'while'", self.input, offset, Token::While);
        let (cond, mut offset) = try_parse!(self.parse_paren_exp(offset));
        expect_token!("'{'", self.input, offset, Token::LeftBrace);
        let (body, mut offset) = try_parse!(self.parse_stmts(offset, Vec::new()));
        let &TokenAndSpan(_, Span { start: _, mut end }) =
                expect_token!("'}'", self.input, offset, Token::RightBrace);

        let while_stmt = WhileStmt {
            cond: cond,
            body: body,
            span: Span { start: start, end: end }
        };
        Ok((while_stmt, offset))    }

    pub fn parse_ass_stmt(&mut self, mut offset: usize) -> ParseResult<AssStmt> {
        let (ass_ident, mut offset) = try_parse!(self.parse_ident(offset));
        let (ass_opt_field, mut offset) = match self.parse_field(offset) {
            Ok((field, offset)) => (Some(field), offset),
            Err(_) => (None, offset)
        };

        expect_token!("'='", self.input, offset, Token::Eq);
        let (ass_exp, mut offset) = try_parse!(self.parse_exp(offset));
        let &TokenAndSpan(_, ref scspan) = expect_token!("';'", self.input, offset, Token::Semicolon);

        let ass_stmt = AssStmt {
            span: combine_spans(&ass_ident.span, &scspan.clone()),
            var_name: ass_ident,
            field: ass_opt_field,
            value: ass_exp
        };
        Ok((ass_stmt, offset))
    }

    pub fn parse_fun_call_stmt(&mut self, mut offset: usize) -> ParseResult<FunCallStmt> {
        let (fun_call, mut offset) = try_parse!(self.parse_fun_call(offset));
        let &TokenAndSpan(_, ref span) = expect_token!("';'", self.input, offset, Token::Semicolon);
        let fun_call_stmt = FunCallStmt {
            span: combine_spans(&fun_call.span.clone(), &span.clone()),
            fun_call: fun_call,
        };
        Ok((fun_call_stmt, offset))
    }

    pub fn parse_return_stmt(&mut self, mut offset: usize) -> ParseResult<ReturnStmt> {
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("'return'", self.input, offset, Token::Return);
        let (value, mut offset) = match self.parse_exp(offset) {
            Ok((exp, offset)) => (Some(exp), offset),
            Err(_) => (None, offset)
        };
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("';' or an expression", self.input, offset, Token::Semicolon);
        let return_stmt = ReturnStmt {
            span: Span { start: start, end: end },
            value: value
        };
        Ok((return_stmt, offset))
    }

    pub fn parse_exp(&mut self, mut offset: usize) -> ParseResult<Exp> {
        self.parse_exp_l1(offset)
    }

    pub fn parse_exp_l1(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Op1, Box::new, self.parse_op1_exp_l1(offset));
        eas = eas | yield_parse!(Exp::Op2, Box::new, self.parse_op2_exp_l1(offset));
        eas = eas | yield_parse!(self.parse_exp_l2(offset));
        Err(eas)
    }

    pub fn parse_exp_l2(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Op2, Box::new, self.parse_op2_exp_l2(offset));
        eas = eas | yield_parse!(self.parse_exp_l3(offset));
        Err(eas)
    }

    pub fn parse_exp_l3(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Op2, Box::new, self.parse_op2_exp_l3(offset));
        eas = eas | yield_parse!(self.parse_exp_l4(offset));
        Err(eas)
    }

    pub fn parse_exp_l4(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Op1, Box::new, self.parse_op1_exp_l4(offset));
        eas = eas | yield_parse!(Exp::Op2, Box::new, self.parse_op2_exp_l4(offset));
        eas = eas | yield_parse!(self.parse_exp_l5(offset));
        Err(eas)
    }

    pub fn parse_exp_l5(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Op2, Box::new, self.parse_op2_exp_l5(offset));
        eas = eas | yield_parse!(self.parse_exp_l6(offset));
        Err(eas)
    }

    pub fn parse_exp_l6(&mut self, mut offset: usize) -> ParseResult<Exp> {
        let mut eas = yield_parse!(Exp::Field, Box::new, self.parse_field_exp(offset));
        eas = eas | yield_parse!(Exp::FunCall, Box::new, self.parse_fun_call(offset));
        eas = eas | yield_parse!(Exp::Ident, self.parse_ident(offset));
        eas = eas | yield_parse!(Exp::Int, self.parse_literal_int(offset));
        eas = eas | yield_parse!(Exp::Char, self.parse_literal_char(offset));
        eas = eas | yield_parse!(Exp::Bool, self.parse_literal_bool(offset));
        eas = eas | yield_parse!(Exp::Pair, Box::new, self.parse_pair_exp(offset));
        eas = eas | yield_parse!(Exp::Paren, Box::new, self.parse_paren_exp(offset));
        eas = eas | yield_parse!(Exp::EmptyList, self.parse_empty_list(offset));
        return Err(eas);
    }

    pub fn parse_paren_exp(&mut self, mut offset: usize) -> ParseResult<ParenExp> {
        let &TokenAndSpan(_, Span { start, end: _ }) =
                expect_token!("'('", self.input, offset, Token::LeftParen);
        let (exp, mut offset) = try_parse!(self.parse_exp(offset));
        let &TokenAndSpan(_, Span { start: _, end }) =
                expect_token!("')'", self.input, offset, Token::RightParen);
        let paren_exp = ParenExp {
            exp: exp,
            spl_type: self.new_type_variable_ref(),
            span: Span { start: start, end: end }
        };
        Ok((paren_exp, offset))
    }

    pub fn parse_field_exp(&mut self, mut offset: usize) -> ParseResult<FieldExp> {
        let (var_name, mut offset) = try_parse!(self.parse_ident(offset));
        let (field, mut offset) = try_parse!(self.parse_field(offset));

        let field_exp = FieldExp {
            span: combine_spans(&var_name.span, &field.span.clone()),
            spl_type: self.new_type_variable_ref(),
            var_name: var_name,
            field: field
        };
        Ok((field_exp, offset))
    }

    pub fn parse_op2_exp_l1(&mut self, mut offset: usize) -> ParseResult<Op2Exp> {
        let (left, mut offset) = try_parse!(self.parse_exp_l2(offset));
        let (op2, mut offset) = try_parse!(self.parse_op2_l1(offset));
        let (right, mut offset) = try_parse!(self.parse_exp_l2(offset));
        self.parse_op2_exp_l1_inner(offset, left, op2, right)
    }

    fn parse_op2_exp_l1_inner(&mut self, offset: usize,
                              left: Exp, op2: Op2, right: Exp) -> ParseResult<Op2Exp> {
        if let Ok((new_op2, offset)) = self.parse_op2_l1(offset) {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            let new_left = Exp::Op2(Box::new(op2_exp));
            match self.parse_exp_l2(offset) {
                Ok((new_right, offset)) => {
                    self.parse_op2_exp_l1_inner(offset, new_left, new_op2, new_right)
                },
                Err(error) => return Err(error)
            }
        } else {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            Ok((op2_exp, offset))
        }
    }

    pub fn parse_op2_exp_l2(&mut self, mut offset: usize) -> ParseResult<Op2Exp> {
        let (left, mut offset) = try_parse!(self.parse_exp_l3(offset));
        let (op2, mut offset) = try_parse!(self.parse_op2_l2(offset));
        let (right, mut offset) = try_parse!(self.parse_exp_l3(offset));
        self.parse_op2_exp_l2_inner(offset, left, op2, right)
    }

    fn parse_op2_exp_l2_inner(&mut self, offset: usize,
                              left: Exp, op2: Op2, right: Exp) -> ParseResult<Op2Exp> {
        if let Ok((new_op2, offset)) = self.parse_op2_l2(offset) {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            let new_left = Exp::Op2(Box::new(op2_exp));
            match self.parse_exp_l3(offset) {
                Ok((new_right, offset)) => {
                    self.parse_op2_exp_l2_inner(offset, new_left, new_op2, new_right)
                },
                Err(error) => return Err(error)
            }
        } else {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            Ok((op2_exp, offset))
        }
    }

    pub fn parse_op2_exp_l3(&mut self, mut offset: usize) -> ParseResult<Op2Exp> {
        // This level contains only the cons operator, so it should be right associative.
        // This make it possible to parse this expression type (top-down) without the
        // "(...)_inner" construction, by expecting the same level, at the right side
        // of the operator
        let (left, mut offset) = try_parse!(self.parse_exp_l4(offset));
        let (op2, mut offset) = try_parse!(self.parse_op2_l3(offset));
        let (right, mut offset) = try_parse!(self.parse_exp_l3(offset));
        let op2_exp = Op2Exp {
            span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
            spl_type: self.new_type_variable_ref(),
            left: left,
            op2: op2,
            right: right
        };
        Ok((op2_exp, offset))
    }

    pub fn parse_op2_exp_l4(&mut self, mut offset: usize) -> ParseResult<Op2Exp> {
        let (left, mut offset) = try_parse!(self.parse_exp_l5(offset));
        let (op2, mut offset) = try_parse!(self.parse_op2_l4(offset));
        let (right, mut offset) = try_parse!(self.parse_exp_l5(offset));
        self.parse_op2_exp_l4_inner(offset, left, op2, right)
    }

    fn parse_op2_exp_l4_inner(&mut self, offset: usize,
                              left: Exp, op2: Op2, right: Exp) -> ParseResult<Op2Exp> {
        if let Ok((new_op2, offset)) = self.parse_op2_l4(offset) {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            let new_left = Exp::Op2(Box::new(op2_exp));
            match self.parse_exp_l5(offset) {
                Ok((new_right, offset)) => {
                    self.parse_op2_exp_l4_inner(offset, new_left, new_op2, new_right)
                },
                Err(error) => return Err(error)
            }
        } else {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            Ok((op2_exp, offset))
        }
    }

    pub fn parse_op2_exp_l5(&mut self, mut offset: usize) -> ParseResult<Op2Exp> {
        let (left, mut offset) = try_parse!(self.parse_exp_l6(offset));
        let (op2, mut offset) = try_parse!(self.parse_op2_l5(offset));
        let (right, mut offset) = try_parse!(self.parse_exp_l6(offset));
        self.parse_op2_exp_l5_inner(offset, left, op2, right)
    }

    fn parse_op2_exp_l5_inner(&mut self, offset: usize,
                              left: Exp, op2: Op2, right: Exp) -> ParseResult<Op2Exp> {
        if let Ok((new_op2, offset)) = self.parse_op2_l5(offset) {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            let new_left = Exp::Op2(Box::new(op2_exp));
            match self.parse_exp_l6(offset) {
                Ok((new_right, offset)) => {
                    self.parse_op2_exp_l5_inner(offset, new_left, new_op2, new_right)
                },
                Err(error) => return Err(error)
            }
        } else {
            let op2_exp = Op2Exp {
                span: combine_spans(get_exp_span(&left), get_exp_span(&right)),
                spl_type: self.new_type_variable_ref(),
                left: left,
                op2: op2,
                right: right
            };
            Ok((op2_exp, offset))
        }
    }

    pub fn parse_op1_exp_l1(&mut self, mut offset: usize) -> ParseResult<Op1Exp> {
        let (op1, offset) = try_parse!(self.parse_op1_l1(offset));
        let (exp, offset) = try_parse!(self.parse_exp(offset));
        let span = Span { start: op1.span.start, end: get_exp_span(&exp).end };
        let op1_exp = Op1Exp {
            op1: op1,
            operand: exp,
            spl_type: self.new_type_variable_ref(),
            span: span
        };
        Ok((op1_exp, offset))
    }

    pub fn parse_op1_exp_l4(&mut self, mut offset: usize) -> ParseResult<Op1Exp> {
        let (op1, offset) = try_parse!(self.parse_op1_l4(offset));
        let (exp, offset) = try_parse!(self.parse_exp(offset));
        let span = Span { start: op1.span.start, end: get_exp_span(&exp).end };
        let op1_exp = Op1Exp {
            op1: op1,
            operand: exp,
            spl_type: self.new_type_variable_ref(),
            span: span
        };
        Ok((op1_exp, offset))
    }

    pub fn parse_literal_bool(&mut self, mut offset: usize) -> ParseResult<LiteralBool> {
        let mut eas = yield_parse!(LiteralBool::True, self.parse_literal_bool_true(offset));
        eas = eas | yield_parse!(LiteralBool::False, self.parse_literal_bool_false(offset));
        Err(eas)
    }

    pub fn parse_literal_bool_true(&mut self, mut offset: usize) -> ParseResult<LiteralTrue> {
        let tas = expect_token!("'True'", self.input, offset, Token::True);
        let &TokenAndSpan(_, ref span) = tas;
        let t = LiteralTrue {
            span: span.clone(),
            spl_type: Rc::new(RefCell::new(infer::Type::Bool)),
        };
        Ok((t, offset))
    }

    pub fn parse_literal_bool_false(&mut self, mut offset: usize) -> ParseResult<LiteralFalse> {
        let tas = expect_token!("'False'", self.input, offset, Token::False);
        let &TokenAndSpan(_, ref span) = tas;
        let f = LiteralFalse {
            span: span.clone(),
            spl_type: Rc::new(RefCell::new(infer::Type::Bool)),
        };
        Ok((f, offset))
    }

    pub fn parse_pair_exp(&mut self, mut offset: usize) -> ParseResult<PairExp> {
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("'('", self.input, offset, Token::LeftParen);
        let (fst, mut offset) = try_parse!(self.parse_exp(offset));
        expect_token!("','", self.input, offset, Token::Comma);
        let (snd, mut offset) = try_parse!(self.parse_exp(offset));
        let &TokenAndSpan(_, Span { start, end }) =
                expect_token!("')'", self.input, offset, Token::RightParen);

        let spl_type = Rc::new(RefCell::new(infer::Type::Pair(
            Box::new(self.new_type_variable()), Box::new(self.new_type_variable()))));
        let parse = PairExp {
            fst: fst,
            snd: snd,
            span: Span { start: start, end: end },
            spl_type: spl_type
        };
        Ok((parse, offset))
    }

    pub fn parse_field(&mut self, mut offset: usize) -> ParseResult<Field> {
        let dot_span = {
            let &TokenAndSpan(_, ref span) =
                    expect_token!("'.'", self.input, offset, Token::Dot);
            span.clone()
        };

        let (kind, kind_span) = {
            let &TokenAndSpan(ref kind_token, ref span) = eat_token!(self.input, offset);
            match kind_token {
                &Token::Hd => (FieldKind::Hd, span.clone()),
                &Token::Tl => (FieldKind::Tl, span.clone()),
                &Token::Fst => (FieldKind::Fst, span.clone()),
                &Token::Snd => (FieldKind::Snd, span.clone()),
                _ => {
                    let error = Error::Expected(vec!("'hd'", "'tl'", "'fst'", "'snd'"));
                    return Err(ErrorAndSpan(error, span.clone()));
                }
            }
        };

        let (next_field, last_span) = if let Ok((token, new_offset)) = self.parse_field(offset) {
            offset = new_offset;
            let token_span = token.span.clone();
            (Some(Box::new(token)), token_span)
        } else {
            (None, kind_span.clone())
        };

        let field = Field {
            span: combine_spans(&dot_span, &last_span),
            kind: kind,
            next: next_field
        };
        Ok((field, offset))
    }

    pub fn parse_fun_call(&mut self, mut offset: usize) -> ParseResult<FunCall> {
        // parse the function name
        let (name, mut offset) = try_parse!(self.parse_ident(offset));

        // parse '('
        expect_token!("'('", self.input, offset, Token::LeftParen);

        // parse multiple arguments
        let mut act_args = Vec::new();
        loop {
            // support empty argument list
            if let &TokenAndSpan(Token::RightParen, _) = peek_token!(self.input, offset) {
                break;
            }
            // parse exp
            let (exp, new_offset) = try_parse!(self.parse_exp(offset));
            act_args.push(exp);
            offset = new_offset;
            // expect comma
            let some_tas = self.input.get(offset);
            match some_tas {
                Some(&TokenAndSpan(Token::Comma, _)) => {
                    offset += 1
                },
                _ => {
                    break;
                }
            };
        }
        let spl_type = self.new_type_variable_ref();
        let &TokenAndSpan(ref token, ref rh_span) =
                expect_token_any!(vec!("','", "')'"), self.input, offset, Token::RightParen);
        let fun_call = FunCall {
            span: combine_spans(&name.span, rh_span),
            spl_type: spl_type,
            fun_name: name,
            act_args: act_args
        };
        Ok((fun_call, offset))
    }

    pub fn parse_op2_l1(&mut self, mut offset: usize) -> ParseResult<Op2> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::AndAnd => Op2Kind::And,
            &Token::OrOr => Op2Kind::Or,
            _ => {
                let error = Error::Expected(vec!("'&&'", "'||'"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let op2 = Op2 {
            kind: kind,
            span: span.clone()
        };
        Ok((op2, offset))
    }

    pub fn parse_op2_l2(&mut self, mut offset: usize) -> ParseResult<Op2> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::EqEq => Op2Kind::Eq,
            &Token::Ne => Op2Kind::Ne,
            &Token::Lt => Op2Kind::Lt,
            &Token::Le => Op2Kind::Le,
            &Token::Gt => Op2Kind::Gt,
            &Token::Ge => Op2Kind::Ge,
            _ => {
                let error = Error::Expected(vec!("'=='", "'!='", "'<'", "'<='", "'>'", "'>='"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let op2 = Op2 {
            kind: kind,
            span: span.clone()
        };
        Ok((op2, offset))
    }

    pub fn parse_op2_l3(&mut self, mut offset: usize) -> ParseResult<Op2> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::Colon => Op2Kind::Cons,
            _ => {
                let error = Error::Expected(vec!("':'"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let op2 = Op2 {
            kind: kind,
            span: span.clone()
        };
        Ok((op2, offset))
    }

    pub fn parse_op2_l4(&mut self, mut offset: usize) -> ParseResult<Op2> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::Plus => Op2Kind::Add,
            &Token::Minus => Op2Kind::Sub,
            _ => {
                let error = Error::Expected(vec!("'+'", "'-'"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let op2 = Op2 {
            kind: kind,
            span: span.clone()
        };
        Ok((op2, offset))
    }

    pub fn parse_op2_l5(&mut self, mut offset: usize) -> ParseResult<Op2> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        let kind = match token {
            &Token::Star => Op2Kind::Mul,
            &Token::Slash => Op2Kind::Div,
            &Token::Percent => Op2Kind::Mod,
            _ => {
                let error = Error::Expected(vec!("'*'", "'/'", "'%'"));
                return Err(ErrorAndSpan(error, span.clone()));
            }
        };
        let op2 = Op2 {
            kind: kind,
            span: span.clone()
        };
        Ok((op2, offset))
    }

    pub fn parse_op1_l1(&mut self, mut offset: usize) -> ParseResult<Op1> {
        let &TokenAndSpan(ref token, ref span) =
                expect_token!("'!'", self.input, offset, Token::Exclamation);
        let parse = Op1 {
            kind: Op1Kind::Not,
            span: span.clone()
        };
        Ok((parse, offset))
    }

    pub fn parse_op1_l4(&mut self, mut offset: usize) -> ParseResult<Op1> {
        let &TokenAndSpan(ref token, ref span) =
                expect_token!("'-'", self.input, offset, Token::Minus);
        let parse = Op1 {
            kind: Op1Kind::Neg,
            span: span.clone()
        };
        Ok((parse, offset))
    }

    pub fn parse_ident(&mut self, mut offset: usize) -> ParseResult<Ident> {
        self.parse_ident_inner(offset, false)
    }

    pub fn parse_ident_as_type(&mut self, mut offset: usize) -> ParseResult<Ident> {
        self.parse_ident_inner(offset, true)
    }

    fn parse_ident_inner(&mut self, mut offset: usize, as_type: bool) -> ParseResult<Ident> {
        let tv = self.new_type_variable_ref();
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        if let Token::Ident(ref name) = *token {
            let spl_type = if as_type {
                let msg = "This compiler does not support polymorphic functions.";
                let error = Error::Plain(msg.to_string());
                return Err(ErrorAndSpan(error, span.clone()));
            } else {
                tv
            };
            let parse = Ident {
                name: name.clone(),
                span: span.clone(),
                spl_type: spl_type,
            };
            Ok((parse, offset))
        } else {
            let error = Error::Expected(vec!("an identifier"));
            Err(ErrorAndSpan(error, span.clone()))
        }
    }

    pub fn parse_literal_int(&mut self, mut offset: usize) -> ParseResult<LiteralInt> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        if let &Token::LiteralInt(value) = token {
            let parse = LiteralInt {
                value: value,
                span: span.clone(),
                spl_type: Rc::new(RefCell::new(infer::Type::Int)),
            };
            Ok((parse, offset))
        } else {
            let error = Error::Expected(vec!("a literal int"));
            Err(ErrorAndSpan(error, span.clone()))
        }
    }

    pub fn parse_literal_char(&mut self, mut offset: usize) -> ParseResult<LiteralChar> {
        let &TokenAndSpan(ref token, ref span) = eat_token!(self.input, offset);
        if let &Token::LiteralChar(ch) = token {
            let parse = LiteralChar {
                value: ch as u8,
                span: span.clone(),
                spl_type: Rc::new(RefCell::new(infer::Type::Char)),
            };
            Ok((parse, offset))
        } else {
            let error = Error::Expected(vec!("a literal char"));
            Err(ErrorAndSpan(error, span.clone()))
        }
    }

    pub fn parse_empty_list(&mut self, mut offset: usize) -> ParseResult<EmptyList> {
        let &TokenAndSpan(_, Span { start, end: _ }) =
                expect_token!("'['", self.input, offset, Token::LeftBracket);
        let &TokenAndSpan(_, Span { start: _, end }) =
                expect_token!("']'", self.input, offset, Token::RightBracket);
        let empty_list = EmptyList {
            span: Span { start: start, end: end },
            spl_type: Rc::new(RefCell::new(
                infer::Type::List(Box::new(self.new_type_variable()))
            )),
        };
        Ok((empty_list, offset))
    }

}

fn get_decl_span(decl: &Decl) -> &Span {
    match decl {
        &Decl::Var(ref x) => &x.span,
        &Decl::Fun(ref x) => &x.span
    }
}

fn get_var_kw_or_type_span(vkot: &VarKwOrType) -> &Span {
    match vkot {
        &VarKwOrType::VarKw(ref x) => &x.span,
        &VarKwOrType::Type(ref x) => get_type_span(&x)
    }
}

fn get_exp_span(exp: &Exp) -> &Span {
    match exp {
        &Exp::Op1(ref x) => &x.span,
        &Exp::Op2(ref x) => &x.span,
        &Exp::Field(ref x) => &x.span,
        &Exp::FunCall(ref x) => &x.span,
        &Exp::Ident(ref x) => &x.span,
        &Exp::Int(ref x) => &x.span,
        &Exp::Char(ref x) => &x.span,
        &Exp::Bool(ref x) => get_literal_bool_span(&x),
        &Exp::Pair(ref x) => &x.span,
        &Exp::Paren(ref x) => get_paren_exp_span(&x),
        &Exp::EmptyList(ref x) => &x.span
    }
}

fn get_ret_type_span(rt: &RetType) -> &Span {
    match rt {
        &RetType::Type(ref x) => get_type_span(&x),
        &RetType::Void(ref x) => &x.span
    }
}

fn get_type_span(t: &Type) -> &Span {
    match t {
        &Type::Basic(ref x) => &x.span,
        &Type::Pair(ref x) => &x.span,
        &Type::List(ref x) => &x.span,
        &Type::Ident(ref x) => &x.span
    }
}

fn get_literal_bool_span(b: &LiteralBool) -> &Span {
    match b {
        &LiteralBool::True(ref x) => &x.span,
        &LiteralBool::False(ref x) => &x.span
    }
}

fn get_paren_exp_span(paren_exp: &ParenExp) -> &Span {
    &paren_exp.span
}

fn make_builtin_functions() -> Vec<Decl> {
    fn span() -> Span{
        Span { start: 0, end: 0 }
    }
    vec!(
        FunDecl {
            span: span(),
            name: Ident {
                span: span(),
                name: "__builtin__.print".to_string(),
                spl_type: Rc::new(RefCell::new(
                    infer::Type::Function(vec!(Box::new(infer::Type::Noop)),
                                          Box::new(infer::Type::Void))
                )),
            },
            args: vec!(
                Ident {
                    span: span(),
                    name: "x".to_string(),
                    spl_type: Rc::new(RefCell::new(infer::Type::Noop)),
                }
            ),
            fun_type: Some(FunType {
                span: span(),
                args: Vec::new(),
                ret: RetType::Void(Void { span: span() }),
            }),
            vars: Vec::new(),
            body: Vec::new(),
        },
        FunDecl {
            span: span(),
            name: Ident {
                span: span(),
                name: "__builtin__.println".to_string(),
                spl_type: Rc::new(RefCell::new(
                    infer::Type::Function(vec!(Box::new(infer::Type::Noop)),
                                          Box::new(infer::Type::Void))
                )),
            },
            args: vec!(
                Ident {
                    span: span(),
                    name: "x".to_string(),
                    spl_type: Rc::new(RefCell::new(infer::Type::Noop)),
                }
            ),
            fun_type: Some(FunType {
                span: span(),
                args: Vec::new(),
                ret: RetType::Void(Void { span: span() }),
            }),
            vars: Vec::new(),
            body: Vec::new(),
        },
        FunDecl {
            span: span(),
            name: Ident {
                span: span(),
                name: "__builtin__.isEmpty".to_string(),
                spl_type: Rc::new(RefCell::new(
                    infer::Type::Function(
                        vec!(Box::new(infer::Type::List(Box::new(infer::Type::Noop)))),
                        Box::new(infer::Type::Bool))
                )),
            },
            args: vec!(
                Ident {
                    span: span(),
                    name: "list".to_string(),
                    spl_type: Rc::new(RefCell::new(
                        infer::Type::List(Box::new(infer::Type::Noop))
                    )),
                }
            ),
            fun_type: Some(FunType {
                span: span(),
                args: Vec::new(),
                ret: RetType::Void(Void { span: span() }),
            }),
            vars: Vec::new(),
            body: Vec::new(),
        },
        FunDecl {
            span: span(),
            name: Ident {
                span: span(),
                name: "__builtin__.putChar".to_string(),
                spl_type: Rc::new(RefCell::new(
                    infer::Type::Function(
                        vec!(Box::new(infer::Type::Char)),
                        Box::new(infer::Type::Void))
                )),
            },
            args: vec!(
                Ident {
                    span: span(),
                    name: "str".to_string(),
                    spl_type: Rc::new(RefCell::new(
                        infer::Type::List(Box::new(infer::Type::Noop))
                    )),
                }
            ),
            fun_type: Some(FunType {
                span: span(),
                args: Vec::new(),
                ret: RetType::Void(Void { span: span() }),
            }),
            vars: Vec::new(),
            body: Vec::new(),
        },
    ).into_iter()
        .map(|x| Decl::Fun(x))
        .collect()
}
