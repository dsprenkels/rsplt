use std::rc::Rc;
use std::cell::RefCell;

use ast;
use infer;
use util::Span;

#[test]
fn test_unify_free_variable() {
    let ident1 = ast::Ident {
        span: Span { start: 0, end: 0 },
        spl_type: Rc::new(RefCell::new(infer::Type::Variable(42))),
        name: "example".to_string()
    };
    let mut ident2 = ast::Ident {
        span: Span { start: 0, end: 0 },
        spl_type: Rc::new(RefCell::new(infer::Type::Variable(3))),
        name: "example".to_string()
    };

    let var_kw = ast::VarKeyword { span: Span { start: 0, end: 0 } };
    let tt = ast::LiteralTrue {
        span: Span { start: 0, end: 0 },
        spl_type: Rc::new(RefCell::new(infer::Type::Bool)),
    };
    let value = ast::Exp::Bool(ast::LiteralBool::True(tt));
    let var_decl = ast::VarDecl {
        span: Span { start: 0, end: 0 },
        var_kw_or_type: ast::VarKwOrType::VarKw(var_kw),
        name: ident1,
        value: value,
        global: true,
    };
    let spl = ast::SPL {
        span: Span { start: 0, end: 0 },
        decls: vec!(ast::Decl::Var(var_decl)),
    };

    assert!(*ident2.spl_type.borrow() != infer::Type::Variable(42));
    ident2.unify_free_variables(&spl, None);
    assert!(*ident2.spl_type.borrow() == infer::Type::Variable(42));
}
