use parser::*;
use scanner::{Token, TokenAndSpan};
use util::{Error, ErrorAndSpan, Span};
use ast;


macro_rules! make_parser {
    ($tokens_input:expr) => {{
        let dummy_span = Span {
            start: 0,
            end: 0
        };
        let input = $tokens_input.into_iter()
                                       .map(|x| TokenAndSpan(x, dummy_span.clone()))
                                       .collect();
        Parser::new(input)
    }}
}

macro_rules! assert_parse_ok {
    ( $func:ident, $tokens_input:expr, $expect:pat ) => {{
        let parse = make_parser!($tokens_input).$func(0);
        if let Ok(($expect, _)) = parse {
        } else {
            panic!("{:?} is does not match Ok(...)", parse);
        }
    }};
}

macro_rules! assert_parse_err_expected {
    ( $func:ident, $tokens_input:expr, $( $expect:expr ),+ ) => {{
        let parse = make_parser!($tokens_input).$func(0);
        if let Err(ErrorAndSpan(Error::Expected(expected), _)) = parse {
            assert_eq!(expected, vec!($( $expect ),+));
        } else {
            panic!("{:?} is not Err(_)", parse);
        }
    }};
}


#[test]
fn test_try_parse_macro() {
    fn parse_literal_int(input: Vec<Token>) -> ParseResult<ast::LiteralInt> {
        let mut parser = make_parser!(input);
        let (pas, offset) = try_parse!(parser.parse_literal_int(0));
        Ok((pas, offset))
    }
    assert_parse_ok!(parse_literal_int, vec!(Token::LiteralInt(3), Token::Eof),
                     ast::LiteralInt{ span: _, value: 3, spl_type: _ });
    assert_parse_err_expected!(parse_literal_int, vec!(Token::LiteralChar('a'), Token::Eof),
                               "a literal int");
}

#[test]
fn test_parse_var_kw_or_type() {
    assert_parse_ok!(parse_var_kw_or_type,
                     vec!(Token::Var, Token::Eof),
                     ast::VarKwOrType::VarKw(_));
    assert_parse_ok!(parse_var_kw_or_type,
                     vec!(Token::Int, Token::Eof),
                     ast::VarKwOrType::Type(_));
}



#[test]
fn test_parse_ret_type() {
    assert_parse_ok!(parse_ret_type, vec!(Token::Void, Token::Eof),
                     ast::RetType::Void(_));
    assert_parse_ok!(parse_ret_type, vec!(Token::Int, Token::Eof),
                     ast::RetType::Type(_));
}

#[test]
fn test_parse_void() {
    assert_parse_ok!(parse_void, vec!(Token::Void, Token::Eof),
                     ast::Void { span: _ });
}

#[test]
fn test_parse_fun_type() {
    assert_parse_ok!(parse_fun_type, vec!(Token::Int, Token::Arrow, Token::Int, Token::Eof),
                     ast::FunType { args: _, ret: _, span: _ });
}

#[test]
fn test_parse_literal_bool() {
    assert_parse_ok!(parse_literal_bool, vec!(Token::True, Token::Eof),
                     ast::LiteralBool::True(_));
    assert_parse_ok!(parse_literal_bool, vec!(Token::False, Token::Eof),
                     ast::LiteralBool::False(_));
    assert_parse_err_expected!(parse_literal_bool, vec!(Token::LiteralInt(3), Token::Eof),
                              "'True'", "'False'");
}

#[test]
fn test_parse_literal_true() {
    assert_parse_ok!(parse_literal_bool_true, vec!(Token::True, Token::Eof),
                     ast::LiteralTrue { span: _, spl_type: _ });
}

#[test]
fn test_parse_literal_false() {
    assert_parse_ok!(parse_literal_bool_false, vec!(Token::False, Token::Eof),
                     ast::LiteralFalse { span: _, spl_type: _ });
}

#[test]
fn test_parse_field() {
    assert_parse_ok!(parse_field, vec!(Token::Dot, Token::Hd, Token::Dot, Token::Tl, Token::Eof),
                     ast::Field { span: _,
                                  kind: ast::FieldKind::Hd,
                                  next: Some(_) });
    assert_parse_ok!(parse_field, vec!(Token::Dot, Token::Hd, Token::Eof),
                     ast::Field { span: _,
                                  kind: ast::FieldKind::Hd,
                                  next: None });
}

#[test]
fn test_parse_exp() {
    assert_parse_ok!(parse_exp, vec!(Token::Minus, Token::LiteralInt(3), Token::Eof),
        ast::Exp::Op1(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LiteralInt(3), Token::Plus, Token::LiteralInt(3), Token::Eof),
        ast::Exp::Op2(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::Ident(String::from("a")), Token::Dot, Token::Hd, Token::Eof),
        ast::Exp::Field(_)
    );
    assert_parse_ok!(parse_exp, vec!(
            Token::Ident(String::from("f")), Token::LeftParen, Token::LiteralInt(3),
            Token::RightParen, Token::Eof
        ),
        ast::Exp::FunCall(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::Ident(String::from("a")), Token::Eof),
        ast::Exp::Ident(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LiteralInt(3), Token::Eof),
        ast::Exp::Int(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LiteralChar('a'), Token::Eof),
        ast::Exp::Char(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::True, Token::Eof),
        ast::Exp::Bool(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::False, Token::Eof),
        ast::Exp::Bool(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LeftParen, Token::LiteralInt(3), Token::Comma,
                                     Token::LiteralInt(7), Token::RightParen, Token::Eof),
        ast::Exp::Pair(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LeftParen, Token::LiteralInt(3), Token::RightParen, Token::Eof),
        ast::Exp::Paren(_)
    );
    assert_parse_ok!(parse_exp, vec!(Token::LeftBracket, Token::RightBracket, Token::Eof),
        ast::Exp::EmptyList(_)
    );
}

#[test]
fn test_left_associative() {
    fn x() -> Token {
        Token::LiteralInt(3)
    }

    fn assert_left_associative(input: Vec<Token>) {
        let mut parser = make_parser!(input);
        let (exp, _) = parser.parse_exp(0).unwrap();
        let op2_exp = match exp {
            ast::Exp::Op2(op2_exp) => *op2_exp,
            _ => panic!("expected an Op2 parse")
        };
        match op2_exp {
            ast::Op2Exp {
                left: ast::Exp::Op2(_),
                right: _, op2: _, span: _, spl_type: _
            } => {},
            _ => panic!("left branch is not an expression token, the full tree is {:?}",
                        op2_exp)
        }
    }
    // singletons
    assert_left_associative(vec!(x(), Token::Star, x(), Token::Star, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::Plus, x(), Token::Plus, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::EqEq, x(), Token::EqEq, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::AndAnd, x(), Token::AndAnd, x(), Token::Eof));

    // pairs
    assert_left_associative(vec!(x(), Token::Star, x(), Token::Plus, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::Plus, x(), Token::Colon, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::Colon, x(), Token::EqEq, x(), Token::Eof));
    assert_left_associative(vec!(x(), Token::EqEq, x(), Token::AndAnd, x(), Token::Eof));
}

#[test]
fn test_right_associative() {
    fn x() -> Token {
        Token::LiteralInt(3)
    }

    fn assert_right_associative(input: Vec<Token>) {
        let mut parser = make_parser!(input);
        let (exp, _) = parser.parse_exp(0).unwrap();
        let op2_exp = match exp {
            ast::Exp::Op2(op2_exp) => *op2_exp,
            _ => panic!("expected an Op2 parse")
        };
        match op2_exp {
            ast::Op2Exp {
                right: ast::Exp::Op2(_),
                left: _, op2: _, span: _, spl_type: _
            } => {},
            _ => panic!("right branch is not an expression token, the full tree is {:?}",
                        op2_exp)
        }
    }
    // cons operator
    assert_right_associative(vec!(x(), Token::Colon, x(), Token::Colon, x(), Token::Eof));

    // pairs
    assert_right_associative(vec!(x(), Token::Plus, x(), Token::Star, x(), Token::Eof));
    assert_right_associative(vec!(x(), Token::Colon, x(), Token::Plus, x(), Token::Eof));
    assert_right_associative(vec!(x(), Token::EqEq, x(), Token::Colon, x(), Token::Eof));
    assert_right_associative(vec!(x(), Token::AndAnd, x(), Token::EqEq, x(), Token::Eof));
}

#[test]
fn test_parse_field_exp() {
    assert_parse_ok!(parse_field_exp, vec!(
            Token::Ident(String::from("a")), Token::Dot, Token::Hd, Token::Dot,
            Token::Tl, Token::Eof
        ),
                     ast::FieldExp { span: _,
                                     var_name: ast::Ident { span: _, name: _, spl_type: _ },
                                     field: ast::Field {
                                        span: _,
                                        kind: ast::FieldKind::Hd,
                                        next: Some(_)
                                     },
                                     spl_type: _ });
    assert_parse_ok!(parse_field_exp, vec!(
            Token::Ident(String::from("a")), Token::Dot, Token::Hd, Token::Eof
        ),
                     ast::FieldExp { span: _,
                                     var_name: ast::Ident { span: _, name: _, spl_type: _ },
                                     field: ast::Field {
                                        span: _,
                                        kind: ast::FieldKind::Hd,
                                        next: None
                                     },
                                     spl_type: _ });
}

#[test]
fn test_parse_ass_stmt() {
    assert_parse_ok!(parse_ass_stmt, vec!(Token::Ident(String::from("a")), Token::Eq,
                                          Token::LiteralInt(3), Token::Semicolon, Token::Eof),
                     ast::AssStmt { span: _, var_name: _, field: None, value: _ });
    assert_parse_ok!(parse_ass_stmt, vec!(Token::Ident(String::from("a")), Token::Dot,
                                          Token::Hd, Token::Eq, Token::LiteralInt(3),
                                          Token::Semicolon, Token::Eof),
                     ast::AssStmt { span: _, var_name: _, field: Some(_), value: _ });
}

#[test]
fn test_parse_return_stmt() {
    assert_parse_ok!(parse_return_stmt, vec!(Token::Return, Token::Semicolon, Token::Eof),
                     ast::ReturnStmt { span: _, value: None });
    assert_parse_ok!(parse_return_stmt, vec!(Token::Return, Token::LiteralInt(3),
                                             Token::Semicolon, Token::Eof),
                     ast::ReturnStmt { span: _, value: Some(_) });

}

#[test]
fn test_parse_basic_type() {
    assert_parse_ok!(parse_basic_type, vec!(Token::Int, Token::Eof),
                     ast::BasicType { kind: ast::BasicTypeKind::Int, span: _ });
    assert_parse_ok!(parse_basic_type, vec!(Token::Bool, Token::Eof),
                     ast::BasicType { kind: ast::BasicTypeKind::Bool, span: _ });
    assert_parse_ok!(parse_basic_type, vec!(Token::Char, Token::Eof),
                     ast::BasicType { kind: ast::BasicTypeKind::Char, span: _ });
}

#[test]
fn test_parse_pair_type() {
    let input =  vec!(Token::LeftParen,
                      Token::Bool, Token::Comma, Token::Int,
                      Token::RightParen, Token::Eof);
    let (pair_type, _) = make_parser!(input).parse_pair_type(0).unwrap();
}

#[test]
fn test_parse_list_type() {
    let input = vec!(Token::LeftBracket, Token::Char, Token::RightBracket, Token::Eof);
    let (list_type, _) = make_parser!(input).parse_list_type(0).unwrap();
}

#[test]
fn test_parse_type() {
    // No extra test case for `id` type, so let's check whether parse_type()
    // can detect the `id` case.
    assert_parse_ok!(parse_type, vec!(Token::Int, Token::Eof),
                     ast::Type::Basic(_));
    assert_parse_ok!(parse_type, vec!(Token::LeftParen,
                                      Token::Bool, Token::Comma, Token::Int,
                                      Token::RightParen, Token::Eof),
                     ast::Type::Pair(_));
    assert_parse_ok!(parse_type, vec!(Token::LeftBracket, Token::Char, Token::RightBracket, Token::Eof),
                     ast::Type::List(_));
    // The following test case does not work since we aren't supporting polymorphic functions
    // assert_parse_ok!(parse_type, vec!(Token::Ident(String::from("testType")), Token::Eof),
    //                  ast::Type::Ident(_));
}

#[test]
fn test_parse_op2_l1() {
    assert_parse_ok!(parse_op2_l1, vec!(Token::AndAnd, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::And });
    assert_parse_ok!(parse_op2_l1, vec!(Token::OrOr, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Or });
}

#[test]
fn test_parse_op2_l2() {
    assert_parse_ok!(parse_op2_l2, vec!(Token::EqEq, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Eq });
    assert_parse_ok!(parse_op2_l2, vec!(Token::Ne, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Ne });
    assert_parse_ok!(parse_op2_l2, vec!(Token::Lt, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Lt });
    assert_parse_ok!(parse_op2_l2, vec!(Token::Le, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Le });
    assert_parse_ok!(parse_op2_l2, vec!(Token::Gt, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Gt });
    assert_parse_ok!(parse_op2_l2, vec!(Token::Ge, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Ge });
}

#[test]
fn test_parse_op2_l3() {
    assert_parse_ok!(parse_op2_l3, vec!(Token::Colon, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Cons });
}

#[test]
fn test_parse_op2_l4() {
    assert_parse_ok!(parse_op2_l4, vec!(Token::Plus, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Add });
    assert_parse_ok!(parse_op2_l4, vec!(Token::Minus, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Sub });
}

#[test]
fn test_parse_op2_l5() {
    assert_parse_ok!(parse_op2_l5, vec!(Token::Star, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Mul });
    assert_parse_ok!(parse_op2_l5, vec!(Token::Slash, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Div });
    assert_parse_ok!(parse_op2_l5, vec!(Token::Percent, Token::Eof),
                     ast::Op2{ span: _, kind: ast::Op2Kind::Mod });
}

#[test]
fn test_parse_op1_l1() {
    assert_parse_ok!(parse_op1_l1, vec!(Token::Exclamation, Token::Eof),
                     ast::Op1{ span: _, kind: ast::Op1Kind::Not });
    assert_parse_err_expected!(parse_op1_l1, vec!(Token::LiteralInt(3), Token::Eof),
                      "'!'");
}

#[test]
fn test_parse_op1_l4() {
    assert_parse_ok!(parse_op1_l4, vec!(Token::Minus, Token::Eof),
                     ast::Op1{ span: _, kind: ast::Op1Kind::Neg });
    assert_parse_err_expected!(parse_op1_l4, vec!(Token::LiteralInt(3), Token::Eof),
                      "'-'");
}

#[test]
fn test_parse_ident() {
    assert_parse_ok!(parse_ident, vec!(Token::Ident(String::from("a")), Token::Eof),
                     ast::Ident{ span: _, name: _, spl_type: _ });
    assert_parse_err_expected!(parse_ident, vec!(Token::LiteralInt(3), Token::Eof),
                      "an identifier");
}

#[test]
fn test_parse_literal_int() {
    assert_parse_ok!(parse_literal_int, vec!(Token::LiteralInt(3), Token::Eof),
                     ast::LiteralInt{ span: _, value: 3, spl_type: _ });
    assert_parse_err_expected!(parse_literal_int, vec!(Token::LiteralChar('a'), Token::Eof),
                      "a literal int");
}

#[test]
fn test_parse_literal_char() {
    assert_parse_ok!(parse_literal_char, vec!(Token::LiteralChar('a'), Token::Eof),
                     ast::LiteralChar{ span: _, value: 97, spl_type: _ });
    assert_parse_err_expected!(parse_literal_char, vec!(Token::LiteralInt(3), Token::Eof),
                               "a literal char");
}
