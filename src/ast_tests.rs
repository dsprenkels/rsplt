#![cfg(tests)]

use ast::*;
use parser;
use scanner;

fn assert_pretty_print_consistency(source: &str) {
    // generate stage1
    let mut tokens1 = Vec::new();
    let scanner = scanner::Scanner::new(&source).filter(scanner::useful_tokens);
    for token_result in scanner {
        tokens1.push(token_result.unwrap());
    }
    let mut parser = parser::Parser::new(tokens1);
    let ast1 = parser.parse_spl();
    let stage1 = format!("{}", ast1.unwrap());

    // generate stage2
    let mut tokens2 = Vec::new();
    let scanner = scanner::Scanner::new(&stage1).filter(scanner::useful_tokens);
    for token_result in scanner {
        tokens2.push(token_result.unwrap());
    }
    let mut parser = parser::Parser::new(tokens2);
    let ast2 = parser.parse_spl();
    let stage2 = format!("{}", ast2.unwrap());

    // test if equal
    assert_eq!(stage1, stage2);
}

#[test]
fn test_example_code() {
    assert_pretty_print_consistency(include_str!("../spl/example_code.spl"))
}
