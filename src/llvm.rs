use std::fmt;

use ast;
use infer;

macro_rules! saved_spl_type {
    () => {
        fn ty(&self) -> String {
            self.spl_type.borrow().llvm_type()
        }
    };
}

trait LLVMType {
    fn llvm_type(&self) -> String;
    fn llvm_call_type(&self) -> Option<String>;
    fn llvm_return_type(&self) -> Option<String>;
}

impl LLVMType for infer::Type {
    fn llvm_type(&self) -> String {
        match *self {
            infer::Type::Void => "void".to_string(),
            infer::Type::Int => "i32".to_string(),
            infer::Type::Char => "i8".to_string(),
            infer::Type::Function(_, _) => {
                // unwrapping is safe, becasue we know that we are handling a function
                [self.llvm_return_type().unwrap(), self.llvm_call_type().unwrap()].join(" ")
            },
            infer::Type::List(ref x) => format!("{{ i1, {}, i8* }}", x.llvm_type()),
            infer::Type::Pair(ref x, ref y) => format!("{{ {}, {} }}", x.llvm_type(), y.llvm_type()),
            infer::Type::Bool => "i1".to_string(),
            infer::Type::Variable(ref x) => panic!("type inference incomplete (type variable {:?})", x),
            _ => unreachable!(),
        }
    }

    fn llvm_call_type(&self) -> Option<String> {
        match *self {
            infer::Type::Function(ref args, ref ret) => {
                let args = args.iter().map(|x| x.llvm_type())
                                      .collect::<Vec<String>>();
                Some(format!("({})", args.join(", ")))
            },
            _ => None,
        }
    }

    fn llvm_return_type(&self) -> Option<String> {
        match *self {
            infer::Type::Function(_, ref ret) => Some(ret.llvm_type()),
            _ => None,
        }
    }
}

trait ToValue {
    /// return type: Type, Value
    fn ty(&self) -> String;
    fn val(&self, ctx: &mut Context) -> String;
}

impl ToValue for ast::Exp {
    fn ty(&self) -> String {
        match *self {
            ast::Exp::Op1(ref x) => x.ty(),
            ast::Exp::Op2(ref x) => x.ty(),
            ast::Exp::Field(ref x) => x.ty(),
            ast::Exp::FunCall(ref x) => x.ty(),
            ast::Exp::Ident(ref x) => x.ty(),
            ast::Exp::Int(ref x) => x.ty(),
            ast::Exp::Char(ref x) => x.ty(),
            ast::Exp::Bool(ref x) => x.ty(),
            ast::Exp::Pair(ref x) => x.ty(),
            ast::Exp::Paren(ref x) => x.ty(),
            ast::Exp::EmptyList(ref x) => x.ty(),
        }
    }

    fn val(&self, ctx: &mut Context) -> String {
        match *self {
            ast::Exp::Op1(ref x) => x.val(ctx),
            ast::Exp::Op2(ref x) => x.val(ctx),
            ast::Exp::Field(ref x) => x.val(ctx),
            ast::Exp::FunCall(ref x) => x.val(ctx),
            ast::Exp::Ident(ref x) => x.val(ctx),
            ast::Exp::Int(ref x) => x.val(ctx),
            ast::Exp::Char(ref x) => x.val(ctx),
            ast::Exp::Bool(ref x) => x.val(ctx),
            ast::Exp::Pair(ref x) => x.val(ctx),
            ast::Exp::Paren(ref x) => x.val(ctx),
            ast::Exp::EmptyList(ref x) => x.val(ctx),
        }
    }
}

impl ToValue for ast::Op1Exp {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let ty = self.operand.ty();
        let val = self.operand.val(ctx);
        let i = ctx.get_instr_count();
        match self.op1.kind {
            ast::Op1Kind::Not => {
                assert_eq!(ty, "i1");
                ctx.add_instr(format!("%{} = xor {} {}, 1", i, ty, val));
            },
            ast::Op1Kind::Neg => {
                assert_eq!(ty, "i32");
                ctx.add_instr(format!("%{} = sub {} 0, {}", i, ty, val));
            },
        }
        ctx.bump_instr_counter();
        format!("%{}", i)
    }
}

impl ToValue for ast::Op2Exp {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let ty0 = self.left.ty();
        let ty1 = self.right.ty();
        match self.op2.kind {
            ast::Op2Kind::Add => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = add {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Sub => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = sub {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Mul => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = mul {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Div => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = sdiv {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Mod => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = srem {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::And => {
                assert_eq!(ty0, "i1");
                assert_eq!(ty1, "i1");
                let label_i = ctx.get_andlabel_count();
                ctx.bump_andlabel_counter();

                // make and_cond
                let val0 = self.left.val(ctx);
                ctx.add_instr(format!("br {} {}, label %and_true{}, label %and_false{}\n",
                                      ty0, val0, label_i, label_i));

                // make and_false
                ctx.label(format!("and_false{}", label_i));
                ctx.add_instr(format!("store {} {}, i1* %.and_tmp{}", ty0, val0, label_i));
                ctx.add_instr(format!("br label %and_exit{}\n", label_i));

                // make and_true
                ctx.label(format!("and_true{}", label_i));
                let val1 = self.right.val(ctx);
                ctx.add_instr(format!("store {} {}, i1* %.and_tmp{}", ty1, val1, label_i));
                ctx.add_instr(format!("br label %and_exit{}\n", label_i));

                // make and_exit
                ctx.label(format!("and_exit{}", label_i));
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = load i1* %.and_tmp{}", i, label_i));
                ctx.bump_instr_counter();

                format!("%{}", i)
            },
            ast::Op2Kind::Or => {
                assert_eq!(ty0, "i1");
                assert_eq!(ty1, "i1");
                let label_i = ctx.get_orlabel_count();
                ctx.bump_orlabel_counter();

                // make and_cond
                let val0 = self.left.val(ctx);
                ctx.add_instr(format!("br {} {}, label %or_true{}, label %or_false{}\n",
                                      ty0, val0, label_i, label_i));

                // make or_false
                ctx.label(format!("or_false{}", label_i));
                let val1 = self.right.val(ctx);
                ctx.add_instr(format!("store {} {}, i1* %.or_tmp{}", ty1, val1, label_i));
                ctx.add_instr(format!("br label %or_exit{}\n", label_i));

                // make or_true
                ctx.label(format!("or_true{}", label_i));
                ctx.add_instr(format!("store {} {}, i1* %.or_tmp{}", ty0, val0, label_i));
                ctx.add_instr(format!("br label %or_exit{}\n", label_i));

                // make or_exit
                ctx.label(format!("or_exit{}", label_i));
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = load i1* %.or_tmp{}", i, label_i));
                ctx.bump_instr_counter();

                format!("%{}", i)
            },
            ast::Op2Kind::Eq  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp eq {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Ne  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp ne {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Gt  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp sgt {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Ge  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp sge {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Lt  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp slt {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Le  => {
                assert_eq!(ty0, ty1);
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = icmp sle {} {}, {}", i, ty0, val0, val1));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::Op2Kind::Cons => {
                // calculate the size of the current list type (for malloc)
                let val0 = self.left.val(ctx);
                let val1 = self.right.val(ctx);
                let i = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = getelementptr {}* null, i32 1", i, ty1));
                let list_size = ctx.bump_instr_counter();
                ctx.add_instr(format!("%{} = ptrtoint {}* %{} to i32", list_size, ty1, i));
                ctx.bump_instr_counter();

                // malloc space for the new node
                let heap_ptr = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = call i8* @malloc(i32 %{})", heap_ptr, list_size));
                ctx.bump_instr_counter();

                // put the old node in the malloc'd space
                let heap_list_ptr = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = bitcast i8* %{} to {}*", heap_list_ptr, heap_ptr, ty1));
                ctx.bump_instr_counter();
                ctx.add_instr(format!("store {} {}, {}* %{}", ty1, val1, ty1, heap_list_ptr));

                // construct the new root node
                let tmp0 = ctx.get_instr_count();
                ctx.add_instr(format!("%{} = insertvalue {} undef, i1 0, 0", tmp0, ty1));
                let tmp1 = ctx.bump_instr_counter();
                ctx.add_instr(format!("%{} = insertvalue {} %{}, {} {}, 1", tmp1, ty1, tmp0, ty0, val0));
                let list = ctx.bump_instr_counter();
                ctx.add_instr(format!("%{} = insertvalue {} %{}, i8* %{}, 2", list, ty1, tmp1, heap_ptr));
                ctx.bump_instr_counter();
                format!("%{}", list)
            },
        }
    }
}

impl ToValue for ast::FieldExp {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        if self.field.next.is_some() {
            panic!("nested fields are not supported when using LLVM generation");
        }
        let ty = self.var_name.ty();
        let val = self.var_name.val(ctx);
        let i = ctx.get_instr_count();
        match self.field.kind {
            ast::FieldKind::Hd => {
                ctx.add_instr(format!("%{} = extractvalue {} {}, 1", i, ty, val));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
            ast::FieldKind::Tl => {
                // TODO check if this list is empty, preventing a segfault
                ctx.add_instr(format!("%{} = extractvalue {} {}, 2", i, ty, val));
                let tmp = ctx.bump_instr_counter();
                ctx.add_instr(format!("%{} = bitcast i8* %{} to {}*", tmp, i, ty));
                let tmp2 = ctx.bump_instr_counter();
                ctx.add_instr(format!("%{} = load {}* %{}", tmp2, ty, tmp));
                ctx.bump_instr_counter();
                format!("%{}", tmp2)
            },
            ast::FieldKind::Fst | ast::FieldKind::Snd => {
                // The LLVM codegen is virtually the same when retrieving the `fst` or `snd`
                // value, only the pointer that should be read is different
                let idx = match self.field.kind {
                    ast::FieldKind::Fst => 0,
                    ast::FieldKind::Snd => 1,
                    _ => unreachable!(),
                };
                ctx.add_instr(format!("%{} = extractvalue {} {}, {}", i, ty, val, idx));
                ctx.bump_instr_counter();
                format!("%{}", i)
            },
        }
    }
}

impl ToValue for ast::FunCall {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let ref name = self.fun_name.name;
        if name == "isEmpty" {
            return self.val_isEmpty(ctx);
        } else if name == "putChar" {
            return self.val_putChar(ctx);
        }

        let args_str = self.act_args.iter().map(
            |arg| format!("{} {}", arg.ty(), arg.val(ctx))
        ).collect::<Vec<String>>().join(", ");
        let return_ty = self.fun_name.spl_type.borrow().llvm_return_type().unwrap();
        if return_ty == "void" {
            ctx.add_instr(format!("call {} @{}({})", return_ty, name, args_str));
            "void".to_string()
        } else {
            let i = ctx.get_instr_count();
            ctx.add_instr(format!("%{} = call {} @{}({})", i, return_ty, name, args_str));
            ctx.bump_instr_counter();
            format!("%{}", i)
        }
    }
}

impl ToValue for ast::PairExp {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let ty = self.ty();
        let ty0 = self.fst.ty();
        let ty1 = self.snd.ty();
        let val0 = self.fst.val(ctx);
        let val1 = self.snd.val(ctx);
        let i0 = ctx.get_instr_count();
        ctx.add_instr(format!("%{} = insertvalue {} undef, {} {}, 0", i0, ty, ty0, val0));
        let i1 = ctx.bump_instr_counter();
        ctx.add_instr(format!("%{} = insertvalue {} %{}, {} {}, 1", i1, ty, i0, ty1, val1));
        ctx.bump_instr_counter();
        format!("%{}", i1)
    }
}

impl ast::FunCall {
    #[allow(non_snake_case)]
    fn val_isEmpty(&self, ctx: &mut Context) -> String {
        assert_eq!(self.act_args.len(), 1);
        let ty = self.act_args[0].ty();
        let val = self.act_args[0].val(ctx);
        let i = ctx.get_instr_count();
        ctx.add_instr(format!("%{} = extractvalue {} {}, 0", i, ty, val));
        ctx.bump_instr_counter();
        format!("%{}", i)
    }

    #[allow(non_snake_case)]
    fn val_putChar(&self, ctx: &mut Context) -> String {
        assert_eq!(self.act_args.len(), 1);
        let ty = self.act_args[0].ty();
        let val = self.act_args[0].val(ctx);
        ctx.add_instr(format!("call i8 @putchar({} {})", ty, val));
        ctx.bump_instr_counter();
        "void".to_string()
    }
}

impl ToValue for ast::Ident {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let i = ctx.get_instr_count();
        if self.is_global(ctx) {
            ctx.add_instr(format!("%{} = load {}* @{}", i, self.ty(), self.name));
        } else {
            ctx.add_instr(format!("%{} = load {}* %{}", i, self.ty(), self.name));
        }
        ctx.bump_instr_counter();
        format!("%{}", i)
    }

}

impl ast::Ident {
    pub fn is_global(&self, ctx: &mut Context) -> bool {
        let in_global = infer::search_global_environment_var(&ctx.spl, &self).is_some();
        let in_fun = ctx.current_fun_decl.clone().map_or(
                true, |x| infer::search_fun_environment_var(&x, &self).is_some());
        let in_local = ctx.current_fun_decl.clone().map_or(
                true, |x| infer::search_local_environment_var(&x, &self).is_some());
        in_global && (!in_fun) && (!in_local)
    }
}

impl ToValue for ast::LiteralInt {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        format!("{}", self.value as i32)
    }
}

impl ToValue for ast::LiteralChar {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        format!("{}", self.value as u8)
    }
}

impl ToValue for ast::LiteralBool {
    fn ty(&self) -> String {
        match *self {
            ast::LiteralBool::True(ref x) => x.ty(),
            ast::LiteralBool::False(ref x) => x.ty(),
        }
    }

    fn val(&self, ctx: &mut Context) -> String {
        match *self {
            ast::LiteralBool::True(ref x) => x.val(ctx),
            ast::LiteralBool::False(ref x) => x.val(ctx),
        }
    }
}

impl ToValue for ast::LiteralTrue {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String { "1".to_string() }
}

impl ToValue for ast::LiteralFalse {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String { "0".to_string() }
}

impl ToValue for ast::ParenExp {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String { self.exp.val(ctx) }
}

impl ToValue for ast::EmptyList {
    saved_spl_type!();
    fn val(&self, ctx: &mut Context) -> String {
        let ty = if let infer::Type::List(ref inner) = *self.spl_type.borrow() {
            inner.llvm_type()
        } else {
            unreachable!()
        };
        format!("{{ i1 1, {} undef, i8* undef }}", ty)
    }
}

#[derive(Debug)]
pub struct Function {
    sig: String,
    body: String,
}

impl fmt::Display for Function {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        try!(writeln!(f, "{} {{", self.sig));
        try!(write!(f, "{}", self.body));
        writeln!(f, "}}")
    }
}

impl Function {
    pub fn add_llvm<T: ToString>(&mut self, instr: T) {
        self.body.push_str(instr.to_string().as_str());
    }

    pub fn add_llvm_front<T: ToString>(&mut self, instr: T) {
        let mut new_body = instr.to_string();
        new_body.push_str(&self.body);
        self.body = new_body;
    }
}

#[derive(Debug)]
pub enum Item {
    Fun(Function),
    Misc(String),
}

impl fmt::Display for Item {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Item::Fun(ref function) => write!(f, "{}", function),
            Item::Misc(ref misc) => writeln!(f, "{}", misc),
        }
    }
}

#[derive(Debug)]
pub struct Context {
    spl: ast::SPL,
    current_fun_decl: Option<ast::FunDecl>,
    module: Vec<Item>,
    current_function: Option<Function>,
    instr_counter: u32,
    andlabel_counter: u32,
    orlabel_counter: u32,
    iflabel_counter: u32,
    whilelabel_counter: u32,
    label: Option<String>,
}

impl fmt::Display for Context {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for item in self.module.iter() {
            try!(writeln!(f, "{}", item));
        }
        Ok(())
    }
}

impl Context {
    pub fn new(spl: &ast::SPL) -> Self {
        Context {
            spl: spl.clone(),
            current_fun_decl: None,
            module: Vec::new(),
            current_function: None,
            instr_counter: 0,
            andlabel_counter: 0,
            orlabel_counter: 0,
            iflabel_counter: 0,
            whilelabel_counter: 0,
            label: None,
        }
    }

    pub fn from(spl: ast::SPL) -> Self {
        let mut ctx = Self::new(&spl);
        spl.make_llvm(&mut ctx);
        ctx
    }

    pub fn add_item<T: ToString>(&mut self, item: T) {
        let x = Item::Misc(item.to_string());
        self.module.push(x);
    }

    pub fn function_begin<T: ToString>(&mut self, signature: T) {
        if self.current_function.is_some() {
            panic!("Context.current_funtion is some value, call function_end() first.");
        }
        let sig = signature.to_string();
        if !sig.starts_with("define") {
            panic!("function definition does not start with \"define\": \n{}", sig);
        }
        let function = Function {
            sig: sig,
            body: String::new(),
        };
        self.current_function = Some(function);
        self.instr_counter = 0;
        self.andlabel_counter = 0;
        self.orlabel_counter = 0;
        self.iflabel_counter = 0;
        self.whilelabel_counter = 0;
    }

    pub fn function_end(&mut self) {
        if self.current_function.is_none() {
            panic!("Context.current_funtion is None, cannot add function to the LLVM module.");
        }
        self.add_instr("unreachable");
        // The call to unwrap() is safe by the if-statement above
        let mut function = self.current_function.take().unwrap();

        // hack stack space for logical operator return values
        for i in 0..self.andlabel_counter {
            let instr = format!("  %.and_tmp{} = alloca i1\n", i);
            function.add_llvm_front(instr);
        }
        for i in 0..self.orlabel_counter {
            let instr = format!("  %.or_tmp{} = alloca i1\n", i);
            function.add_llvm_front(instr);
        }
        function.add_llvm_front("entry:\n");

        self.module.push(Item::Fun(function));
    }

    pub fn add_instr<T: ToString>(&mut self, instr: T) {
        if let Some(ref mut function) = self.current_function {
            if let Some(label) = self.label.take() {
                function.add_llvm(format!("{}:\n", label));
            }
            function.add_llvm(format!("  {}\n", instr.to_string()));
        } else {
            self.module.push(Item::Misc(instr.to_string()));
        }
    }

    pub fn label<T: ToString>(&mut self, label: T) {
        if self.label.is_some() {
            panic!("Some label already set");
        }
        self.label = Some(label.to_string());
    }

    pub fn get_instr_count(&self) -> u32 {
        self.instr_counter
    }

    pub fn get_iflabel_count(&self) -> u32 {
        self.iflabel_counter
    }

    pub fn get_andlabel_count(&self) -> u32 {
        self.andlabel_counter
    }

    pub fn get_orlabel_count(&self) -> u32 {
        self.orlabel_counter
    }

    pub fn get_whilelabel_count(&self) -> u32 {
        self.whilelabel_counter
    }

    pub fn bump_instr_counter(&mut self) -> u32 {
        self.instr_counter += 1;
        self.get_instr_count()
    }

    pub fn bump_andlabel_counter(&mut self) -> u32 {
        self.andlabel_counter += 1;
        self.get_andlabel_count()
    }

    pub fn bump_orlabel_counter(&mut self) -> u32 {
        self.orlabel_counter += 1;
        self.get_orlabel_count()
    }

    pub fn bump_iflabel_counter(&mut self) -> u32 {
        self.iflabel_counter += 1;
        self.get_iflabel_count()
    }

    pub fn bump_whilelabel_counter(&mut self) -> u32 {
        self.whilelabel_counter += 1;
        self.get_whilelabel_count()
    }
}

impl ast::SPL {
    pub fn make_llvm(&self, ctx: &mut Context) {
        self.check_main();
        self.prelude(ctx);
        for decl in self.decls.iter() {
            decl.make_llvm_global(ctx);
        }
    }

    fn prelude(&self, ctx: &mut Context) {
        // declare libc print function for integers
        ctx.add_item(
r#"; format string to be used with libc's printf(...)
@.format_int_ln = private unnamed_addr constant [4 x i8] c"%i\0A\00"
@.format_int = private unnamed_addr constant [3 x i8] c"%i\00"

; declaration of libc's printf(...) function
declare i32 @printf(i8* noalias nocapture, ...)

; declaration of the putchar function
declare i8 @putchar(i8)

; declaration of memcpy
declare void @llvm.memcpy.p0i8.p0i8.i32(i8* nocapture, i8* nocapture readonly, i32, i32, i1)
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* nocapture, i8* nocapture readonly, i64, i32, i1)

; declaration of malloc
declare noalias i8* @malloc(i32)

; declaration of free
declare void @free(i8*)

"#);
        // define print function
        ctx.function_begin(r#"define void @print(i32 %val)"#);
        ctx.add_instr("%format_str = getelementptr [3 x i8]* @.format_int, i64 0, i64 0");
        ctx.add_instr("call i32 (i8*, ...)* @printf(i8* %format_str, i32 %val)");
        ctx.add_instr("ret void");
        ctx.function_end();

        // define println function
        ctx.function_begin(r#"define void @println(i32 %val)"#);
        ctx.add_instr("%format_str = getelementptr [4 x i8]* @.format_int_ln, i64 0, i64 0");
        ctx.add_instr("call i32 (i8*, ...)* @printf(i8* %format_str, i32 %val)");
        ctx.add_instr("ret void");
        ctx.function_end();
    }

    fn check_main(&self) {
        // check if a main function is present and if it returns Int
        let mut main_present = false;
        for decl in self.decls.iter() {
            if let ast::Decl::Fun(ref fun_decl) = *decl {
                if fun_decl.name.name == "main" {
                    if let infer::Type::Function(_, ref ret) = *fun_decl.name.spl_type.borrow() {
                        if **ret != infer::Type::Int {
                            panic!("`main` function does not return Int");
                        }
                        main_present = true;
                        break;
                    } else {
                        unreachable!();
                    }
                }
            }
        }
        if !main_present {
            panic!("`main` function does not exist");
        }
    }
}

impl ast::Decl {
    pub fn make_llvm_global(&self, ctx: &mut Context) {
        match *self {
            ast::Decl::Var(ref var_decl) => var_decl.make_llvm_global(ctx),
            ast::Decl::Fun(ref fun_decl) => fun_decl.make_llvm(ctx),
        }
    }
}

impl ast::FunDecl {
    pub fn make_llvm(&self, ctx: &mut Context) {
        // skip builtins
        if self.name.name.starts_with("__builtin__.") { return; }

        ctx.current_fun_decl = Some(self.clone());

        let mut llvm_args = Vec::with_capacity(self.args.len());
        for (i, spl_arg) in self.args.iter().enumerate() {
            let llvm_arg = spl_arg.ty();
            llvm_args.push(format!("{} %.arg{}", llvm_arg, i));
        }
        let return_type = if let infer::Type::Function(_, ref ty) = *self.name.spl_type.borrow() {
            ty.llvm_type()
        } else {
            unreachable!();
        };
        let signature = format!(r#"define {} @{}({})"#,
                                return_type, self.name.name, llvm_args.join(", "));
        ctx.function_begin(signature);
        for (i, spl_arg) in self.args.iter().enumerate() {
            let ty = spl_arg.ty();
            let val = &spl_arg.name;
            ctx.add_instr(format!("%{} = alloca {}", val, ty));
            ctx.add_instr(format!("store {} %.arg{}, {}* %{}", ty, i, ty, val));
        }

        for var in self.vars.iter() {
            var.make_llvm_local(ctx);
        }

        for stmt in self.body.iter() {
            stmt.make_llvm(ctx);
        }

        // TODO Require a `ret` instruction before Context::function_end() adds `unreachable`?

        ctx.function_end();
        ctx.current_fun_decl = None;
    }
}

impl ast::VarDecl {
    pub fn make_llvm_global(&self, ctx: &mut Context) {
        let ty = self.value.ty();
        let val = self.value.val(ctx);
        ctx.add_instr(format!("@{} = global {} {}", &self.name, ty, val));
    }

    pub fn make_llvm_local(&self, ctx: &mut Context) {
        let ty = self.value.ty();
        let val = self.value.val(ctx);
        ctx.add_instr(format!("%{} = alloca {}", &self.name, ty));
        ctx.add_instr(format!("store {} {}, {}* %{}", ty, val, ty, &self.name));
    }
}

impl ast::Stmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        match *self {
            ast::Stmt::If(ref stmt) => stmt.make_llvm(ctx),
            ast::Stmt::While(ref stmt) => stmt.make_llvm(ctx),
            ast::Stmt::Ass(ref stmt) => stmt.make_llvm(ctx),
            ast::Stmt::FunCall(ref stmt) => stmt.make_llvm(ctx),
            ast::Stmt::Return(ref stmt) => stmt.make_llvm(ctx),
        }
    }
}

impl ast::IfStmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        let i = ctx.get_iflabel_count();
        ctx.bump_iflabel_counter();
        ctx.add_instr(format!("br label %ifelse_cond{}\n", i));

        // make ifelse_cond
        ctx.label(format!("ifelse_cond{}", i));
        let ty = self.cond.ty();
        if ty != "i1" {
            panic!("Type of if-conditional should be Bool");
        }
        let val = self.cond.val(ctx);
        let instr = format!("br {} {}, label %then_body{}, label %else_body{}\n", ty, val, i, i);
        ctx.add_instr(instr);

        // make then_body
        ctx.label(format!("then_body{}", i));
        for stmt in self.if_body.iter() {
            stmt.make_llvm(ctx);
        }
        ctx.add_instr(format!("br label %ifelse_exit{}\n", i));

        // make else_body
        ctx.label(format!("else_body{}", i));
        for stmt in self.else_body.iter() {
            stmt.make_llvm(ctx);
        }
        ctx.add_instr(format!("br label %ifelse_exit{}\n", i));

        // make ifelse_exit
        ctx.label(format!("ifelse_exit{}", i));
    }
}

impl ast::WhileStmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        let i = ctx.get_iflabel_count();
        ctx.bump_whilelabel_counter();
        ctx.add_instr(format!("br label %while_cond{}\n", i));

        // make while_cond
        ctx.label(format!("while_cond{}", i));
        let ty = self.cond.ty();
        if ty != "i1" {
            panic!("Type of while-conditional should be Bool");
        }
        let val = self.cond.val(ctx);
        let instr = format!("br {} {}, label %while_body{}, label %while_exit{}\n", ty, val, i, i);
        ctx.add_instr(instr);

        // make while_body
        ctx.label(format!("while_body{}", i));
        for stmt in self.body.iter() {
            stmt.make_llvm(ctx);
        }
        ctx.add_instr(format!("br label %while_cond{}\n", i));

        // make while_exit
        ctx.label(format!("while_exit{}", i));
    }
}

impl ast::AssStmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        let llvm_ident = if self.var_name.is_global(ctx) {
            format!("@{}", self.var_name.name)
        } else {
            format!("%{}", self.var_name.name)
        };
        let self_ty = self.var_name.ty();
        let value_ty = self.value.ty();
        let val = self.value.val(ctx);
        match self.field {
            Some(ref field) => {
                if field.next.is_some() {
                    panic!("nested fields are not allowed in assignments");
                }
                match field.kind {
                    ast::FieldKind::Hd => {
                        // TODO check if we are assigning to an empty list, preventing UB
                        // At this point we can assume that we are handling a non-empty list,
                        // so we don't need to touch the nil and the next ptr field.
                        let i = ctx.get_instr_count();
                        // Get a pointer to the location of the next pointer field
                        ctx.add_instr(format!("%{} = getelementptr {}* {}, i32 0, i32 1",
                                              i, self_ty, llvm_ident));
                        ctx.bump_instr_counter();
                        ctx.add_instr(format!("store {} {}, {}* %{}", value_ty, val, value_ty, i));

                    },
                    ast::FieldKind::Tl => {
                        // TODO check if we are assigning to an empty list, preventing UB
                        assert_eq!(self_ty, value_ty);
                        let ty = self_ty;

                        // At this point we can assume that we are handling a non-empty list,
                        // so we don't need not to touch the nil field and the inner value.

                        // calculate the size of the current list type (for malloc)
                        let i = ctx.get_instr_count();
                        ctx.add_instr(format!("%{} = getelementptr {}* null, i32 1", i, ty));
                        let list_size = ctx.bump_instr_counter();
                        ctx.add_instr(format!("%{} = ptrtoint {}* %{} to i32", list_size, ty, i));
                        ctx.bump_instr_counter();

                        // malloc space for the new node
                        let heap_ptr = ctx.get_instr_count();
                        ctx.add_instr(format!("%{} = call i8* @malloc(i32 %{})", heap_ptr, list_size));
                        ctx.bump_instr_counter();

                        // store the `value` list on the heap
                        let heap_list_ptr = ctx.get_instr_count();
                        ctx.add_instr(format!("%{} = bitcast i8* %{} to {}*", heap_list_ptr, heap_ptr, ty));
                        ctx.bump_instr_counter();
                        ctx.add_instr(format!("store {} {}, {}* %{}", ty, val, ty, heap_list_ptr));

                        // update the `next` pointer to point to the new tail
                        let next_ptr = ctx.get_instr_count();
                        ctx.add_instr(format!("%{} = getelementptr {}* {}, i32 0, i32 2",
                                              next_ptr, ty, llvm_ident));
                        ctx.bump_instr_counter();
                        ctx.add_instr(format!("store i8* %{}, i8** %{}", heap_ptr, next_ptr));
                    },
                    ast::FieldKind::Fst | ast::FieldKind::Snd => {
                        // The LLVM codegen is virtually the same when assigning to fst and snd,
                        // only the pointer that should be written to is different
                        let idx = match field.kind {
                            ast::FieldKind::Fst => 0,
                            ast::FieldKind::Snd => 1,
                            _ => unreachable!(),
                        };
                        let i = ctx.get_instr_count();
                        // get a pointer to the location of the first element
                        ctx.add_instr(format!("%{} = getelementptr {}* {}, i32 0, i32 {}",
                                              i, self_ty, llvm_ident, idx));
                        ctx.bump_instr_counter();
                        ctx.add_instr(format!("store {} {}, {}* %{}", value_ty, val, value_ty, i))
                    },
                }
            },
            None => {
                assert_eq!(self_ty, value_ty);
                ctx.add_instr(format!("store {} {}, {}* {}", value_ty, val, value_ty, llvm_ident))
            }
        }
    }
}

impl ast::FunCallStmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        self.fun_call.val(ctx);
    }
}

impl ast::ReturnStmt {
    pub fn make_llvm(&self, ctx: &mut Context) {
        if let Some(ref value) = self.value {
            let ty = value.ty();
            if ty != "void" {
                let val = value.val(ctx);
                ctx.add_instr(format!("ret {} {}", ty, val));
            } else {
                // still call the function (we don't need to save the return value)
                value.val(ctx);
                ctx.add_instr(format!("ret {}", ty));
            }
        } else {
            ctx.add_instr(format!("ret void"));
        }
        ctx.bump_instr_counter();
    }
}
