\documentclass[a4paper, 10pt]{article}

\usepackage[utf8]{inputenc}
\usepackage{fancyvrb}
\usepackage[british]{babel}
\usepackage{parskip}
\usepackage{fullpage}
\usepackage{hyperref}

\fvset{numbers=left}

\title{Compiler Construction: \texttt{rsplt}}
\author{Gerdriaan Mulder \and Daan Sprenkels}
\date{June 2016}

\begin{document}

\maketitle
\tableofcontents
\newpage

\section{Introduction}

Over the past couple of months we have built \texttt{rsplt}, our SPL compiler. We have
chosen Rust as the programming language to write the compiler in. Rust is not a functional
language, as was recommended, but has a relatively modern feature set. For instance, it
does support pattern matching and it has a rich macro system, which which one can use for
writing monadic design patterns.

We will talk about division of work, lexing and parsing, type checking, code
generation (in SSM), and our extension: LLVM code generation.

\subsection{Division of work}
In the beginning of the project we had devised to write our code together as much as
possible. This however turned out to be very hard, because due to our different span
of courses our schedules were terribly incompatible. Next to that, Gerdriaan did not
know Rust at all before the start of this project. The consequence of this was often
that one person knew a specific piece of code very well, while the other would not.
Fortunately, we have mostly thought up the designs of the code together. In practice,
Daan was generally the person writing the code, while Gerdriaan often made the presentations
and did the reporting and provided SPL programs to test with.

\paragraph{Lexer and parser}
Daan has written slightly more code of the lexer and parser. Gerdriaan has made the presentation.

\paragraph{Type checking}
Pretty much all of the code in this part has been written by Daan or it has been
written together. In this case it was also Gerdriaan who has made the presentation
and the corresponding report.

\paragraph{SSM codegen}
This has mainly been implemented by Gerdriaan. We have not used slides for the
presentation of this pass.

\paragraph{LLVM codegen}
In correspondence to Gerdriaan having done the SSM code generation, Daan has implemented
the vast majority of the code for the LLVM code generation. He has also made the
presentation for this part.

\subsection{Statistics}
Some say Bill Gates once said ``Measuring programming progress by lines of code is
like measuring aircraft building progress by
weight''\footnote{\url{http://ask.metafilter.com/114578/Did-he-really-say-that}}. 

Nevertheless, we'd gladly provide some statistics.

Total time spent is hard to say. Given the busy schedule of both of us, we worked
mostly at these hours of
day\footnote{\url{https://gitlab.science.ru.nl/dsprenkels/rsplt/graphs/master/commits}}: 
17h (32 commits), 15h (26 commits) and 16h (21 commits).

The number of 
commits\footnote{\url{https://gitlab.science.ru.nl/dsprenkels/rsplt/graphs/master/contributors}} 
may also be an indication of time spent: Daan: 192, Gerdriaan: 72, total number of
commits on \texttt{master}: 310\footnote{Yes, this does not add up. We're not sure
what Gitlab was thinking}. We also used \emph{continuous integration} to test whether
our implementation matched our tests (in most cases). In total, we had 1142 builds.
This number should be roughly divided by 3,5 to come close to the total number of
commits that were tested automatically. We used 3 versions of Rust to compile our
code on (stable, beta and nightly) and we created another test suite to specifically
test LLVM code.

As for lines of code:
\begin{Verbatim}[fontsize=\footnotesize]
$ ls src/
ast.rs            ast_tests.rs  codegen.rs  infer.rs         infer_tests.rs
llvm.rs           main.rs       parser.rs   parser_tests.rs  scanner.rs
scanner_tests.rs  util.rs
$ cd src
$ cloc *.rs
      12 text files.
      12 unique files.                              
       0 files ignored.

-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Rust                            12            601            275           5487

\end{Verbatim}

\section{Lexing and parsing}

After defining all tokens that would be present in our language, we have built the
scanner. This has been pretty straightforward. Thereafter we started designing the
parser by looking how we wanted our AST to look like. Although the programming
language we chose to use for building our SPL compiler does support algebraic
data types, we considered that it would be globally preferable to split the AST in
nodes of two categories: \texttt{enum} nodes and \texttt{struct} nodes. \texttt{enum}
nodes that branch in to multiple possible sub-nodes where \texttt{struct} nodes are
terminals or nodes containing multiple sub-nodes.

While writing our parser (recursive descent) we have rewritten our grammar so that
it mirrored the parser itself. We have also paid a lot of attention to keeping track
of our errors by saving in each node the start and end locations in the source code\footnote{%
Because we had underestimated the effort needed for the implementation of the type
checker, we have only implemented this for the lexer/parser.}. When errors occur, the
parser will report errors like these:

\begin{Verbatim}
error: expected '{'
5:21     if (i % 3 == 0) || i % 5 == 0) {
                         ^~
\end{Verbatim}

\subsection{Grammar for SPL}

The grammar that we have ultimately produced is practically a one-on-one mirror of
the structure of our parser. As you can see, we have added a lot of nodes. This is
due to the rationale as explained in the previous section.

We have tried to eliminate all non-deterministic behaviours and have adapted the
grammar so that it coheres to our own rules of operator associativity. The result
is this:

\begin{Verbatim}
SPL         = Decl+
Decl        = VarDecl
            | FunDecl
VarDecl     = VarKwOrType id '=' Exp ';'
VarKwOrType = VarKw
            | Type
VarKw       = 'var'
FunDecl     = id '(' [ id ( ',' id )* ] ')' [ '::' FunType ]
              '{' VarDecl* Stmt+ '}'
RetType     = Type
            | Void
Void        = 'Void'
FunType     = [ Type+ '->' ] RetType
Type        = BasicType
            | PairType
            | ListType
            | id
BasicType   = 'Int'
            | 'Bool'
            | 'Char'
PairType    = '(' Type ',' Type ')'
ListType    = '[' Type ']'
Stmt        = IfStmt
            | WhileStmt
            | ReturnStmt
            | AssStmt
            | FunCallStmt
Stmts       = Stmt*
IfStmt      = 'if' ParenExp '{' Stmt* '}'
              [ 'else' '{' Stmt* '}' ]
WhileStmt   = 'while' ParenExp '{' Stmt* '}'
AssStmt     = id [ Field ] '=' Exp ';'
FunCallStmt = FunCall ';'
ReturnStmt  = 'return' [ Exp ] ';'

Exp         = ExpL1
ExpL1       = Op1ExpL1
            | Op2ExpL1
            | ExpL2
ExpL2       = Op2ExpL2
            | ExpL3
ExpL3       = Op2ExpL3
            | ExpL4
ExpL4       = Op1ExpL4
            | Op2ExpL4
            | ExpL5
ExpL5       = Op2ExpL5
            | ExpL6
ExpL6       = FieldExp
            | FunCall
            | id
            | int
            | char
            | bool
            | PairExp
            | ParenExp
            | EmptyList
ParenExp    = '(' Exp ')'
FieldExp    = id Field

Op2ExpL1    = ExpL2 Op2L1 ExpL2 [ Op2ExpL1` ]
Op2ExpL1`   = Op2L1 ExpL2 [ Op2ExpL1` ]

Op2ExpL2    = ExpL3 Op2L2 ExpL3 [ Op2ExpL2` ]
Op2ExpL2`   = Op2L2 ExpL3 [ Op2ExpL2` ]

<!-- The following level is for the cons operator and should be
     right associative -->
Op2ExpL3    = ExpL4 Op2L3 ExpL3

Op2ExpL4    = ExpL5 Op2L4 ExpL5 [ Op2ExpL4` ]
Op2ExpL4`   = Op2L4 ExpL5 [ Op2ExpL4` ]

Op2ExpL5    = ExpL6 Op2L5 ExpL6 [ Op2ExpL5` ]
Op2ExpL5`   = Op2L5 ExpL6 [ Op2ExpL5` ]

Op1ExpL1    = Op1L1 Exp
Op1ExpL4    = Op1L4 Exp

bool        = True | False
True        = 'True'
False       = 'False'
PairExp     = '(' Exp ',' Exp ')'
Field       = '.' FieldKind [ Field ]
FieldKind   = 'hd' | 'tl' | 'fst' | 'snd'
FunCall     = id '(' [ Exp ( ',' Exp )* ] ')'
Op2L1       = '&&' | '||'
Op2L2       = '==' | '!=' | '<' | '<=' | '>' | '>='
Op2L3       = ':'
Op2L4       = '+' | '-'
Op2L5       = '*' | '/' | '%'
Op1L1        = '!'
Op1L4        = '-'
id          = alpha ( '_' | alphaNum )*
int         = [ '-' ] digit+
EmptyList   = []
\end{Verbatim}

\section{Type checking}

\subsection{Scoping rules}
We created \emph{environments}\footnote{Another term that is commonly used
is \emph{namespace}.} to deal with scoping. In an environment, we keep track
of variable names and function names. These names are ``stored'' in separate
containers, so it is possible to use the same name for a function and a 
variable in the same scope. The three environments are:
\begin{enumerate}
\item global
\item function
\item local
\end{enumerate}

The \emph{global environment} contains all \texttt{VarDecl}s and
\texttt{FunDecl}s. We use the \emph{function environment} for variable
names from the \texttt{FunDecl}s and the \emph{local environment} for
the \texttt{VarDecl}s within a function. Variable name lookups are done bottom-up,
so first the local environment is checked, then the function environment, and
finally the global environment. Since functions can only be defined at the global
level, function name lookups are done directly in the global environment.

Although we mention that we \emph{store} function and variable names in the environments,
the environments are created on the fly. We do this because storing extra information
that is readily available in the abstract syntax tree seemed inefficient.

Although this system of environments is very easy, it has one major drawback:
From some scope, the compiler is only able to access variables from upper environments.
For example, let us consider the following SPL program:

\begin{Verbatim}
main () :: Int {
    var x = 3;
    var y = x * x;
    print(y);
}
\end{Verbatim}

This program will not compile, because the scope at the point of initialisation of
\texttt{x} is the same as at the initialisation of \texttt{y}. Therefore, when
initialising \texttt{y}, one cannot access \texttt{x} yet.\footnote{A better
method of scoping should be remembering a different scope for each variable
declaration.} This is in all cases not a problem, because we are able to assign
\texttt{y} later on in the program:

\begin{Verbatim}
main () :: Int {
    var x = 3;
    var y = 0;
    y = x * x;
    print(y);
}
\end{Verbatim}

\subsection{Typing rules}
There are a few types that we distinguish in our \texttt{enum Type}:
\begin{itemize}
\item \texttt{Variable}
\item \texttt{Void}
\item \texttt{Int}
\item \texttt{Char}
\item \texttt{[Type]} (list)
\item \texttt{(Type, Type)} (tuple)
\item \texttt{Bool}
\item \texttt{Function}
\end{itemize}

Most of them are pretty straightforward. \texttt{FunctionType} needs a bit of explanation.
We started with two different type of \texttt{FunctionType}s: normal and polymorphic.
Normal functions only have one implementation, whereas polymorphic functions can have
multiple implementations for the same function signature. After a bit of coding, we noticed
that we did not need an extra type: a normal function can be expressed as a polymorphic
function with only one implementation.

Our original approach was to assume every function to be polymorphic (although
it may not have been at all in practice). We define a function to have one
\emph{template} and a set of \emph{signatures} (instances). As we encounter calls to the
function where all the arguments and the return value were consisting of
primitive types, we would try to instantiate the function. If an instance
had already been created, this instance would of course be re-used. In the
translation pass we would just generate code for each of the function instances
and treat them as separate functions when calling them.

\subsection{Type Inference}
We apply the following algorithm for type inference:

\begin{enumerate}
\item Recursively execute \texttt{try\_inference} on each node in the AST.
\item Return \texttt{True} is some inference has taken place and \texttt{False}
      if type inference has been completed.
\item Go to (1) until all return values are \texttt{false}.
\item Iterate over all AST nodes again to check whether each expression and function
      is typed, because there can still occur free variables of which the type is still
      variable.
\end{enumerate}

\section{SSM code generation}
SSM, or \emph{Simple Stack Machine} has an assembly-like language that contains
simple constructs like labels branching, computational operators, loading and storing
of values on a heap and a stack.

The code generation for SSM was done in two parts. The first part contained all separate
instructions without any implementation logic (\texttt{src/codegen.rs}). The second
part was implemented in \texttt{src/ast.rs}, because it needed the information that
was added during type inference.

\subsection{Code generation is like pretty printing}
We saw SSM code generation as another form of a \emph{pretty printer}. Directly printing
assembly instructions would be cumbersome and not flexible enough for changing syntax.
In the first part, we created a similar, tree-like, structure as in the abstract syntax 
tree.

An SSM program in our implementation consists of several \texttt{SSMLine}s. We
distinguished the following cases for an \texttt{SSMLine}:
\begin{itemize}
    \item a label
    \item an instruction
    \item a comment\footnote{The comments in SPL are filtered and will never reach
            this part of the code. For debugging purposes it was handy to add
            descriptions to parts of the assembly program to indicate what one line
            of SPL generated in SSM. The \emph{cons} operator generates three SSM
            instructions, for example.}
    \item a literal (string or number)
\end{itemize}

This tree-like structure makes it easy to distinguish the several layers of abstraction
in the SSM assembly language. It also makes it a pleasure to read the source code,
although a simple \texttt{LDC -1} creates a rather long string in Rust, as can be seen
on line 2 below. A couple of other examples (in Rust):
\begin{Verbatim}[fontsize=\footnotesize]
let mut output: Vec<SSMLine> = Vec::new();
output.push(SSMLine::Instruction(Operation::Load(LoadType::Constant(OpArg::LiteralInt(-1)))));
output.push(SSMLine::Instruction(Operation::Compute(ComputeType::Eq)));
output.push(SSMLine::Comment(String::from("fst")));
output.push(SSMLine::Literal(SSMLiteral::LiteralString(String::from("VarRead_")+&self.var_name.clone().name)));
output.push(SSMLine::SSMLabel(String::from(format!("whilestmt_{}_cond", whilestmtcount))));
\end{Verbatim}

In the second part we actually built a program that could be interpreted by the
SSM. It works similar to the pretty printer of SPL itself.

Every instruction in SPL needs one or more instructions in SSM. Most instructions
are rather simple and only require one SSM instruction, without any added logic
to it. The most obvious are the \texttt{ComputeType}s. You place the correct
variable value(s) on the stack and execute the specific \texttt{ComputeType}.

\subsection{Reading and writing variables}
A particular implementation was needed for reading and writing of variables. We needed
to adhere to the scoping rules, so we added an intermediate representation
for those specifically. On line 5 above you can see an example. It denotes
that a variable should be read (\texttt{VarRead\_}) and that the variable
name is concatenated to it. Since we only have \texttt{Stmt}s inside
functions, the implementation for SSM for \texttt{FunDecl} was extended
to loop over all instructions inside it again after translating it to
SSM. In that step, the correct \emph{local index} was given to the
\emph{load local} instruction
\texttt{LDL}. See further on: calling convention.

The same was done for storing of variables (denoted with \texttt{VarStore\_}),
and we added special cases for storing, for example, the head of a list (see the
\texttt{FieldExp} in the grammar). Storing a value inside a \texttt{Field} appeared
easy at first, but the SSM assembly language has some (intentional?) quirks that
make life more difficult.

\subsection{Spotting bugs is hard}
The following statement appeared very troublesome:
\begin{Verbatim}[fontsize=\footnotesize]
// list is of type [Int].
while ((! isEmpty (list)) && list . hd % 2 == 0) {
    list = list . tl;
}
\end{Verbatim}

The problem lies in the nested \texttt{ParenExp} with \texttt{\&\&}, which generates
the bitwise \texttt{AND}
instruction. The empty list is implemented as two values of \texttt{-1} (or
\texttt{0xffffffff}) on the heap\footnote{This is needed to accommodate the existence
of 0 in a list}. \texttt{isEmpty()} returns \texttt{0x00000000} in
case it is true. The \texttt{!} operator makes this \texttt{0xffffffff}. Next on,
when \texttt{list . hd} is the empty list, we \texttt{AND} \texttt{0xffffffff}
with itself, yielding the same output. 

The \texttt{NOT} instruction inverts the current value at the stack pointer and
does not adjust the stack pointer afterwards. So, in case of nested expressions
we need to save the result of the inner expression(s).

However, this code worked fine when the correct adjustments were made to save
\texttt{list} in line 2\footnote{See commit \texttt{3f7ff39} if you have access
to our Git repository}. This shows that you can look over subtle details when
you look at something you \emph{think} is wrong, but is not the problem at all.

\subsection{Calling convention}
Regarding calling convention, the following happens when calling a function
(see \texttt{impl ToSSMCode for FunCall} in \texttt{src/ast.rs}):
\begin{enumerate}
    \item all function arguments are pushed to the stack
    \item \texttt{print}, \texttt{putChar} and \texttt{isEmpty} have different
            implementations that are inlined when called. After that, normal
            execution of the program continues
    \item we execute \texttt{BSR function\_name} (branch subroutine)
    \item after execution we reset the stack pointer\footnote{Another method
    was to load the function name and call \texttt{JSR}: jump subroutine} to
    the position it was before the branch
    \item we push the contents of the \texttt{RR} register on the stack.
\end{enumerate}

When a function is called, the following steps happen within a function (see
\texttt{impl ToSSMCode for FunDecl} in \texttt{src/ast.rs}):
\begin{enumerate}
    \item we reserve room for function variables and local variables using
            \texttt{LINK x} with x the sum of the number of function and
            local variables
    \item we load all function variables using a negative offset from the mark
            pointer into the reserved local room. We also save them for lookup
            later
    \item the same happens with the local variables
    \item the function body is executed
    \item if we execute the \texttt{main} function, we remove the return statement, 
            because
            the machine should halt after that (we do not print return codes). We
            add the \texttt{HALT} instruction after the last line of the main
            function
    \item we loop over all the output code to replace all variable reads and
            stores with the appropriate \texttt{LDL} and \texttt{STL} instructions.
            Special attention is needed for lists and fields. To store the tail
            of a list, we need more than one instruction.
\end{enumerate}

There are some programs which run fine when compiled to SSM, some do not. For example,
\texttt{squareOddNumbers.spl} is quite simple and does what it is supposed to do. A
more complex example such as \texttt{insertsort.spl} gives several \texttt{ifstmtcond}
blocks for the same \texttt{if}-statement. This is probably caused by an implementation
issue in Rust, where usage of global variables is discouraged. There should be a fix
for this, but given that Gerdriaan wrote this part (and his little experience with
Rust) and time issues, this was not fixed.

Although we aimed for flexibility in implementation, LLVM has a completely different
structure regarding instructions. Next to that, code generation for SSM was not
finished in time, so we sought advice on this problem and decided to split the
work such that we could also deliver the compiler extension.

\section{Compiler extension: LLVM code generation}
We decided that our compiler extension should be \emph{code generation for the LLVM
backend}. We had just had a lot of difficulties and frustrations in the implementation
of the type checking pass. Therefore, we did not want to do an extension that would
need us to more stuff related to typing. On the other hand, the idea of \texttt{rsplt}
being able to write real x86 assembly that could actually be executed seemed pretty
cool to us.

Because we hadn't worked with LLVM any time before, we took the LLVM IR reference and
started writing small programs in LLVM intermediate language. One of the first programs
in LLVM was calculating Fibonacci numbers:

\begin{Verbatim}[fontsize=\footnotesize]
; Declare the string constant as a global constant.
@.format_int = private unnamed_addr constant [4 x i8] c"%i\0A\00"

declare i32 @printf(i8* noalias nocapture, ...)

define void @print_int(i32) {
    %2 = getelementptr [4 x i8]* @.format_int, i64 0, i64 0
    call i32 (i8*, ...)* @printf(i8* %2, i32 %0)
    ret void
}

define i32 @fib(i32 %arg0) {
    %i = alloca i32
    %a = alloca i32
    %b = alloca i32
    store i32 0, i32* %i
    store i32 0, i32* %a
    store i32 1, i32* %b
    br label %while_cond

while_cond:
    %1 = load i32* %i
    %2 = icmp slt i32 %1, %arg0
    br i1 %2, label %while_body, label %while_exit

while_body:
    ; calculate next fibonnaci number
    %3 = load i32* %a
    %4 = load i32* %b
    %5 = add i32 %3, %4
    store i32 %4, i32* %a
    store i32 %5, i32* %b

    ; increment i
    %6 = load i32* %i
    %7 = add i32 1, %6
    store i32 %7, i32* %i

    br label %while_cond

while_exit:
    %8 = load i32* %a
    ret i32 %8
}

define i32 @main() {
    ; program should print "55"
    %1 = call i32 @fib(i32 10)
    call void @print_int(i32 %1)
    ret i32 0
}
\end{Verbatim}

Another method of getting familiar with the LLVM intermediate representation was
by writing small programs in other languages (like C or Rust), to see how they
were translated by their respective compiler.

To keep track of the generated LLVM IR code, we introduced a \texttt{llvm::Context}
struct type. This struct keeps track of the generated code and metadata, like label
counters.

At the beginning of each function, stack space is allocated for each argument and
local variable using the \texttt{alloca} instruction. All the intermediary values
use the SSA based registers.

\subsection{Basic blocks}
LLVM supports function-like basic blocks. This made it easy to pass function arguments
by-value. This usually generates better machine code, because SPL does not have
any data structures that are very large. In the case that we would have had larger
data structures, we would probably decide to pass those
variables by-reference. For other control-flow constructs like \texttt{if} and
\texttt{while} loops we just use labels and \texttt{br} instructions.

The system also supports multiple calling conventions. Currently we use the calling
convention of \texttt{C}, but we have already experimented with other ones\footnote{One
of the mostly used calling conventions for \texttt{internal} functions is the
\texttt{fastcc} calling convention. This calling convention keep as much variables
in registers as possible, which also allows for tail call optimisation.}.

\subsection{Basic arithmetic}
The implementation of basic arithmetic operations was very straightforward. In the
case of \texttt{Op2Exp}: Calculate the two expressions, put them each in a register
and apply the corresponding instruction to the two registers.

At first, the logical operators \texttt{\&\&} and \texttt{||} were implemented in the
same way: calculating the two operand booleans, and doing a bitwise and/or instruction.
We benchmarked a program with its Clean version, and we found the Clean version was
about 5 percent faster. Because short-circuiting of these operations was the only
optimisation left, we decided to implement this. This resulted in an SPL program that
was about twice as fast as its Clean version.

\subsection{Lists}
Lists are implemented as singly linked lists. In the case of a list containing integers
this is an LLVM aggregate of type \texttt{\{ i1, i32, i8* \}}\footnote{It is possible
to leave the first boolean field and use only the third field by putting \texttt{null}
in the third field if we have the end of the list. At the time this data structure
was designed, we were unsure if this wouldn't cause any problems. So we decided to use
a separate value instead.}. The first value is a
boolean signifying the end of the list. The second value is the contained value and
the third value is a pointer to the next node (or \texttt{undef} if the first value
is \texttt{0}. The LLVM \texttt{@malloc} instrinsic is used for allocating on the heap.
There is currently no garbage collection, and lists are leaked when they go out of
scope.

\appendix
\section{Installing and using \texttt{rsplt}}

\begin{itemize}
    \item Download and install \texttt{rustc} and \texttt{cargo} from \texttt{https://www.rust-lang.org/downloads.html}.
    \item Clone the repo from \texttt{https://gitlab.science.ru.nl/dsprenkels/rsplt} using \texttt{git}.
    \item The release tag is \texttt{release-v0.1.0} and can be browsed on Gitlab
    through \url{https://gitlab.science.ru.nl/dsprenkels/rsplt/tags/release-v0.1.0}.
    \item \texttt{cd} into the source tree and run \texttt{cargo run -- --help} to run \texttt{rsplt}.
    \item To use LLVM code generation:
    \begin{itemize}
        \item \texttt{git checkout} the \texttt{dsprenkels/llvm} branch.
        \item install LLVM version \textbf{3.6} (\texttt{llc-3.6}, \texttt{lli-3.6})\footnote{Version 3.5
        will probably also work, but we have used version 3.6 for development.}.
        \item Use the same \texttt{cargo} command as above to run the program.
    \end{itemize}
\end{itemize}

\end{document}

