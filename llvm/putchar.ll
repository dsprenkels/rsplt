; External declaration of the putchar function
declare i8 @putchar(i8) nounwind

; Definition of main function
define i32 @main() {   ; i32()*
  ; Convert [13 x i8]* to i8  *...
  call i8 @putchar(i8 72)
  call i8 @putchar(i8 101)
  call i8 @putchar(i8 108)
  call i8 @putchar(i8 108)
  call i8 @putchar(i8 111)
  call i8 @putchar(i8 44)
  call i8 @putchar(i8 32)
  call i8 @putchar(i8 119)
  call i8 @putchar(i8 111)
  call i8 @putchar(i8 114)
  call i8 @putchar(i8 108)
  call i8 @putchar(i8 100)
  call i8 @putchar(i8 33)
  call i8 @putchar(i8 10)
  ret i32 0
}
