; Declare the string constant as a global constant.
@.format_int = private unnamed_addr constant [4 x i8] c"%i\0A\00"

declare i32 @printf(i8* noalias nocapture, ...)

define void @print_int(i32) {
    %2 = getelementptr [4 x i8]* @.format_int, i64 0, i64 0
    call i32 (i8*, ...)* @printf(i8* %2, i32 %0)
    ret void
}

define {i32, i32} @divmod(i32 %a, i32 %b) {
entry:
    ; just print the arguments
    %0 = sdiv i32 %a, %b
    %1 = srem i32 %a, %b
    %2 = alloca {i32, i32}
    %3 = getelementptr {i32, i32}* %2, i32 0, i32 0
    store i32 %0, i32* %3
    %4 = getelementptr {i32, i32}* %2, i32 0, i32 1
    store i32 %1, i32* %4
    %5 = load {i32, i32}* %2
    ret {i32, i32} %5
}

define i32 @main() {
    ; program should print "55"
entry:
    %0 = call {i32, i32} @divmod(i32 107, i32 12)
    %1 = extractvalue {i32, i32} %0, 0
    %2 = extractvalue {i32, i32} %0, 1
    call void @print_int(i32 %1) ; print `div` part
    call void @print_int(i32 %2) ; print `mod` part
    ret i32 0
}
