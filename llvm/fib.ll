; Declare the string constant as a global constant.
@.format_int = private unnamed_addr constant [4 x i8] c"%i\0A\00"

declare i32 @printf(i8* noalias nocapture, ...)

define void @print_int(i32) {
    %2 = getelementptr [4 x i8]* @.format_int, i64 0, i64 0
    call i32 (i8*, ...)* @printf(i8* %2, i32 %0)
    ret void
}

define i32 @fib(i32 %arg0) {
    %i = alloca i32
    %a = alloca i32
    %b = alloca i32
    store i32 0, i32* %i
    store i32 0, i32* %a
    store i32 1, i32* %b
    br label %while_cond

while_cond:
    %1 = load i32* %i
    %2 = icmp slt i32 %1, %arg0
    br i1 %2, label %while_body, label %while_exit

while_body:
    ; calculate next fibonnaci number
    %3 = load i32* %a
    %4 = load i32* %b
    %5 = add i32 %3, %4
    store i32 %4, i32* %a
    store i32 %5, i32* %b

    ; increment i
    %6 = load i32* %i
    %7 = add i32 1, %6
    store i32 %7, i32* %i

    br label %while_cond

while_exit:
    %8 = load i32* %a
    ret i32 %8
}

define i32 @main() {
    ; program should print "55"
    %1 = call i32 @fib(i32 10)
    call void @print_int(i32 %1)
    ret i32 0
}
