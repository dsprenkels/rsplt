NUMBER = 692963517;

def factorize(n):
    primes = []
    for i in xrange(2, n):
        if n % i == 0 and co_prime_with(i, primes):
            primes.append(i)
    return primes

def co_prime_with(elem, primes):
    return not any(elem % p == 0 for p in primes)

if __name__ == "__main__":
    result = factorize(NUMBER);
    print(result);
