factorize :: Int -> [Int]
factorize n = factorize_inner n 2 []

factorize_inner :: Int -> Int -> [Int] -> [Int]
factorize_inner n i primes
    | i < n = if (n `rem` i == 0) && (coPrimeWith i primes)
        then factorize_inner n (i + 1) (i : primes)
        else factorize_inner n (i + 1) primes
    | otherwise = primes

coPrimeWith :: Int -> [Int] -> Bool
coPrimeWith x primes = not $ any (\p -> x `rem` p == 0) primes

main = print $ factorize 692963517
