Grammar for SPL
===============

The Simple Programming Language (SPL) is defined in the following grammar

Rules
-----

```
SPL         = Decl+
Decl        = VarDecl
            | FunDecl
VarDecl     = VarKwOrType id '=' Exp ';'
VarKwOrType = VarKw
            | Type
VarKw       = 'var'
FunDecl     = id '(' [ id ( ',' id )* ] ')' [ '::' FunType ] '{' VarDecl* Stmt+ '}'
RetType     = Type
            | Void
Void        = 'Void'
FunType     = [ Type+ '->' ] RetType
Type        = BasicType
            | PairType
            | ListType
            | id
BasicType   = 'Int'
            | 'Bool'
            | 'Char'
PairType    = '(' Type ',' Type ')'
ListType    = '[' Type ']'
Stmt        = IfStmt
            | WhileStmt
            | ReturnStmt
            | AssStmt
            | FunCallStmt
Stmts       = Stmt*
IfStmt      = 'if' ParenExp '{' Stmt* '}' [ 'else' '{' Stmt* '}' ]
WhileStmt   = 'while' ParenExp '{' Stmt* '}'
AssStmt     = id [ Field ] '=' Exp ';'
FunCallStmt = FunCall ';'
ReturnStmt  = 'return' [ Exp ] ';'

Exp         = ExpL1
ExpL1       = Op1ExpL1
            | Op2ExpL1
            | ExpL2
ExpL2       = Op2ExpL2
            | ExpL3
ExpL3       = Op2ExpL3
            | ExpL4
ExpL4       = Op1ExpL4
            | Op2ExpL4
            | ExpL5
ExpL5       = Op2ExpL5
            | ExpL6
ExpL6       = FieldExp
            | FunCall
            | id
            | int
            | char
            | bool
            | PairExp
            | ParenExp
            | EmptyList
ParenExp    = '(' Exp ')'
FieldExp    = id Field

Op2ExpL1    = ExpL2 Op2L1 ExpL2 [ Op2ExpL1` ]
Op2ExpL1`   = Op2L1 ExpL2 [ Op2ExpL1` ]

Op2ExpL2    = ExpL3 Op2L2 ExpL3 [ Op2ExpL2` ]
Op2ExpL2`   = Op2L2 ExpL3 [ Op2ExpL2` ]

<!-- The following level is for the cons operator and should be
     right associative -->
Op2ExpL3    = ExpL4 Op2L3 ExpL3

Op2ExpL4    = ExpL5 Op2L4 ExpL5 [ Op2ExpL4` ]
Op2ExpL4`   = Op2L4 ExpL5 [ Op2ExpL4` ]

Op2ExpL5    = ExpL6 Op2L5 ExpL6 [ Op2ExpL5` ]
Op2ExpL5`   = Op2L5 ExpL6 [ Op2ExpL5` ]

Op1ExpL1    = Op1L1 Exp
Op1ExpL4    = Op1L4 Exp

bool        = True | False
True        = 'True'
False       = 'False'
PairExp     = '(' Exp ',' Exp ')'
Field       = '.' FieldKind [ Field ]
FieldKind   = 'hd' | 'tl' | 'fst' | 'snd'
FunCall     = id '(' [ Exp ( ',' Exp )* ] ')'
Op2L1       = '&&' | '||'
Op2L2       = '==' | '!=' | '<' | '<=' | '>' | '>='
Op2L3       = ':'
Op2L4       = '+' | '-'
Op2L5       = '*' | '/' | '%'
Op1L1        = '!'
Op1L4        = '-'
id          = alpha ( '_' | alphaNum )*
int         = [ '-' ] digit+
EmptyList   = []
```
