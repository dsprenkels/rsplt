# Notes Daan

## Scanner: word splitting problem

What qualifies as a word, in the language, and what does not?
A good example was the question wether to parse strings like `<>`
into `[Token::Lt, Token::Gt]`, or throw an error. An advantage of
the former would be that it would become easier in the parser.

In the end, we defined the following types of words:
- literal integers
- all consecutive whitespace (these tokens will be dropped anyway)
- comments (will also be dropped)
- all literal terminal tokens that are more than one char (e.g. `!=`, `::`)

See [issue 1](https://gitlab.science.ru.nl/dsprenkels/rsplt/issues/1) for more.

## In general: how to keep track of where an AST is in the original tree (for errors)?

For each struct type in the AST, keep a `Span { start, end }` value, so we can output
pretty errors like:

```plain
error: expected ')'
5:15     while ( n = 20 ) {
                   ^
```

## Parser: struct-enum problem

In the beginning of building the parser, there was the problem that constructs like

```plain
Exp         = FieldExp
            | Op2Exp
            | Op1Exp
            | int
            | char
            | 'True'
            | 'False'
            | '(' Exp ')'
            | FunCall
            | '[]'
            | PairExp
```

could not be wrapped inside one struct or enum data type, so we transformed the grammar
into two different rules:

Struct rules:
Grammar rules that represent a token, but without branching (without '|' characters in the grammar).

Enum rules:
Grammar rules that branch into multiple possible other rules.

Afted adapting the grammar, this resulted in something like this:

```
ExpL6       = FieldExp
            | FunCall
            | id
            | int
            | char
            | bool
            | PairExp
            | ParenExp
            | '[]'
```

## Parser: solve left recursion

Solution: apply recipe from Wikipedia. :D

Result:

```plain
// for left associative Op2's
Op2ExpL1    = ExpL2 Op2L1 ExpL2 [ Op2ExpL1` ]
Op2ExpL1`   = Op2L1 ExpL2 [ Op2ExpL1` ]

// for right associative Op2's (cons)
Op2ExpL3    = ExpL4 Op2L3 ExpL3
```

## CLoC

Lines of code: 2920
Of which test cases: 830 (30%)